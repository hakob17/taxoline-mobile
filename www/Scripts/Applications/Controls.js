﻿var mapType = "Yandex";

btBack = {
    backgroundData: "left.png",
    onClick: function () {
        uiBack();
    }
}

btBacktoStart = {
    backgroundData: "left.png",
    onClick: function () {
        checkMustRemoveAllButLastAfterFirstShow();
    }
}

function getAddressDescription(address_) {
    var result = address_.Description;
    if (IsNull(result)) {
        result = "";
    }

    if (globalParams.IgnoreAddressPrefixes && globalParams.IgnoreAddressPrefixes.length) {
        $.each(globalParams.IgnoreAddressPrefixes, function () {
            var cnt = 0;
            while (cnt++ < 10 && !IsNullOrEmpty(result) && result.length > this.length && result.substr(0, this.length) == this) {
                result = result.substr(this.length, result.length - this.length);
            }
        });
    }

    if (!IsNullOrEmpty(address_.Entrance)) {
        if (IsNullOrEmpty(result))
            result = ControlsConsts.entranceWithColonCaption + address_.Entrance;
        else
            result += ControlsConsts.entranceWithCommaAndColonCaption + address_.Entrance;
    }

    if (!IsNullOrEmpty(address_.Notes)) {
        if (IsNullOrEmpty(result))
            result = address_.Notes;
        else
            result += ", " + address_.Notes;
    }

    return result;
}

function getNameDesctiptionHtml(item_, description_) {
    var html = item_.Name;

    var description = description_;
    description = translatParsedMessage(description);
    if (IsNullOrEmpty(description))
        description = item_.Description;
    if (!IsNullOrEmpty(description) && description != item_.Name)
        html += "<div class='smallComment'>" + translatParsedMessage(description) + "</div>";
    return html;
}

function tryLater(count_) {
    showMessage({
        message: ControlsConsts.serviceUnavailableMessage,
        onButtonClick: function () {
            uiBack(count_);
        }
    });
}

function cxCreateDiv(params_) {
    params_.kind = "div";
    return cxCreateElement(params_);
}

function cxCreateImg(params_) {
    params_.kind = "img";
    var result = cxCreateElement(params_);
    result.attr("src", params_.src);
    return result;
}

function cxCreateSpan(params_) {
    params_.kind = "span";
    return cxCreateElement(params_);
}

function cxCreateTable(params_) {
    params_.kind = "table";
    return cxCreateElement(params_);
}

function cxCreateTR(params_) {
    params_.kind = "tr";
    return cxCreateElement(params_);
}

function cxCreateTD(params_) {
    params_.kind = "td";
    return cxCreateElement(params_);
}

function cxCreateInput(params_) {
    params_.kind = "input";
    var result = cxCreateElement(params_);

    result.on("keyup").keyup(function (e) {
        if (e.keyCode == 13) {
            if (params_.onEnterPressed) {
                var answer = params_.onEnterPressed();
                if (answer != false)
                    result.blur();
            }
            else
                result.blur();
        }
    });

    return result;
}

function cxCreateTextArea(params_) {
    params_.kind = "textarea";
    var result = cxCreateElement(params_);
    return result;
}

function cxCreateListItemContent(params_) {
    return new cxCreateListItemContentEx(params_).divItemText;
}

function cxCreateListItemContentEx(params_) {

    if (!IsNullOrEmpty(params_.item.ReadyHTML)) {
        params_.parent.html(params_.item.ReadyHTML);
        params_.parent.addClass("listItemPadding");
    }
    else {
        var table = cxCreateTable({className: "listItemTable", parent: params_.parent});
        var tr = cxCreateTR({parent: table});

        if (params_.item.Image) {
            var tdPic = cxCreateTD({className: "listItemLeftTD", parent: tr});
            this.divPic = cxCreateDiv({className: "listItemIcon", parent: tdPic});
            setBackground(this.divPic, params_.item.Image.toLowerCase(), picSizeEm);
        }

        var html = params_.html;
        if (IsNullOrEmpty(html)) {
            html = getNameDesctiptionHtml(params_.item);
        }
        var tdText = cxCreateTD({className: "listItemCenterTD", parent: tr});
        this.divItemText = cxCreateDiv({className: "listItemText", parent: tdText, html: html});
        if (IsNullOrEmpty(params_.item.Image)) {
            this.divItemText.css("padding-left", "0.3em");
        }

        if (params_.item.rightButton) {
            var tdRightPic = cxCreateTD({className: "listItemRightTD", parent: tr});
            this.divRightPic = cxCreateDiv({className: "listItemIcon listItemIconRight", parent: tdRightPic});
            setBackground(this.divRightPic, params_.item.rightButton.Image.toLowerCase(), picSizeEm);
            if (params_.item.Image)
                this.divItemText.addClass("listItemTextEx");
            addClickHandler(tdRightPic, {
                onClick: function () {
                    if (params_.item.rightButton.onClick)
                        params_.item.rightButton.onClick();
                }, transparent: true
            });
        }
    }

    if (params_.item.Clickable != false) {
        addClickHandler(params_.parent, {
            onClick: params_.onClick,
            onHold: params_.onHold
        });
    }
}

function cxCreateListItemInput(params_) {
    var table = cxCreateTable({className: "listItemTable", parent: params_.parent});
    var tr = cxCreateTR({parent: table});

    if (params_.image) {
        var tdPic = cxCreateTD({className: "listItemLeftTD", parent: tr});
        this.divPic = cxCreateDiv({className: "listItemIcon", parent: tdPic});
        setBackground(this.divPic, params_.image.toLowerCase(), picSizeEm);
    }

    var tdText = cxCreateTD({className: "listItemCenterTD", parent: tr});
    var teInput = cxCreateInput({parent: tdText, onEnterPressed: params_.onEnterPressed});
    if (!IsNullOrEmpty(params_.value))
        teInput.val(params_.value);
    teInput.attr("placeholder", params_.placeholder);

    return teInput;
}

function cxCreateElement(params_) {
    var result = $(document.createElement(params_.kind));
    if (params_.className)
        result.addClass(params_.className);
    if (params_.parent)
        params_.parent.append(result);
    if (params_.html)
        result.html(params_.html);
    if (params_.flipDirection) {
        result.attr("flipDirection", params_.flipDirection);

        console.log(result);
    }
    return result;
}

function showMessage(params_) {
    params_.buttonCaption = "ОК";
    showMessageBox(params_);
}

function showCancelMessage(params_) {
    params_.buttonCaption = ControlsConsts.cancelCapt;
    showMessageBox(params_);
}

function showMessageBox(params_) {
    log("showMessage: " + params_.message);
    log(params_);

    if (nativeNotifications && navigator.notification && navigator.notification.alert) {

        navigator.notification.alert(
            translatParsedMessage(params_.message),
            params_.onButtonClick,
            translatParsedMessage(params_.caption),
            params_.onButtonClick);

        if (params_.onShow)
            params_.onShow();
    }
    else {
        var divWrapper = cxCreateDiv({className: "centeredContentWrapper", flipDirection: params_.flipDirection});
        var divCentered = cxCreateDiv({className: "centeredContent", parent: divWrapper});
        var divMessage = cxCreateDiv({className: "centeredContentMessage", parent: divCentered, html: params_.message});

        var divToolbar = cxCreateDiv({className: "toolbar", parent: divMessage});

        createButton(divToolbar, {
            caption: params_.buttonCaption,
            onClick: params_.onButtonClick
        });

        uiShow(divWrapper, params_.onShow);
    }
}

function showConfirmation(params_) {
    log("showConfirmation: " + params_.message);

    if (nativeNotifications && navigator.notification && navigator.notification.confirm) {
        navigator.notification.confirm(
            params_.message,
            function (button_) {
                if (button_ == 1) {
                    if (params_.onButtonYesClick)
                        params_.onButtonYesClick();
                }
                else {
                    if (params_.onButtonNoClick)
                        params_.onButtonNoClick();
                }
            },
            params_.caption,
            [ControlsConsts.yesCaption, ControlsConsts.noCaption]);

        if (params_.onShow)
            params_.onShow();
    }
    else {
        var divWrapper = cxCreateDiv({className: "centeredContentWrapper", flipDirection: params_.flipDirection});
        var divCentered = cxCreateDiv({className: "centeredContent", parent: divWrapper});
        var divMessage = cxCreateDiv({className: "centeredContentMessage", parent: divCentered, html: params_.message});

        var divToolbar = cxCreateDiv({className: "toolbar", parent: divMessage});

        createButton(divToolbar, {
            caption: ControlsConsts.yesCaption,
            onClick: params_.onButtonYesClick
        });
        createButton(divToolbar, {
            caption: ControlsConsts.noCaption,
            onClick: params_.onButtonNoClick
        });

        uiShow(divWrapper, params_.onShow);
    }
}


function createButton(parent_, params_) {
    var divButton = cxCreateDiv({className: "button", parent: parent_});

    var caption = params_.caption;
    if (IsNull(caption))
        caption = "&nbsp";

    if (params_.className)
        divButton.addClass(params_.className);

    cxCreateSpan({parent: divButton, html: caption});

    var backgroundSize = params_.backgroundSize;
    if (IsNullOrEmpty(backgroundSize))
        backgroundSize = "1.25em";
    if (params_.backgroundData) {
        setBackground(divButton, params_.backgroundData, backgroundSize);
    }

    addClickHandler(divButton, {onClick: params_.onClick});
    return divButton;
}

function setBackground(element_, background_, size_) {
    element_.css("background-image", "url(" + getLocalURLPrefix() + "Images/Applications/" + background_ + ")");
    element_.css("background-repeat", "no-repeat");
    element_.css("background-position", "50% 50%");
    element_.css("background-size", size_);
}

function createCaptionButton(parent_, params_) {

    var className = "captionButton";
    if (params_.className)
        className = params_.className;

    var divButton = cxCreateDiv({className: className, parent: parent_, html: params_.caption});
    if (params_.backgroundData) {

        if (IsNull(params_.size))
            params_.size = "1.25em";

        setBackground(divButton, params_.backgroundData, params_.size);
        if (params_.backgroundData == "left.png") {
            divButton.prop("cxKind", "Back");
            divButton.prop("cxBackParams", params_);
        }

    }

    addClickHandler(divButton, {onClick: params_.onClick});
    return divButton;
}

function createCaption(parent_, params_) {

    var listClassName = "listCaption";

    if (params_.transparent) {
        parent_ = cxCreateDiv({parent: parent_, className: "dvTopToolbar"});
        listClassName = "listTransparentCaption";
    }

    var divCaption = cxCreateDiv({className: listClassName, parent: parent_});
    var table = cxCreateTable({className: "captionTable", parent: divCaption});
    var tr = cxCreateTR({parent: table});
    var tdLeft = cxCreateTD({className: "captionLeftTD", parent: tr});
    var tdCenter = cxCreateTD({className: "captionCenterTD", parent: tr});
    var tdRight = cxCreateTD({className: "captionRightTD", parent: tr});

    if (params_ && params_.buttons && params_.buttons.left)
        createCaptionButton(tdLeft, params_.buttons.left);

    if (params_.caption) {
        var spCaption = cxCreateSpan({parent: tdCenter, html: params_.caption});
    }
    else if (params_.buttons && params_.buttons.center) {
        createCaptionButton(tdCenter, params_.buttons.center);
    }

    if (params_.buttons && params_.buttons.right)
        createCaptionButton(tdRight, params_.buttons.right);
}

function showSelectableList(params_) {
    var that = this;

    log("showSelectableList: " + params_.caption);

    var divWrapper = cxCreateDiv({className: "scrollableControl", flipDirection: params_.flipDirection});

    createCaption(divWrapper, params_)

    var divListBody = cxCreateDiv({className: "listBody", parent: divWrapper});

    this.loadData = function (data_) {
        divListBody.empty();

        $.each(data_, function () {
            var listItem = this;

            var html = getNameDesctiptionHtml(listItem);

            var divItem = cxCreateDiv({className: "listItem", parent: divListBody});

            if (listItem.Style == "Buttons") {
                divItem.addClass("listItemPadding");
                divItem.css("text-align", "center")
                $.each(listItem.Buttons, function () {

                    var button = this;
                    var big = button.Style == "Big" ? true : false;

                    var btn = createCaptionButton(divItem, {
                        className: big ? "selectorButtonBig" : "selectorButton",
                        size: "1.5em",
                        onClick: function () {
                            if (params_.onSelect)
                                params_.onSelect(button);
                        }
                    });
                    cxCreateImg({parent: btn, src: getLocalURLPrefix() + "Images/Applications/" + button.Image});
                    cxCreateSpan({className: big ? null : "smallComment", parent: btn, html: button.Name});
                });
            }
            else if (listItem.Style == "Button") {
                var bt = createButton(divItem, {
                    caption: listItem.Name,
                    onClick: function () {
                        if (params_.onSelect)
                            params_.onSelect(listItem);
                    }
                });
                if (listItem.Color == "Green")
                    bt.addClass("greenButton");
                else if (listItem.Color == "Red")
                    bt.addClass("redButton");
                divItem.css("text-align", "center");
                bt.css("margin", "0");
                //bt.css("width", "100%");
                //divItem.css("border", "none");
            }
            else if (listItem.Style == "Input") {
                var inputItem = {
                    parent: divItem,
                    item: listItem
                };
                $.extend(inputItem, listItem);
                listItem.reference = cxCreateListItemInput(inputItem);
            }
            else {
                cxCreateListItemContent({
                    parent: divItem,
                    item: listItem,
                    onClick: function () {
                        if (params_.onSelect)
                            params_.onSelect(listItem);
                    },
                    onHold: function () {
                        if (params_.onHold)
                            params_.onHold(listItem, null, divItem);
                    }
                });
            }
        });
    }
    that.loadData(params_.items);

    uiShow(divWrapper);
}

function showSuperList(params_) {
    log("showSuperList: " + params_.caption);
    var divWrapper = cxCreateDiv({className: "scrollableControl"});

    var divWrapper = cxCreateDiv({className: "scrollableControl"});
    createCaption(divWrapper, params_)
    var divListBody = cxCreateDiv({className: "listBody", parent: divWrapper});

    if (params_.items) {
        var simpleItems = new Array();

        function fillItems(data_) {
            $.each(simpleItems, function () {
                this.remove();
            });

            var currentItem = null;
            $.each(data_, function () {
                var listItem = this;

                var html = getNameDesctiptionHtml(listItem);

                var divItem = cxCreateDiv({className: "listItem"});
                if (currentItem == null)
                    divListBody.prepend(divItem);
                else
                    currentItem.after(divItem);

                cxCreateListItemContent({
                    parent: divItem,
                    item: listItem,
                    onClick: function () {
                        if (listItem.onSelect)
                            listItem.onSelect(listItem, fillItems);
                    },
                    onHold: function () {
                        if (listItem.onHold)
                            listItem.onHold(listItem, fillItems);
                    }
                });

                simpleItems.push(divItem);
            });
        }

        fillItems(params_.items);
    }

    var listItems = new Array();
    if (params_.multiItems) {
        $.each(params_.multiItems, function () {
            var listItem = $.extend({}, this);
            listItems.push(listItem);

            var hasOption = $.grep(params_.multiItemsSelected, function (e) {
                    return e.ID == listItem.ID;
                }).length > 0;
            if (hasOption) {
                listItem.checked = true;
                listItem.Image = "checked.png";
            }
            else {
                listItem.checked = false;
                listItem.Image = "unchecked.png";
            }

            var divItem = cxCreateDiv({className: "listItem", parent: divListBody});

            var itemEx = new cxCreateListItemContentEx({
                parent: divItem,
                item: listItem,
                onClick: function () {
                    if (listItem.checked) {
                        setBackground(itemEx.divPic, "unchecked.png", picSizeEm);
                        listItem.checked = false;
                    }
                    else {
                        setBackground(itemEx.divPic, "checked.png", picSizeEm);
                        listItem.checked = true;
                    }
                }
            });
        });
    }

    var teInput = null;
    if (params_.memo) {
        var divItemNotes = cxCreateDiv({className: "listItem listItemPadding", parent: divListBody});
        teInput = cxCreateTextArea({parent: divItemNotes});
        teInput.val(params_.value);
        teInput.attr("placeholder", params_.placeholder);
        teInput.attr("rows", "5");
    }

    var divToolbarItem = cxCreateDiv({parent: divListBody, className: "listItem listItemPadding"});
    divToolbarItem.css("border", "none");
    divToolbarItem.css("text-align", "center")
    var btApply = createCaptionButton(divToolbarItem, {
        className: "selectorButtonBig",
        size: "1.5em",
        onClick: function () {
            var result = new Array();
            if (!IsNull(listItems)) {
                $.each(listItems, function () {
                    if (this.checked)
                        result.push(this);
                });
            }

            var memoValue = null;
            if (!IsNull(teInput))
                memoValue = teInput.val();

            params_.onContinue(result, memoValue, listItems);
        }
    });
    cxCreateImg({parent: btApply, src: getLocalURLPrefix() + "Images/Applications/apply.png"});
    cxCreateSpan({parent: btApply, html: ControlsConsts.applyCaption});

    uiShow(divWrapper);
}

function showMultiSelectableList(params_) {
    log("showMultiSelectableList: " + params_.caption);

    var divWrapper = cxCreateDiv({className: "scrollableControl"});
    createCaption(divWrapper, params_)
    var divListBody = cxCreateDiv({className: "listBody", parent: divWrapper});

    var listItems = new Array();
    $.each(params_.items, function () {
        var listItem = $.extend({}, this);
        listItems.push(listItem);

        var hasOption = $.grep(params_.selected, function (e) {
                return e.ID == listItem.ID;
            }).length > 0;
        if (hasOption) {
            listItem.checked = true;
            listItem.Image = "checked.png";
        }
        else {
            listItem.checked = false;
            listItem.Image = "unchecked.png";
        }

        var divItem = cxCreateDiv({className: "listItem", parent: divListBody});

        var itemEx = new cxCreateListItemContentEx({
            parent: divItem,
            item: listItem,
            onClick: function () {
                if (listItem.checked) {
                    setBackground(itemEx.divPic, "unchecked.png", picSizeEm);
                    listItem.checked = false;
                }
                else {
                    setBackground(itemEx.divPic, "checked.png", picSizeEm);
                    listItem.checked = true;
                }
            }
        });
    });

    var divToolbarItem = cxCreateDiv({parent: divListBody, className: "listItem listItemPadding"});
    divToolbarItem.css("border", "none");
    divToolbarItem.css("text-align", "center")
    var btApply = createCaptionButton(divToolbarItem, {
        className: "selectorButtonBig",
        size: "1.5em",
        onClick: function () {
            var result = new Array();
            $.each(listItems, function () {
                if (this.checked)
                    result.push(this);
            });
            params_.onContinue(result);
        }
    });
    cxCreateImg({parent: btApply, src: getLocalURLPrefix() + "Images/Applications/apply.png"});
    cxCreateSpan({parent: btApply, html: ControlsConsts.applyCaption});

    uiShow(divWrapper);
}

function showMemoEditor(params_) {
    log("showMemoEditor: " + params_.caption);

    var divWrapper = cxCreateDiv({className: "scrollableControl"});

    createCaption(divWrapper, params_)

    var divListBody = cxCreateDiv({className: "listBody", parent: divWrapper});

    if (params_.items) {
        function fillItems(data_) {
            $.each(data_, function () {
                var listItem = this;

                var html = getNameDesctiptionHtml(listItem);

                var divItem = cxCreateDiv({className: "listItem", parent: divListBody});

                cxCreateListItemContent({
                    parent: divItem,
                    item: listItem,
                    onClick: function () {
                        if (listItem.onSelect)
                            listItem.onSelect(listItem, fillItems);
                    },
                    onHold: function () {
                        if (listItem.onHold)
                            listItem.onHold(listItem, fillItems);
                    }
                });
            });
        }

        fillItems(params_.items);
    }

    divItemNotes = cxCreateDiv({className: "listItem listItemPadding", parent: divListBody});
    var teInput = cxCreateTextArea({parent: divItemNotes});
    teInput.val(params_.value);
    teInput.attr("placeholder", params_.placeholder);
    teInput.attr("rows", "5");

    var divToolbarItem = cxCreateDiv({parent: divListBody, className: "listItem listItemPadding"});
    divToolbarItem.css("border", "none");
    divToolbarItem.css("text-align", "center")
    var btApply = createCaptionButton(divToolbarItem, {
        className: "selectorButtonBig",
        size: "1.5em",
        onClick: function () {
            params_.onContinue(teInput.val());
        }
    });
    cxCreateImg({parent: btApply, src: getLocalURLPrefix() + "Images/Applications/apply.png"});
    cxCreateSpan({parent: btApply, html: ControlsConsts.applyCaption});

    uiShow(divWrapper);
}


function somethingWentWrong(params_) {

    navigator.notification.alert(
        translatParsedMessage(params_.message),
        params_.onButtonClick,
        translatParsedMessage(params_.caption),
        params_.onButtonClick);
    // var divWrapper = cxCreateDiv({className: "scrollableControl"});
    // createCaption(divWrapper, params_)
    //
    // var divListBody = cxCreateDiv({className: "infoBody", parent: divWrapper});
    // var divListBody = cxCreateSpan({parent: divListBody, html: params_.message});
    //
    // uiShow(divWrapper);
}

function showInfoPage(params_) {
    log("showInfoPage: " + params_.caption);

    var divWrapper = cxCreateDiv({className: "scrollableControl"});
    createCaption(divWrapper, params_)

    var divListBody = cxCreateDiv({className: "infoBody", parent: divWrapper});
    var divListBody = cxCreateSpan({parent: divListBody, html: params_.message});

    uiShow(divWrapper);
}

function showInputDigitsControl(params_) {
    log("showInputDigitsControl");

    var divWrapper = cxCreateDiv({className: "centeredContentWrapper", flipDirection: params_.flipDirection});
    createCaption(divWrapper, params_)

    var divCentered = cxCreateDiv({className: "centeredContent", parent: divWrapper});
    var divBody = cxCreateDiv({parent: divCentered, className: "tableWrapper"});

    var table = cxCreateDiv({parent: divBody, className: "digitsTable"});
    var trCaption = cxCreateDiv({parent: table, className: "digitsTableTR"});
    var tdCaption = cxCreateDiv({parent: trCaption, className: "digitsTableTD width100"});
    var Caption = cxCreateSpan({parent: tdCaption, html: params_.prompt});

    var trNumber = cxCreateDiv({parent: table, className: "digitsTableTR font150"});
    var tdNumber = cxCreateDiv({parent: trNumber, className: "digitsTableTD width100"});

    var divNumber = cxCreateSpan({parent: tdNumber, className: "numberSpan"});

    var value = params_.initialValue;
    if (value == undefined || value == null)
        value = "";
    if (params_.phoneNumber != null) {
        value = params_.phoneNumber;
    }
    var displayValue = "";
    var ok = false;
    var btContinue;

    function prepareDisplayNumber() {
        displayValue = "";
        var j = 0;
        for (var i = 0; i < params_.placeholder.length; i++) {
            if (params_.placeholder[i] == "_"/* || params_.placeholder[i] == value[j]*/) {
                if (j == value.length)
                    break;
                displayValue += value[j++];
            }
            else {
                displayValue += params_.placeholder[i];
            }
        }
        console.log(displayValue);
        ok = displayValue.length == params_.placeholder.length;

        if (ok) {
            btContinue.removeClass("disabledButton");
            //btContinue.addClass("greenButton");
        }
        else {
            btContinue.addClass("disabledButton");
            //btContinue.removeClass("greenButton");
        }

        if (displayValue.length)
            divNumber.html(displayValue);
        else
            divNumber.html("&nbsp");
    }

    function addDigit(digit_) {
        if (ok)
            return;

        value += digit_;
        prepareDisplayNumber();
    }

    function delDigit(digit_) {
        if (value.length == 0)
            return;

        value = value.substr(0, value.length - 1);
        prepareDisplayNumber();
    }

    function clearDigits() {
        value = "";
        prepareDisplayNumber();
    }


    var trA = cxCreateDiv({parent: table, className: "digitsTableTR"});
    var trB = cxCreateDiv({parent: table, className: "digitsTableTR"});
    var trC = cxCreateDiv({parent: table, className: "digitsTableTR"});
    var trD = cxCreateDiv({parent: table, className: "digitsTableTR"});
    var trE = cxCreateDiv({parent: table, className: "digitsTableTR"});
    var trL = cxCreateDiv({parent: table, className: "digitsTableTR"});
    var trLL = cxCreateDiv({parent: table, className: "digitsTableTR"});
    var td1 = cxCreateDiv({parent: trA, className: "digitsTableTD width33"});
    cxCreateDiv({parent: trA, className: "digitsTableTD width2"});
    var td2 = cxCreateDiv({parent: trA, className: "digitsTableTD width33"});
    cxCreateDiv({parent: trA, className: "digitsTableTD width2"});
    var td3 = cxCreateDiv({parent: trA, className: "digitsTableTD width33"});
    var td4 = cxCreateDiv({parent: trB, className: "digitsTableTD width33"});
    cxCreateDiv({parent: trB, className: "digitsTableTD width2"});
    var td5 = cxCreateDiv({parent: trB, className: "digitsTableTD width33"});
    cxCreateDiv({parent: trB, className: "digitsTableTD width2"});
    var td6 = cxCreateDiv({parent: trB, className: "digitsTableTD width33"});
    var td7 = cxCreateDiv({parent: trC, className: "digitsTableTD width33"});
    cxCreateDiv({parent: trC, className: "digitsTableTD width2"});
    var td8 = cxCreateDiv({parent: trC, className: "digitsTableTD width33"});
    cxCreateDiv({parent: trC, className: "digitsTableTD width2"});
    var td9 = cxCreateDiv({parent: trC, className: "digitsTableTD width33"});
    var tdClear = cxCreateDiv({parent: trD, className: "digitsTableTD width33"});
    cxCreateDiv({parent: trD, className: "digitsTableTD width2"});
    var td0 = cxCreateDiv({parent: trD, className: "digitsTableTD width33"});
    cxCreateDiv({parent: trD, className: "digitsTableTD width2"});
    var tdBackspace = cxCreateDiv({parent: trD, className: "digitsTableTD width33"});
    var tdContinue = cxCreateDiv({parent: trE, className: "digitsTableTD width100"});
    if (params_.autorization) {
        var tdLanguage_am = cxCreateDiv({parent: trL, className: "digitsTableTD width33"});
        var tdLanguage_en = cxCreateDiv({parent: trL, className: "digitsTableTD width33"});
        var tdLanguage_ru = cxCreateDiv({parent: trL, className: "digitsTableTD width33"});

        var tdLanguage_am_label = cxCreateDiv({parent: trLL, className: "digitsTableTD width33"});
        var tdLanguage_en_label = cxCreateDiv({parent: trLL, className: "digitsTableTD width33"});
        var tdLanguage_ru_label = cxCreateDiv({parent: trLL, className: "digitsTableTD width33"});

        var btn_am = createButton(tdLanguage_am, {
            caption: " ", backgroundData: 'am.png', className: 'flag', backgroundSize: '1em', onClick: function () {
                localStorage.setItem('currentLanguage', 'am');
                window.location.reload();
            }
        });
        var btn_en = createButton(tdLanguage_en, {
            caption: " ", backgroundData: 'en.png', className: 'flag', backgroundSize: '1em', onClick: function () {
                localStorage.setItem('currentLanguage', 'en');
                window.location.reload();
            }
        });
        var btn_ru = createButton(tdLanguage_ru, {
            caption: " ", backgroundData: 'ru.png', className: 'flag', backgroundSize: '1em', onClick: function () {
                localStorage.setItem('currentLanguage', 'ru');
                window.location.reload();
            }
        });

        var btn_am_label = createButton(tdLanguage_am_label, {
            caption: "AM", onClick: function () {
                localStorage.setItem('currentLanguage', 'am');
                window.location.reload();
            }
        });
        var btn_en_label = createButton(tdLanguage_en_label, {
            caption: "EN", onClick: function () {
                localStorage.setItem('currentLanguage', 'en');
                window.location.reload();
            }
        });
        var btn_ru_label = createButton(tdLanguage_ru_label, {
            caption: "RU", onClick: function () {
                localStorage.setItem('currentLanguage', 'ru');
                window.location.reload();
            }
        });
    }


    var bt1 = createButton(td1, {
        caption: "1", onClick: function () {
            addDigit("1");
        }
    });
    var bt2 = createButton(td2, {
        caption: "2", onClick: function () {
            addDigit("2");
        }
    });
    createButton(td3, {
        caption: "3", onClick: function () {
            addDigit("3");
        }
    });
    createButton(td4, {
        caption: "4", onClick: function () {
            addDigit("4");
        }
    });
    createButton(td5, {
        caption: "5", onClick: function () {
            addDigit("5");
        }
    });
    createButton(td6, {
        caption: "6", onClick: function () {
            addDigit("6");
        }
    });
    createButton(td7, {
        caption: "7", onClick: function () {
            addDigit("7");
        }
    });
    createButton(td8, {
        caption: "8", onClick: function () {
            addDigit("8");
        }
    });
    createButton(td9, {
        caption: "9", onClick: function () {
            addDigit("9");
        }
    });
    createButton(td0, {
        caption: "0", onClick: function () {
            addDigit("0");
        }
    });
    createButton(tdClear, {backgroundData: "clear.png", backgroundSize: "1em", onClick: clearDigits});
    createButton(tdBackspace, {
        backgroundData: "backspace.png", backgroundSize: "1em", onClick: function () {
            delDigit();
        }
    });

    /*btContinue = createButton(tdContinue, {
     caption: "Продолжить", onClick: function () {
     if (ok) {
     params_.onClick(value, displayValue);
     }
     }
     });*/

    var btContinue = createCaptionButton(tdContinue, {
        className: "selectorButtonBig",
        size: "1.5em",
        onClick: function () {
            if (ok) {
                params_.onClick(value, displayValue);
            }
        }
    });
    cxCreateImg({parent: btContinue, src: getLocalURLPrefix() + "Images/Applications/apply.png"});
    cxCreateSpan({parent: btContinue, html: ControlsConsts.continueCaption});


    btContinue.addClass("disabledButton");

    prepareDisplayNumber();

    uiShow(divWrapper);
}

function showEditRemoveDialog(params_) {
    log("showEditRemoveDialog: " + params_.caption);
    log(params_);

    var item = params_.item;

    var divWrapper = cxCreateDiv({className: "scrollableControl"});

    createCaption(divWrapper, params_)

    var divListBody = cxCreateDiv({className: "listBody", parent: divWrapper});

    var divItem = cxCreateDiv({className: "listItem", parent: divListBody});
    var teInput = cxCreateListItemInput({
        parent: divItem,
        image: "caption.png",
        value: item.Name,
        placeholder: params_.placeholder
    });

    var divItemEx = cxCreateDiv({className: "listItem", parent: divListBody});

    function setNameEx() {
        divItemEx.empty();

        var html = item.Description;

        cxCreateListItemContent({
            parent: divItemEx,
            item: item,
            html: html,
        });
        /*
         if (!IsNull(item.Image)) {
         var divPic = cxCreateDiv({ className: "listItemIcon", parent: divItemEx });
         setBackground(divPic, item.Image.toLowerCase(), picSizeEm);
         var divItemText = cxCreateDiv({ className: "listItemText", parent: divItemEx, html: html });
         if (item.rightButton) {
         var divPic = cxCreateDiv({ className: "listItemIcon listItemIconRight", parent: divItemEx });
         setBackground(divPic, item.rightButton.Image.toLowerCase(), picSizeEm);
         divItemText.addClass("listItemTextEx");
         addClickHandler(divPic, {
         onClick: function () {
         if (item.rightButton.onClick)
         item.rightButton.onClick();
         }, transparent: true
         });
         }
         }
         else {
         divItemEx.html(html);
         }
         */
    }

    setNameEx();

    if (item.Clickable != false) {
        addClickHandler(divItemEx, {
            onClick: function () {
                if (params_.onClick)
                    params_.onClick(item);
            }
        });
    }

    function updateItem(item_) {
        item = item_;
        teInput.val(item.Name);
        setNameEx();
    }

    var divToolbarItem = cxCreateDiv({parent: divListBody, className: "listItem listItemPadding"});
    divToolbarItem.css("border", "none");
    divToolbarItem.css("text-align", "center")
    var btApply = createCaptionButton(divToolbarItem, {
        className: "selectorButtonBig",
        size: "1.5em",
        onClick: function () {
            var newItem = $.extend({}, item);
            newItem.Name = teInput.val();
            params_.onEdit(newItem);
        }
    });
    cxCreateImg({parent: btApply, src: getLocalURLPrefix() + "Images/Applications/apply.png"});
    cxCreateSpan({parent: btApply, html: ControlsConsts.applyCaption});
    /*
     if (params_.CanDelete != false) {
     var btDelete = createCaptionButton(divToolbarItem, {
     className: "selectorButtonBig",
     size: "1.5em",
     onClick: function () {
     showConfirmation({
     caption: "Подтверждение",
     message: params_.deleteConfirmationText,
     onButtonYesClick: function () {
     uiRemovePreLast();
     params_.onDelete();
     },
     onButtonNoClick: function () {
     uiBack();
     }
     });
     }
     });
     cxCreateImg({ parent: btDelete, src: getLocalURLPrefix() + "Images/Applications/delete.png" });
     cxCreateSpan({ parent: btDelete, html: "Удалить" });
     }


     /*
     var divContinue = cxCreateDiv({ className: "listItem", parent: divListBody });
     var btContinue = createButton(divContinue, {
     caption: "Применить",
     onClick: function () {
     var newItem = $.extend({}, item);
     newItem.Name = teInput.val();
     params_.onEdit(newItem);
     }
     });
     btContinue.addClass("greenButton");
     btContinue.css("margin", "0");
     btContinue.css("width", "100%");
     divContinue.css("text-align", "center");
     divContinue.css("border", "none");

     if (params_.CanDelete != false) {
     var divDelete = cxCreateDiv({ className: "listItem", parent: divListBody });
     var btDelete = createButton(divDelete, {
     caption: "Удалить",
     onClick: function () {
     showConfirmation({
     caption: "Подтверждение",
     message: params_.deleteConfirmationText,
     onButtonYesClick: function () {
     uiRemovePreLast();
     params_.onDelete();
     },
     onButtonNoClick: function () {
     uiBack();
     }
     });
     }
     });
     btDelete.addClass("redButton");
     btDelete.css("margin", "0");
     btDelete.css("width", "100%");
     divDelete.css("text-align", "center");
     divDelete.css("border", "none");
     }
     */
    uiShow(divWrapper);

    return updateItem;
}

function showRateForm(params_) {
    log("showRateForm: " + params_.caption);

    var divWrapper = cxCreateDiv({className: "scrollableControl"});

    var rating = null;

    createCaption(divWrapper, params_)

    var divListBody = cxCreateDiv({className: "listBody", parent: divWrapper});

    divItemRating = cxCreateDiv({className: "listItem listItemPadding", parent: divListBody});
    divItemRating.css("text-align", "center");

    var divs = new Array();
    for (var i = 1; i <= 5; i++) {
        var div = cxCreateDiv({className: "listItemIconRatingStar", parent: divItemRating});
        setBackground(div, "star0.png", picSizeEm);
        div.css("margin", "0 0.125em");
        divs.push(div);
        addClickHandler(div, {
            onClick: function (sender_) {
                var rat = divs.indexOf(sender_) + 1;
                rating = rat;

                for (var j = 0; j < rat; j++) {
                    setBackground(divs[j], "star1.png", picSizeEm);
                }
                for (var j = rat; j < 5; j++) {
                    setBackground(divs[j], "star0.png", picSizeEm);
                }
            }, transparent: true
        });
    }


    divItemNotes = cxCreateDiv({className: "listItem listItemPadding", parent: divListBody});
    var teInput = cxCreateTextArea({parent: divItemNotes});
    teInput.val(params_.value);
    teInput.attr("placeholder", ControlsConsts.commentCaption);
    teInput.attr("rows", "5");
    teInput.on("input", function () {
        calculateHeight();
    });

    /*
     var divContinue = cxCreateDiv({ className: "listItem", parent: divListBody });
     var btContinue = createButton(divContinue, {
     caption: "Отправить",
     onClick: function () {
     params_.onContinue(rating, teInput.val());
     }
     });
     btContinue.addClass("greenButton");
     btContinue.css("margin", "0");
     btContinue.css("width", "100%");
     divContinue.css("text-align", "center");
     divContinue.css("border", "none");
     */

    var divContinue = cxCreateDiv({parent: divListBody, className: "listItem listItemPadding"});
    divContinue.css("border", "none");
    divContinue.css("text-align", "center")
    var btContinue = createCaptionButton(divContinue, {
        className: "selectorButtonBig",
        size: "1.5em",
        onClick: function () {
            params_.onContinue(rating, teInput.val());
        }
    });
    cxCreateImg({parent: btContinue, src: getLocalURLPrefix() + "Images/Applications/apply.png"});
    cxCreateSpan({parent: btContinue, html: ControlsConsts.sendCaption});


    var minHeight = null;
    var maxHeight = null;

    var domInput = teInput.get(0);

    function calculateHeight() {
        return;
        if (IsNull(minHeight)) {
            minHeight = domInput.clientHeight;
            maxHeight = divListBody.get(0).clientHeight + divItemNotes.get(0).offsetHeight - teInput.get(0).offsetHeight;
        }
        domInput.style.height = minHeight + "px";
        var h = Math.max(minHeight, Math.min(domInput.scrollHeight, maxHeight));
        domInput.style.height = h + "px";
    }

    uiShow(divWrapper);
}

function showMap(params_) {
    log("showMap");

    var myMap;

    var bounds = new Array();
    var c1 = new Array();
    c1.push(mapStartCenter[0] - 0/*0.002*/);
    c1.push(mapStartCenter[1] - 0/*0.002*/);
    bounds.push(c1);
    var c2 = new Array();
    c2.push(mapStartCenter[0] + 0/*0.002*/);
    c2.push(mapStartCenter[1] + 0/*0.002*/);
    bounds.push(c2);

    var divWrapper = cxCreateDiv({className: "fullScreenWrapper"});
    var divMap = cxCreateDiv({className: "fullScreenWrapper", parent: divWrapper});
    var divOverlay = cxCreateDiv({className: "fullScreenOverlay", parent: divWrapper});
    divOverlay.css("background", "transparent");

    var tb = cxCreateTable({parent: divOverlay});

    var trTop = cxCreateTR({parent: tb});
    var trCeil = cxCreateTR({parent: tb});
    var trCenter = cxCreateTR({parent: tb});
    var trFloor = cxCreateTR({parent: tb});
    var trBottom = cxCreateTR({parent: tb});

    var tdTop = cxCreateTD({parent: trTop});
    var tdCeil = cxCreateTD({parent: trCeil});
    var tdCenter = cxCreateTD({parent: trCenter});
    var tdFloor = cxCreateTD({parent: trFloor});
    var tdBottom = cxCreateTD({parent: trBottom});

    var divCaption = cxCreateDiv({parent: tdCeil});

    tb.css("width", "100%");
    tb.css("height", "100%");

    tdTop.css("height", "2em");
    tdBottom.css("height", "2em");

    tdCeil.css("vertical-align", "top");
    tdCenter.css("vertical-align", "middle");
    tdFloor.css("vertical-align", "bottom");

    divCaption.html("&nbsp");
    divCaption.css("font-size", "1.3em");
    divCaption.css("padding", "0 2em");


    function setMapBounds() {
        var d1 = bounds[1][1] - bounds[0][1];
        var d2 = bounds[1][0] - bounds[0][0];

        var boundsWithMargins = [[bounds[0][0] - d2 / 8, bounds[0][1] - d1 / 8], [bounds[1][0] + d2 / 8, bounds[1][1] + d1 / 8]];
        MxSetBounds(myMap, mapType, boundsWithMargins, {
            checkZoomRange: true,
            preciseZoom: true
        });
    }

    var btCenter = createCaptionButton(divOverlay, {
        backgroundData: "zoom.png",
        className: "transparentButton",
        onClick: function () {
            setMapBounds();
        }
    });

    btCenter.css("position", "absolute");
    btCenter.css("bottom", "0.25em");
    btCenter.css("left", "0.25em");

    var btZoomOut = createCaptionButton(divOverlay, {
        backgroundData: "minus.png",
        className: "transparentButton",
        onClick: function () {
            var z = MxGetZoom(myMap, mapType);
            MxSetZoom(myMap, mapType, z - 1, {
                checkZoomRange: true,
                duration: 200
            });
        }
    });

    btZoomOut.css("position", "absolute");
    btZoomOut.css("bottom", "0.25em");
    btZoomOut.css("left", "2.25em");

    var btZoomIn = createCaptionButton(divOverlay, {
        backgroundData: "plus.png",
        className: "transparentButton",
        onClick: function () {
            var z = MxGetZoom(myMap, mapType);
            MxSetZoom(myMap, mapType, z + 1, {
                checkZoomRange: true,
                duration: 200
            });
        }
    });

    btZoomIn.css("position", "absolute");
    btZoomIn.css("bottom", "0.25em");
    btZoomIn.css("left", "4.25em");

    var btMenu = createCaptionButton(divOverlay, {
        backgroundData: "left.png",
        className: "transparentButton",
        onClick: function () {
            if (params_.onBackClick)
                params_.onBackClick();
        }
    });
    btMenu.css("position", "absolute");
    btMenu.css("top", "0.25em");
    btMenu.css("left", "0.25em");

    var btCall = createCaptionButton(divOverlay, {
        backgroundData: "call.png",
        className: "transparentButton",
        onClick: function () {
            if (params_.onCallClick)
                params_.onCallClick();
        }
    });

    btCall.css("position", "absolute");
    btCall.css("top", "0.25em");
    btCall.css("right", "0.25em");

    var idMap = "dvMap_" + generateUUID();
    divMap.attr("id", idMap);

    var skipTransparent = false;

    var ids = "";
    var oldIds = "";
    var geoObjects = {};

    this.loadData = function (data_, params_) {
        if (IsNull(data_))
            return;

        var points = data_.points;
        var caption = translatParsedMessage(data_.caption);

        if (IsNullOrEmpty(caption))
            caption = "&nbsp";

        divCaption.html(caption);

        ids = "";
        $.each(geoObjects, function () {
            this.isActual = false;
        });

        var p1 = new Array();
        var p2 = new Array();

        var carPoint = new Array();
        var toPointArray = new Array();
        var interPointArray = new Array();
        var fromPointArray = new Array();

        var interIndex = 0;
        $.each(points, function () {
            var point = this;

            var image = point.Item1;
            var p = point.Item2;

            var name = image;

            if (name == "point_from") {
                fromPointArray[0] = p.Lat;
                fromPointArray[1] = p.Lon;
            }

            if (name == "point_inter") {
                interPointArray[interIndex] = new Array();
                interPointArray[interIndex][0] = p.Lat;
                interPointArray[interIndex][1] = p.Lon;
                name += interIndex;
                interIndex++;
            }
            if (name == "point_car") {
                carPoint[0] = p.Lat;
                carPoint[1] = p.Lon;
            }
            if (name == "point_to") {
                toPointArray[0] = p.Lat;
                toPointArray[1] = p.Lon;
            }

            ids += name + "|";

            image += ".png";

            if (p1.length) {
                p1[0] = Math.min(p1[0], p.Lat - 0/*0.002*/);
                p1[1] = Math.min(p1[1], p.Lon - 0/*0.002*/);
            }
            else {
                p1[0] = p.Lat - 0/*0.002*/;
                p1[1] = p.Lon - 0/*0.002*/;
            }
            if (p2.length) {
                p2[0] = Math.max(p2[0], p.Lat + 0/*0.002*/);
                p2[1] = Math.max(p2[1], p.Lon + 0/*0.002*/);
            }
            else {
                p2[0] = p.Lat + 0/*0.002*/;
                p2[1] = p.Lon + 0/*0.002*/;
            }

            var geoObject = geoObjects[name];

            if (IsNull(geoObject)) {
                console.log(name + ' New Placemark');
                geoObject = MxPlacemark(mapType, [p.Lat, p.Lon], {
                    iconLayout: 'default#image',
                    iconImageHref: getLocalURLPrefix() + "Images/Applications/" + image,
                    iconImageSize: [48, 48],
                    iconImageOffset: [-24, -44]
                });
                MxAddPlacemarkOnMap(myMap, mapType, geoObject);

                geoObject.isActual = true;
                geoObjects[name] = geoObject;
            }
            else {
                geoObject.isActual = true;
                MxSetCoordinates(geoObject, mapType, [p.Lat, p.Lon]);
                //geoObject.geometry.setCoordinates([p.Lat, p.Lon]);
            }
        });

        var removeList = new Array();
        $.each(geoObjects, function (name_) {
            if (!this.isActual) {
                removeList.push(name_);
            }
        });

        $.each(removeList, function () {
            var geoObject = geoObjects[this];
            MxRemovePlacemarkFromMap(myMap, mapType, geoObject);
            //myMap.geoObjects.remove(geoObject);
            geoObjects[this] = null;
        });

        bounds = new Array();
        bounds.push(p1);
        bounds.push(p2);

        if (params_ && params_.drawRoute) {


            if (fromPointArray.length && carPoint.length) {
                MxRoute(mapType, [carPoint, fromPointArray], function (route) {
                        console.log('route1');
                        geoObjects['route'] = route;
                        MxAddRoute(myMap, mapType, route);
                    },
                    function (error) {
                        console.log("Error: " + error.message);
                    });
            }
            else if (toPointArray.length && carPoint.length) {
                var array = new Array();
                array[0] = carPoint;
                for (var i = 1; i < interIndex + 1; i++) {
                    array[i] = interPointArray[i - 1];
                }
                array[interIndex + 1] = toPointArray;
                MxRoute(mapType, array, function (route) {
                    //if (mapType == 'Yandex' || mapType == 'YandexNarod') {
                    geoObjects['route'] = route;
                    /*}
                     if (mapType == "Google") {
                     geoObjects['route'] = route.routes[0];
                     }*/

                    MxAddRoute(myMap, mapType, route); // TODO переделать

                    /*myMap.geoObjects.add(route);
                     route.getWayPoints().removeAll();
                     route.getPaths().options.set({
                     strokeColor: '0000ffff'
                     });*/
                }, function (error) {
                    console.log("Error: " + error.message);
                });
            }

            /*if (params_.isMovingTo == false) {
             var array = new Array();
             array[0] = carPoint;
             for (var i = 1; i < interIndex + 1; i++) {
             array[i] = interPointArray[i - 1];
             }
             array[interIndex + 1] = toPointArray;

             MxRoute(mapType, array, function (route) {
             if (mapType == 'Yandex' || mapType == 'YandexNarod') {
             geoObjects['route'] = route;
             }
             if (mapType == "Google") {
             geoObjects['route'] = route.routes[0];
             }

             MxAddRoute(myMap, mapType, route); // TODO переделать

             myMap.geoObjects.add(route);
             route.getWayPoints().removeAll();
             route.getPaths().options.set({
             strokeColor: '0000ffff'
             });
             }, function (error) {
             console.log("Error: " + error.message);
             });
             }*/
        }

        if (ids != oldIds) {
            oldIds = ids;

            setTimeout(function () { // непонятно почему без таймаута не работает
                setMapBounds();
            }, 100);
        }
    }


    uiShow(divWrapper, function () {
        var mapOptions = {idMap: idMap, mapStartCenter: mapStartCenter, zoom: 18}

        function ready(map_) {

            myMap = map_;

            $.each(divWrapper, function () {
                this.cxRefresh = function () {
                    MxFitToViewport(myMap, mapType);
                }
            });

            var transparentTimeout = null;

            if (params_.onShow)
                params_.onShow(myMap);
        }

        InitialMap(myMap, mapType, mapOptions, ready);

    });
}


function showAddressPickerControl(params_) {
    log("showAddressPickerControl");

    var myMap;

    var divWrapper = cxCreateDiv({className: "fullScreenWrapper"});
    var divMap = cxCreateDiv({className: "fullScreenWrapper", parent: divWrapper});
    var divOverlay = cxCreateDiv({className: "fullScreenOverlay", parent: divWrapper});

    var tb = cxCreateTable({parent: divOverlay});

    var trTop = cxCreateTR({parent: tb});
    var trCeil = cxCreateTR({parent: tb});
    var trCenter = cxCreateTR({parent: tb});
    var trFloor = cxCreateTR({parent: tb});
    var trBottom = cxCreateTR({parent: tb});

    var tdTop = cxCreateTD({parent: trTop});
    var tdCeil = cxCreateTD({parent: trCeil});
    var tdCenter = cxCreateTD({parent: trCenter});
    var tdFloor = cxCreateTD({parent: trFloor});
    var tdBottom = cxCreateTD({parent: trBottom});

    var divFromLabel = cxCreateDiv({parent: tdCeil});
    divFromLabel.html(params_.caption);
    var divFromAddress = cxCreateDiv({parent: tdCeil});
    divFromAddress.html("&nbsp");

    var divNext = cxCreateDiv({parent: tdFloor});
    divNext.addClass("textButton");
    spanNext = cxCreateSpan({parent: divNext});
    spanNext.html(ControlsConsts.applyCaption);
    var divFindAddress = cxCreateDiv({parent: tdFloor});

    tb.css("width", "100%");
    tb.css("height", "100%");

    tdTop.css("height", "5%");
    tdBottom.css("height", "8%");

    tdCeil.css("vertical-align", "top");
    tdCenter.css("vertical-align", "middle");
    tdFloor.css("vertical-align", "bottom");

    divFromLabel.css("font-size", "0.9em");
    divFromAddress.css("font-size", "1.3em");
    spanNext.css("font-size", "1.5em");
    divNext.css("padding-bottom", "0.5em");

    spanNext.addClass("textButton");

    var btCenter = createCaptionButton(divOverlay, {
        backgroundData: "location.png",
        className: "transparentButton",
        onClick: function () {
            gotoCenter();
        }
    });

    btCenter.css("position", "absolute");
    btCenter.css("bottom", "0.25em");
    btCenter.css("left", "0.25em");

    var btZoomOut = createCaptionButton(divOverlay, {
        backgroundData: "minus.png",
        className: "transparentButton",
        onClick: function () {
            var z = MxGetZoom(myMap, mapType);
            MxSetZoom(myMap, mapType, z - 1, {
                checkZoomRange: true,
                duration: 200
            });
        }
    });

    btZoomOut.css("position", "absolute");
    btZoomOut.css("bottom", "0.25em");
    btZoomOut.css("left", "2.25em");

    var btZoomIn = createCaptionButton(divOverlay, {
        backgroundData: "plus.png",
        className: "transparentButton",
        onClick: function () {
            var z = MxGetZoom(myMap, mapType);
            MxSetZoom(myMap, mapType, z + 1, {
                checkZoomRange: true,
                duration: 200
            });
        }
    });

    btZoomIn.css("position", "absolute");
    btZoomIn.css("bottom", "0.25em");
    btZoomIn.css("left", "4.25em");

    var btMenu = createCaptionButton(divOverlay, {
        backgroundData: "left.png",
        className: "transparentButton",
        onClick: function () {
            uiBack();
        }
    });

    btMenu.css("position", "absolute");
    btMenu.css("top", "0.25em");
    btMenu.css("left", "0.25em");

    var btCall = createCaptionButton(divOverlay, {
        backgroundData: "call.png",
        className: "transparentButton",
        onClick: function () {
            if (params_.onCallClick)
                params_.onCallClick();
        }
    });

    btCall.css("position", "absolute");
    btCall.css("top", "0.25em");
    btCall.css("right", "0.25em");

    var resultName = null;
    var resultDescription = null;

    addClickHandler(spanNext, {
        onClick: function () {
            if (params_.onContinue)
                params_.onContinue({
                    Kind: "Map",//"Building",
                    MapType: mapType,
                    Image: "Map.png",//"Building",
                    Point: MxGetCenter(myMap, mapType),
                    Name: resultName,
                    Description: resultDescription
                });
        }, transparent: true
    });

    setBackground(divOverlay, "marker.png", "3.0em");

    var idMap = "dvMap_" + generateUUID();
    divMap.attr("id", idMap);

    function clearAddress() {
        divFromAddress.html("&nbsp");
        spanNext.hide();
    }

    clearAddress();


    function detectAddress(lat_, lon_) {

        point = MxGetCenter(myMap, mapType);
        if (lat_ && lon_)
            point = [lat_, lon_];

        MxGeocode(myMap, mapType, point, {kind: 'house', results: 1, json: true}, function (res) {

                resultName = MxGetAddress(mapType, res);
                resultDescription = MxGetAddressDescription(mapType, res, resultName);

                if (resultName) {
                    divFromAddress.html(resultName);
                    spanNext.show();
                }
                else {
                    MxGeocode(myMap, mapType, point, {kind: 'locality', results: 1, json: true}, function (res) {

                            resultName = MxGetAddress(mapType, res);
                            resultDescription = MxGetAddressDescription(mapType, res, resultName);

                            if (resultName) {
                                divFromAddress.html(resultName);
                                spanNext.show();
                            }
                            else {
                                clearAddress();
                            }
                        },
                        function (err) {
                            clearAddress();
                        });
                }
            },
            function (err) {
                clearAddress();
            });
    }

    var skipTransparent = false;
    var gotoCenterCanceled = false;

    function gotoCenter() {
        gotoCenterCanceled = false;
        // TODO
        cxGeoLocate(function (lat_, lon_, acc_) {
            log("geolocation success lat=" + lat_ + " lon=" + lon_ + " acc=" + acc_);
            if (gotoCenterCanceled) {
                return;
            }
            skipTransparent = true;

            MxPanTo(myMap, mapType, convertPoint(mapType, [lat_, lon_]), {
                flying: false,
                safe: false
            }, function () {
                detectAddress();
                skipTransparent = false;
            }, function (err) {
                skipTransparent = false;
            });

        }, function () {
            log("geolocation failed");
        }, 5000);

        /*
         if (navigator.geolocation) {
         navigator.geolocation.getCurrentPosition(
         function (position) {
         if (gotoCenterCanceled) {
         return;
         }
         skipTransparent = true;

         MxPanTo(myMap, mapType, convertPoint(mapType, [position.coords.latitude, position.coords.longitude]), {
         flying: false,
         safe: false
         }, function () {
         detectAddress();
         skipTransparent = false;
         }, function (err) {
         skipTransparent = false;
         });

         },
         function handle_error(err) {
         // не смогли определить координаты
         });
         }
         else {
         // не смогли определить координаты
         }
         */
    }


    uiShow(divWrapper, function () {

        var mapOptions = {idMap: idMap, mapStartCenter: mapStartCenter, zoom: 18}

        function ready(map_) {
            myMap = map_;

            $.each(divWrapper, function () {
                this.cxRefresh = function () {
                    MxFitToViewport(myMap, mapType);
                    //myMap.container.fitToViewport();
                }
            });

            MxAddEvent(myMap, mapType, "actionbegin", function (e) {
                if (skipTransparent)
                    return;

                gotoCenterCanceled = true;

                transparentTimeout = setTimeout(function () {
                    transparentTimeout = null;
                    //tb.fadeOut(200);
                    //divOverlay.animate({ "background-color": overlayTransparentBackground }, 200);
                    tb.hide();
                    btCall.hide();
                    btCenter.hide();
                    btZoomIn.hide();
                    btZoomOut.hide();
                    btMenu.hide();
                    divOverlay.css("background-color", overlayTransparentBackground);
                }, 200);

            });

            MxAddEvent(myMap, mapType, "actionend", function (e) {
                if (skipTransparent)
                    return;

                if (transparentTimeout) {
                    clearTimeout(transparentTimeout);
                    transparentTimeout = null;
                }
                else {
                    //tb.fadeIn(200);
                    //divOverlay.animate({ "background-color": overlayDefaultBackground }, 200);
                    tb.show();
                    btCall.show();
                    btCenter.show();
                    btZoomIn.show();
                    btZoomOut.show();
                    btMenu.show();
                    divOverlay.css("background-color", overlayDefaultBackground);
                }

                detectAddress();
            });

            MxAddEvent(myMap, mapType, "click", function (e) {
                skipTransparent = true;
                console.log(e);
                MxPanTo(myMap, mapType, MxGetCoords(e, mapType), null, function () {
                        detectAddress();
                        skipTransparent = false;
                    },
                    function () {
                        skipTransparent = false;
                    });
            });

            var transparentTimeout = null;

            gotoCenter();

        }

        InitialMap(myMap, mapType, mapOptions, ready);
    });
}

function showNewOrderControl(params_) {
    log("showNewOrderControl");

    var myMap;

    var divWrapper = cxCreateDiv({className: "fullScreenWrapper"});
    var divMap = cxCreateDiv({className: "fullScreenWrapper", parent: divWrapper});
    var divOverlay = cxCreateDiv({
        className: "fullScreenOverlay", parent: divWrapper
        // html: ' <svg class="pulsar" width="100px" height="100px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14 14">' +
        // '<animate xlink:href="#animation-back" attributeName="r" from="1" to="6.5" dur="1.2s" begin="0s" repeatCount="indefinite"' +
        // 'fill="freeze" id="circ-anim" />' +
        // '<animate xlink:href="#animation-back" attributeType="CSS" attributeName="opacity"' +
        // 'from="1" to="0" dur="1.2s" begin="0s" repeatCount="indefinite" fill="freeze" id="circ-anim"' +
        // '/><circle id="animation-back" cx="7.2" cy="7" r="1" stroke-width="0.333"/>' +
        // '<circle class="animation-front" cx="7.2" cy="7" r="0.6"/>' +
        // '        </svg>'
    });

    var tb = cxCreateTable({parent: divOverlay});

    var trTop = cxCreateTR({parent: tb});
    var trCeil = cxCreateTR({parent: tb});
    var trCenter = cxCreateTR({parent: tb});
    var trFloor = cxCreateTR({parent: tb});
    var trBottom = cxCreateTR({parent: tb});

    var tdTop = cxCreateTD({parent: trTop});
    var tdCeil = cxCreateTD({parent: trCeil});
    var tdCenter = cxCreateTD({parent: trCenter});
    var tdFloor = cxCreateTD({parent: trFloor});
    var tdBottom = cxCreateTD({parent: trBottom});

    var divFromLabel = cxCreateDiv({parent: tdCeil});
    divFromLabel.html(ControlsConsts.fromCaption);
    var divFromAddress = cxCreateDiv({parent: tdCeil});
    divFromAddress.html("&nbsp");

    var divNext = cxCreateDiv({parent: tdFloor});
    divNext.addClass("textButton");
    spanNext = cxCreateDiv({parent: divNext});
    spanNext.html(ControlsConsts.futherCaption);
    var divFindAddress = cxCreateDiv({parent: tdFloor});
    divFindAddress.addClass("textButton");
    spanFindAddress = cxCreateSpan({parent: divFindAddress});
    spanFindAddress.html(ControlsConsts.searchAddress);

    divFromLabel.css("font-size", "0.9em");
    divFromAddress.css("font-size", "1.3em");
    spanNext.css("font-size", "1.5em");
    divNext.css("padding-bottom", "0.5em");
    divFromAddress.css("padding-top", "0.15em");

    spanNext.addClass("textButton");
    spanFindAddress.addClass("textButton");

    var btCenter = createCaptionButton(divOverlay, {
        backgroundData: "location.png",
        className: "transparentButton",
        onClick: function () {
            gotoCenter();
        }
    });

    btCenter.css("position", "absolute");
    btCenter.css("bottom", "0.25em");
    btCenter.css("left", "0.25em");

    var btZoomOut = createCaptionButton(divOverlay, {
        backgroundData: "minus.png",
        className: "transparentButton",
        onClick: function () {
            var z = MxGetZoom(myMap, mapType);
            MxSetZoom(myMap, mapType, z - 1, {
                checkZoomRange: true,
                duration: 200
            });
        }
    });

    btZoomOut.css("position", "absolute");
    btZoomOut.css("bottom", "0.25em");
    btZoomOut.css("left", "2.25em");

    var btZoomIn = createCaptionButton(divOverlay, {
        backgroundData: "plus.png",
        className: "transparentButton",
        onClick: function () {
            var z = MxGetZoom(myMap, mapType);
            MxSetZoom(myMap, mapType, z + 1, {
                checkZoomRange: true,
                duration: 200
            });
        }
    });

    btZoomIn.css("position", "absolute");
    btZoomIn.css("bottom", "0.25em");
    btZoomIn.css("left", "4.25em");

    var btBack = createCaptionButton(divOverlay, {
        backgroundData: "left.png",
        className: "transparentButton",
        onClick: function () {
            if (params_.onBackClick)
                params_.onBackClick();
        }
    });

    btBack.css("position", "absolute");
    btBack.css("top", "0.25em");
    btBack.css("left", "0.25em");

    var btMenu = createCaptionButton(divOverlay, {
        backgroundData: "menu.png",
        className: "transparentButton",
        onClick: function () {
            if (params_.onMenuClick)
                params_.onMenuClick();
        }
    });

    btMenu.css("position", "absolute");
    btMenu.css("top", "0.25em");
    btMenu.css("right", "0.25em");

    var resultName = null;
    var resultDescription = null;

    addClickHandler(spanNext, {
        onClick: function () {
            if (params_.onNextClick)
                params_.onNextClick({
                    Kind: "Map",//"Building",
                    MapType: mapType,
                    Image: "Map.png",//"Building",
                    Point: MxGetCenter(myMap, mapType),
                    Name: resultName,
                    Description: resultDescription
                });
        }, transparent: true
    });

    addClickHandler(spanFindAddress, {
        onClick: function () {
            if (params_.onFindAddressFromClick)
                params_.onFindAddressFromClick();
        }, transparent: true
    });


    tb.css("width", "100%");
    tb.css("height", "100%");

    tdTop.css("height", "5%");
    tdBottom.css("height", "8%");


    tdCeil.css("vertical-align", "top");
    tdCenter.css("vertical-align", "middle");
    tdFloor.css("vertical-align", "bottom");

    setBackground(divOverlay, "marker.png", "3.0em");

    var idMap = "dvMap_" + generateUUID();
    divMap.attr("id", idMap);

    function clearAddress() {
        divFromAddress.html("&nbsp");
        spanNext.hide();
    }

    clearAddress();

    function detectAddress(lat_, lon_) {

        point = MxGetCenter(myMap, mapType);
        if (lat_ && lon_)
            point = [lat_, lon_];

        MxGeocode(myMap, mapType, point, {kind: 'house', results: 1, json: true}, function (res) {

                resultName = MxGetAddress(mapType, res);
                resultDescription = MxGetAddressDescription(mapType, res, resultName);

                if (resultName) {
                    divFromAddress.html(resultName);
                    spanNext.show();
                }
                else {
                    MxGeocode(myMap, mapType, point, {kind: 'locality', results: 1, json: true}, function (res) {

                            resultName = MxGetAddress(mapType, res);
                            resultDescription = MxGetAddressDescription(mapType, res, resultName);

                            if (resultName) {
                                divFromAddress.html(resultName);
                                spanNext.show();
                            }
                            else {
                                clearAddress();
                            }
                        },
                        function (err) {
                            clearAddress();
                        });
                }
            },
            function (err) {
                clearAddress();
            });
    }

    var skipTransparent = false;
    var gotoCenterCanceled = false;
    var lastCenter = mapStartCenter;

    function gotoCenter() {
        gotoCenterCanceled = false;

        function panToCenter() {
            skipTransparent = true;
            MxPanTo(myMap, mapType, convertPoint(mapType, lastCenter), {
                flying: false,
                safe: false
            }, function () {
                detectAddress();
                skipTransparent = false;
            }, function () {
                skipTransparent = false;
            });

        }

        if (lastCenter) {
            panToCenter();
        }

        cxGeoLocate(function (lat_, lon_, acc_) {
            log("geolocation success lat=" + lat_ + " lon=" + lon_ + " acc=" + acc_);
            lastCenter = [lat_, lon_];
            if (gotoCenterCanceled) {
                return;
            }
            panToCenter();
        }, function () {
            log("geolocation failed");
        }, 5000);

        /*
         if (navigator.geolocation) {
         navigator.geolocation.getCurrentPosition(
         function (position) {
         lastCenter = [position.coords.latitude, position.coords.longitude];
         if (gotoCenterCanceled) {
         return;
         }

         panToCenter();
         },
         function handle_error(err) {
         // не смогли определить координаты
         });
         }
         else {
         // не смогли определить координаты
         }
         */
    }

    uiShow(divWrapper, function () {

        var mapOptions = {idMap: idMap, mapStartCenter: mapStartCenter, zoom: 18}

        function ready(map_) {
            myMap = map_;

            $.each(divWrapper, function () {
                this.cxRefresh = function () {
                    MxFitToViewport(myMap, mapType);
                }
            });

            var transparentTimeout = null;

            MxAddEvent(myMap, mapType, "actionbegin", function (e) {
                if (skipTransparent)
                    return;

                gotoCenterCanceled = true;

                transparentTimeout = setTimeout(function () {
                    transparentTimeout = null;
                    //tb.fadeOut(200);
                    //divOverlay.animate({ "background-color": overlayTransparentBackground }, 200);
                    tb.hide();
                    btBack.hide();
                    btMenu.hide();
                    btCenter.hide();
                    btZoomIn.hide();
                    btZoomOut.hide();
                    divOverlay.css("background-color", overlayTransparentBackground);
                }, 200);
            });

            MxAddEvent(myMap, mapType, "actionend", function (e) {
                if (skipTransparent)
                    return;

                if (transparentTimeout) {
                    clearTimeout(transparentTimeout);
                    transparentTimeout = null;
                }
                else {
                    //tb.fadeIn(200);
                    //divOverlay.animate({ "background-color": overlayDefaultBackground }, 200);
                    tb.show();
                    btBack.show();
                    btMenu.show();
                    btCenter.show();
                    btZoomIn.show();
                    btZoomOut.show();
                    divOverlay.css("background-color", overlayDefaultBackground);
                }

                detectAddress();
            });

            MxAddEvent(myMap, mapType, "click", function (e) {
                skipTransparent = true;
                MxPanTo(myMap, mapType, MxGetCoords(e, mapType), null, function () {
                        detectAddress();
                        skipTransparent = false;
                    },
                    function () {
                        skipTransparent = false;
                    });
            });


            gotoCenter();

        }

        InitialMap(myMap, mapType, mapOptions, ready);

    });
}


function showAddressFinderList(params_) {
    log("showAddressFinderList: " + params_.caption);
    log(params_);

    var findID = null;

    var savedAddress = $.extend({}, params_.value);
    var currentAddress = $.extend({}, params_.value);

    if (IsNull(currentAddress.CityID)) {
        currentAddress.CityID = globalParams.KladrDefaultCityID,
            currentAddress.CityName = globalParams.KladrDefaultCityName
        currentAddress.CityDescription = globalParams.KladrDefaultCityDesription;
    }

    if (IsNull(currentAddress.Kind)) {
        currentAddress.Kind = params_.defaultMode;
    }

    if (currentAddress.Kind == "City" || currentAddress.Kind == "Street" || currentAddress.Kind == "Building")
        currentAddress.Kind = "Address";

    if (params_.allowedModes.indexOf(currentAddress.Kind) < 0 || currentAddress.Kind == "Map" || currentAddress.Kind == "Custom") {
        currentAddress.Kind = params_.defaultMode;
    }

    var addressChanged = false;

    var divWrapper = cxCreateDiv({className: "scrollableControl"});
    var params = {
        caption: params_.caption,
        buttons: {
            left: {
                backgroundData: "left.png",
                onClick: function () {
                    uiBack();
                }
            }
        }

    }
    createCaption(divWrapper, params);

    var divListBody = cxCreateDiv({className: "listBody", parent: divWrapper});

    var divToolbarItem = cxCreateDiv({parent: divListBody, className: "listItem listItemPadding"});
    divToolbarItem.css("padding-bottom", 0);
    divToolbarItem.css("text-align", "center")
    var btNear = createCaptionButton(divToolbarItem, {
        className: "selectorButton selectorButtonTab",
        size: "1.5em",
        onClick: function () {
            applyMode("Near");
        }
    });
    cxCreateImg({parent: btNear, src: getLocalURLPrefix() + "Images/Applications/from.png"});
    cxCreateSpan({className: "smallComment", parent: btNear, html: ControlsConsts.nearCaption});

    var btSearch = createCaptionButton(divToolbarItem, {
        className: "selectorButton selectorButtonTab",
        size: "1.5em",
        onClick: function () {
            applyMode("Address");
        }
    });
    cxCreateImg({parent: btSearch, src: getLocalURLPrefix() + "Images/Applications/search.png"});
    cxCreateSpan({className: "smallComment", parent: btSearch, html: ControlsConsts.searchCaption});

    var btHistory = createCaptionButton(divToolbarItem, {
        className: "selectorButton selectorButtonTab",
        size: "1.5em",
        onClick: function () {
            applyMode("History");
        }
    });
    cxCreateImg({parent: btHistory, src: getLocalURLPrefix() + "Images/Applications/created.png"});
    cxCreateSpan({className: "smallComment", parent: btHistory, html: ControlsConsts.historyCaption});

    var btMy = createCaptionButton(divToolbarItem, {
        className: "selectorButton selectorButtonTab",
        size: "1.5em",
        onClick: function () {
            applyMode("My");
        }
    });
    cxCreateImg({parent: btMy, src: getLocalURLPrefix() + "Images/Applications/my.png"});
    cxCreateSpan({className: "smallComment", parent: btMy, html: ControlsConsts.myCaption});

    var btMap = createCaptionButton(divToolbarItem, {
        className: "selectorButton selectorButtonTab",
        size: "1.5em",
        onClick: function () {
            applyMode("Map");
        }
    });
    cxCreateImg({parent: btMap, src: getLocalURLPrefix() + "Images/Applications/map.png"});
    cxCreateSpan({className: "smallComment", parent: btMap, html: ControlsConsts.mapCaption});

    var btCustom = createCaptionButton(divToolbarItem, {
        className: "selectorButton selectorButtonTab",
        size: "1.5em",
        onClick: function () {
            applyMode("Custom");
        }
    });
    cxCreateImg({parent: btCustom, src: getLocalURLPrefix() + "Images/Applications/question.png"});
    cxCreateSpan({className: "smallComment", parent: btCustom, html: ControlsConsts.dontKnowCaption});


    if (params_.allowedModes.indexOf("Near") < 0)
        btNear.hide();
    if (params_.allowedModes.indexOf("History") < 0)
        btHistory.hide();
    if (params_.allowedModes.indexOf("My") < 0)
        btMy.hide();
    if (params_.allowedModes.indexOf("Map") < 0)
        btMap.hide();
    if (params_.allowedModes.indexOf("Address") < 0)
        btSearch.hide();
    if (params_.allowedModes.indexOf("Custom") < 0)
        btCustom.hide();


    var _mode = null;

    function applyMode(mode_) {
        _mode = mode_;

        if (_mode != "Map") {
            divToolbarItem.detach();
            divListBody.empty();
            divListBody.append(divToolbarItem);
            divToolbarItem.children().removeClass("selectorButtonSelected");
        }

        if (_mode == "Near") {
            btNear.addClass("selectorButtonSelected");
            showNear();
        }
        else if (_mode == "History") {
            btHistory.addClass("selectorButtonSelected");
            showHistory();
        }
        else if (_mode == "My") {
            btMy.addClass("selectorButtonSelected");
            showMy();
        }
        else if (_mode == "Map") {
            //btMap.addClass("selectorButtonSelected");
            showMap();
        }
        else if (_mode == "Address") {
            btSearch.addClass("selectorButtonSelected");
            showSearch();
        }
        else if (_mode == "Custom") {
            btCustom.addClass("selectorButtonSelected");
            showCustom();
        }
    }

    applyMode(currentAddress.Kind);

    function showNear() {
        currentAddress.Kind = "Near";

        findObjects("Near", null, function (item_) {

            if (item_.Kind == "My" || params_.showAddressComments == false) {
                if (params_.onSelect)
                    params_.onSelect($.extend({}, item_));
            }
            else {
                showAddressComments({
                    caption: ControlsConsts.notesCapt,
                    value: item_,
                    onSelect: function (newAddress_) {
                        uiRemovePreLast();
                        if (params_.onSelect)
                            params_.onSelect($.extend({}, newAddress_));
                    }
                });
            }
        });
    }

    function showHistory() {
        currentAddress.Kind = "History";

        findObjects("History", null, function (item_) {
            if (params_.onSelect)
                params_.onSelect($.extend({}, item_));
        });
    }

    function showMy() {
        currentAddress.Kind = "My";

        findObjects("My", null, function (item_) {
            if (params_.onSelect)
                params_.onSelect($.extend({}, item_));
        });
    }

    function showCustom() {
        currentAddress.Kind = "Custom";

        addItem({
            Kind: "Custom",
            Name: ControlsConsts.sayDriver,
            Description: "",
            Image: "question.png"
        }, function (item_) {
            if (params_.onSelect)
                params_.onSelect($.extend({}, item_));
        });
    }

    function showMap() {
        showAddressPickerControl({
            caption: params_.caption,
            onContinue: function (result_) {

                var addr = $.extend({}, result_);

                if (params_.showAddressComments == false) {
                    uiRemovePreLast();
                    if (params_.onSelect)
                        params_.onSelect(addr);
                }
                else {
                    showAddressComments({
                        caption: ControlsConsts.notesCapt,
                        value: addr,
                        onSelect: function (newAddress_) {
                            uiRemovePreLast();
                            uiRemovePreLast();
                            if (params_.onSelect)
                                params_.onSelect(newAddress_);
                        }
                    });
                }
                /*
                 uiRemovePreLast();
                 if (params_.onSelect)
                 params_.onSelect();*/
            },
            onCallClick: params_.onCallClick
        });
    }

    var items = new Array();

    function showSearch() {

        currentAddress.Kind = "Address";
        addressChanged = false;

        function prepareAddressName() {
            var name = "";
            var descr = "";

            if (!IsNullOrEmpty(currentAddress.BuildingDescription)) {
                name = currentAddress.BuildingDescription;
            }
            else if (!IsNullOrEmpty(currentAddress.StreetDescription)) {
                name = currentAddress.StreetDescription;
            }
            else if (!IsNullOrEmpty(currentAddress.CityDescription)) {
                name = currentAddress.CityDescription;
            }

            currentAddress.Name = name;
            currentAddress.Description = name;
            currentAddress.Entrance = teInputEntrance.val();
            currentAddress.Notes = teInputNotes.val();
        }

        function localitySelect(item_) {
            log("localitySelect");
            log(item_);
            teInputLocality.val(item_.Name);
            currentAddress.CityID = item_.ID;
            currentAddress.CityName = item_.Name;
            currentAddress.CityDescription = item_.Description;
            currentAddress.StreetID = null;
            currentAddress.StreetName = "";
            currentAddress.StreetDescription = "";
            currentAddress.BuildingID = null;
            currentAddress.BuildingName = "";
            currentAddress.BuildingDescription = "";
            currentAddress.Point = null;  //всё-таки нужно сделать
            currentAddress.Entrance = "";
            teInputStreet.val("");
            savedStreet = null;
            teInputBuilding.val("");
            savedBuilding = null;
            teInputEntrance.val("");
            clearItems();
            divSearchStreet.show();
            teInputStreet.focus();
            prepareAddressName();
            addressChanged = true;
        }

        function streetSelect(item_) {
            log("streetSelect");
            log(item_);

            if (item_.Kind != "Street") {

                if (item_.Kind == "My" || params_.showAddressComments == false) {
                    if (params_.onSelect)
                        params_.onSelect($.extend({}, item_));
                }
                else {
                    showAddressComments({
                        caption: ControlsConsts.notesCapt,
                        value: item_,
                        onSelect: function (newAddress_) {
                            uiRemovePreLast();
                            if (params_.onSelect)
                                params_.onSelect($.extend({}, newAddress_));
                        }
                    });
                }

                return;
            }

            teInputStreet.val(item_.Name);
            currentAddress.StreetID = item_.ID;
            currentAddress.StreetName = item_.Name;
            currentAddress.StreetDescription = item_.Description;
            currentAddress.BuildingID = null;
            currentAddress.BuildingName = "";
            currentAddress.BuildingDescription = "";
            currentAddress.Point = null;
            currentAddress.Entrance = "";
            teInputBuilding.val("");
            savedBuilding = null;
            teInputEntrance.val("");
            clearItems();
            divSearchBuilding.show();
            teInputBuilding.focus();
            prepareAddressName();
            addressChanged = true;

            findObjects("Building", "", buildingSelect, function () {
                $('body').animate({
                    scrollTop: divSearchBuilding.offset().top
                }, 200);
            });
        }

        function buildingSelect(item_) {
            log("buildingSelect");
            log(item_);
            teInputBuilding.val(item_.Name);
            currentAddress.BuildingID = item_.ID;
            currentAddress.BuildingName = item_.Name;
            currentAddress.BuildingDescription = item_.Description;
            currentAddress.Entrance = "";
            teInputEntrance.val("");
            clearItems();
            divSearchEntrance.show();
            divApplyCustomAddress.show();
            divSearchNotes.show();
            teInputEntrance.focus();
            prepareAddressName();
            addressChanged = true;
        }

        var timeout = null;

        var divSearchLocality = cxCreateDiv({parent: divListBody, className: "listItem"});

        var teInputLocality = cxCreateListItemInput({
            parent: divSearchLocality,
            image: "locality.png",
            placeholder: ControlsConsts.cityCaption,
            onEnterPressed: function () {
                if (items.length == 1) {
                    localitySelect(items[0]);
                }
                else {
                    var counter = 0;
                    var item = null;
                    $.each(items, function () {
                        if (this.Name.toUpperCase() == teInputLocality.val().toUpperCase()) {
                            counter++;
                            item = this;
                        }
                    });
                    if (counter == 1)
                        localitySelect(item);
                    else
                        return false;
                }
            }
        });

        var savedLocality = null;
        teInputLocality.on("input", function () {

            if (savedLocality == teInputLocality.val())
                return;
            savedLocality = teInputLocality.val();

            clearItems();
            divSearchStreet.hide();
            divSearchBuilding.hide();
            divSearchEntrance.hide();
            divApplyCustomAddress.hide();
            divSearchNotes.hide();
            if (timeout != null)
                clearTimeout(timeout);

            timeout = setTimeout(function () {
                findObjects("Locality", teInputLocality.val(), localitySelect, function () {
                    $('body').animate({
                        scrollTop: divSearchLocality.offset().top
                    }, 200);
                });
            }, 500);
        });

        var divSearchStreet = cxCreateDiv({parent: divListBody, className: "listItem"});
        var teInputStreet = cxCreateListItemInput({
            parent: divSearchStreet,
            image: "street.png",
            placeholder: ControlsConsts.streetCaption,
            onEnterPressed: function () {
                if (items.length == 1) {
                    streetSelect(items[0]);
                }
                else {
                    var counter = 0;
                    var item = null;
                    $.each(items, function () {
                        if (this.Name.toUpperCase() == teInputStreet.val().toUpperCase()) {
                            counter++;
                            item = this;
                        }
                    });
                    if (counter == 1)
                        streetSelect(item);
                    else
                        return false;
                }
            }
        });

        var savedStreet = null;
        teInputStreet.on("input", function () {

            if (savedStreet == teInputStreet.val())
                return;
            savedStreet = teInputStreet.val();

            clearItems();

            divSearchBuilding.hide();
            divSearchEntrance.hide();
            divApplyCustomAddress.hide();
            divSearchNotes.hide();
            if (timeout != null)
                clearTimeout(timeout);

            timeout = setTimeout(function () {
                findObjects("Street", teInputStreet.val(), streetSelect, function () {
                    $('body').animate({
                        scrollTop: divSearchStreet.offset().top
                    }, 200);
                });
            }, 500);
        });

        var divSearchBuilding = cxCreateDiv({parent: divListBody, className: "listItem"});
        var teInputBuilding = cxCreateListItemInput({
            parent: divSearchBuilding,
            image: "building.png",
            placeholder: ControlsConsts.houseCaption,
            onEnterPressed: function () {
                if (items.length == 1) {
                    buildingSelect(items[0]);
                }
                else {
                    var counter = 0;
                    var item = null;
                    $.each(items, function () {
                        if (this.Name.toUpperCase() == teInputBuilding.val().toUpperCase()) {
                            counter++;
                            item = this;
                        }
                    });
                    if (conter == 1)
                        buildingSelect(item);
                    else
                        return false;
                }
            }
        });

        var savedBuilding = null;
        teInputBuilding.on("input", function () {

            if (savedBuilding == teInputBuilding.val())
                return;
            savedBuilding = teInputBuilding.val();

            clearItems();
            divSearchEntrance.hide();
            divApplyCustomAddress.hide();
            divSearchNotes.hide();
            if (timeout != null)
                clearTimeout(timeout);

            timeout = setTimeout(function () {
                findObjects("Building", teInputBuilding.val(), buildingSelect, function () {
                    $('body').animate({
                        scrollTop: divSearchBuilding.offset().top
                    }, 200);
                });
            }, 500);
        });

        var divSearchEntrance = cxCreateDiv({parent: divListBody, className: "listItem"});
        var teInputEntrance = cxCreateListItemInput({
            parent: divSearchEntrance,
            image: "entrance.png",
            placeholder: ControlsConsts.entranceCaption,
            onEnterPressed: function () {
                teInputNotes.focus();
            }
        });

        teInputEntrance.on("input", function () {
            prepareAddressName();
            addressChanged = true;
        });

        var divSearchNotes = cxCreateDiv({parent: divListBody, className: "listItem"});
        var teInputNotes = cxCreateListItemInput({
            parent: divSearchNotes,
            image: "info.png",
            placeholder: ControlsConsts.addressInfo,
            onEnterPressed: function () {
                if (params_.onSelect)
                    params_.onSelect($.extend({}, currentAddress));
            }
        });

        teInputNotes.on("input", function () {
            prepareAddressName();
            addressChanged = true;
        });

        var divApplyCustomAddress = cxCreateDiv({parent: divListBody, className: "listItem listItemPadding"});
        divApplyCustomAddress.css("border", "none");
        divApplyCustomAddress.css("text-align", "center")
        var btApply = createCaptionButton(divApplyCustomAddress, {
            className: "selectorButtonBig",
            size: "1.5em",
            onClick: function () {
                if (params_.onSelect) {
                    params_.onSelect($.extend({}, currentAddress));
                }
            }
        });
        cxCreateImg({parent: btApply, src: getLocalURLPrefix() + "Images/Applications/apply.png"});
        cxCreateSpan({parent: btApply, html: ControlsConsts.applyCaption});

        /*
         var divApplyCustomAddress = cxCreateDiv({ className: "listItem", parent: divListBody });
         var btContinue = createButton(divApplyCustomAddress, {
         caption: "Применить",
         onClick: function () {
         if (params_.onSelect) {
         params_.onSelect($.extend({}, currentAddress));
         }
         }
         });
         btContinue.addClass("greenButton");
         btContinue.css("margin", "0");
         btContinue.css("width", "100%");
         divApplyCustomAddress.css("text-align", "center");
         divApplyCustomAddress.css("border", "none");
         */
        log("restoring address");
        log(currentAddress);

        if (IsNull(currentAddress.CityID)) {
            divSearchStreet.hide();
            divSearchBuilding.hide();
            divSearchEntrance.hide();
            divSearchNotes.hide();
            divApplyCustomAddress.hide();
        }
        else {
            savedLocality = currentAddress.CityName;
            teInputLocality.val(currentAddress.CityName);
        }

        if (IsNull(currentAddress.StreetID)) {
            divSearchBuilding.hide();
            divSearchEntrance.hide();
            divSearchNotes.hide();
            divApplyCustomAddress.hide();
        }
        else {
            savedStreet = currentAddress.StreetName;
            teInputStreet.val(currentAddress.StreetName);
        }

        if (IsNull(currentAddress.BuildingID)) {
            divSearchEntrance.hide();
            divSearchNotes.hide();
            divApplyCustomAddress.hide();
        }
        else {
            savedBuilding = currentAddress.BuildingName;
            teInputBuilding.val(currentAddress.BuildingName);
        }
        teInputEntrance.val(currentAddress.Entrance);
        teInputNotes.val(currentAddress.Notes);

        prepareAddressName();

    }

    function addItem(item_, clickCallback_) {

        item_.Description = getAddressDescription(item_);

        items.push(item_);

        var divItem = cxCreateDiv({className: "listItem", parent: divListBody});
        divItem.prop("cxData", item_);

        cxCreateListItemContent({
            parent: divItem,
            item: item_,
            onClick: function () {
                clickCallback_(item_);
            },
        });

        /*

         var html = getNameDesctiptionHtml(item_);

         var divItem = cxCreateDiv({ className: "listItem" });
         var divPic = cxCreateDiv({ className: "listItemIcon", parent: divItem });

         if (item_.Image) {
         setBackground(divPic, item_.Image.toLowerCase(), picSizeEm);
         }

         var divText = cxCreateDiv({ className: "listItemText", parent: divItem, html: html });

         divItem.prop("cxData", item_);

         divListBody.append(divItem);
         addClickHandler(divItem, {
         onClick: function () {
         clickCallback_(item_);
         }
         });*/
    }

    function clearItems() {
        items = new Array();

        divListBody.children().each(function () {
            if (this.cxData)
                $(this).remove();
        });
    }

    function findObjects(mode_, filter_, clickCallback_, finishedCallBack_) {
        clearItems();

        var savedID = generateUUID();
        findID = savedID;

        var savedMode = currentAddress.Kind;
        cxAjax({
            url: getDataURLPrefix() + "/data/GlobalAction?ActionName=TaxiClient_GetAddresses",
            params: {
                FilterText: filter_,
                Kind: mode_,
                AllowedModes: params_.allowedModes,
                RegionId: globalParams.KladrDefaultRegionID,
                CityId: currentAddress.CityID,
                StreetId: currentAddress.StreetID,
                Lon: mapStartCenter[1],
                Lat: mapStartCenter[0]
            },
            onSuccess: function (result) {
                if (savedMode != currentAddress.Kind)
                    return;

                if (savedID != findID)
                    return;

                var answer = $.parseJSON(result);

                if (answer && answer.length) {
                    $.each(answer, function () {
                        addItem(this, clickCallback_);
                    });
                }

                if (finishedCallBack_)
                    finishedCallBack_();

                if (mode_ == "Building" && !IsNullOrEmpty(filter_) && answer.length == 0) {

                    var description = "";

                    if (!IsNullOrEmpty(currentAddress.StreetDescription)) {
                        description = currentAddress.StreetDescription + ", ";
                    }
                    else if (!IsNullOrEmpty(currentAddress.CityDescription)) {
                        description = currentAddress.StreetDescription + ", ";
                    }

                    description += ControlsConsts.houseCapt + " " + filter_;

                    var item = {
                        ID: null,
                        Kind: "Building",
                        KindStr: null,
                        Image: "Building.png",
                        Point: null,
                        Name: filter_ + ControlsConsts.notInBase,
                        Type: ControlsConsts.houseCapt,
                        Description: description,
                    };
                    addItem(item, clickCallback_);
                }
            }
        });
    }

    uiShow(divWrapper);
}

function showAddressComments(params_) {
    log("showAddressComments: " + params_.caption);
    log(params_);

    var divWrapper = cxCreateDiv({className: "scrollableControl"});
    var params = {
        caption: params_.caption,
        buttons: {
            left: {
                backgroundData: "left.png",
                onClick: function () {
                    uiBack();
                }
            }
        }

    }
    createCaption(divWrapper, params);

    var divListBody = cxCreateDiv({className: "listBody", parent: divWrapper});

    var listItem = params_.value;

    var divItem = cxCreateDiv({className: "listItem", parent: divListBody});

    cxCreateListItemContent({
        parent: divItem,
        item: listItem
    });
    function apply() {

        var newAddress = $.extend({}, params_.value);
        newAddress.Entrance = teInputEntrance.val();
        newAddress.Notes = teInputNotes.val();

        if (params_.onSelect)
            params_.onSelect(newAddress);
    }

    var divSearchEntrance = cxCreateDiv({parent: divListBody, className: "listItem"});
    var teInputEntrance = cxCreateListItemInput({
        parent: divSearchEntrance,
        image: "entrance.png",
        placeholder: ControlsConsts.entranceCaption,
        onEnterPressed: function () {
            teInputNotes.focus();
        }
    });

    var divSearchNotes = cxCreateDiv({parent: divListBody, className: "listItem"});
    var teInputNotes = cxCreateListItemInput({
        parent: divSearchNotes,
        image: "info.png",
        placeholder: ControlsConsts.addressInfo,
        onEnterPressed: apply
    });

    var divApplyCustomAddress = cxCreateDiv({parent: divListBody, className: "listItem listItemPadding"});
    divApplyCustomAddress.css("border", "none");
    divApplyCustomAddress.css("text-align", "center")
    var btApply = createCaptionButton(divApplyCustomAddress, {
        className: "selectorButtonBig selectorButtonSelected",
        size: "1.5em",
        onClick: apply
    });
    cxCreateImg({parent: btApply, src: getLocalURLPrefix() + "Images/Applications/apply.png"});
    cxCreateSpan({parent: btApply, html: ControlsConsts.applyCaption});

    /*
     var divApplyCustomAddress = cxCreateDiv({ className: "listItem", parent: divListBody });
     var btContinue = createButton(divApplyCustomAddress, {
     caption: "Применить",
     onClick: apply
     });
     btContinue.addClass("greenButton");
     btContinue.css("margin", "0");
     btContinue.css("width", "100%");
     divApplyCustomAddress.css("text-align", "center");
     divApplyCustomAddress.css("border", "none");
     */
    teInputEntrance.val(params_.value.Entrance);
    teInputNotes.val(params_.value.Notes);

    uiShow(divWrapper);
}

function showOrderConfirmation(params_) {
    log("showOrderConfirmation: " + params_.caption);

    var mode = "default";
    var cityID = globalParams.KladrDefaultCityID;
    var divCity = null;

    var divWrapper = cxCreateDiv({className: "scrollableControl"});

    createCaption(divWrapper, params_)

    var divListBody = cxCreateDiv({className: "listBody", parent: divWrapper});

    // Откуда
    var divItemFrom = cxCreateDiv({className: "listItem", parent: divListBody});
    var divItemFromText = cxCreateListItemContent({
        parent: divItemFrom,
        item: {
            Image: "addr_from.png",
            rightButton: {
                Image: "info.png",
                onClick: function () {
                    showAddressComments({
                        caption: ControlsConsts.notesCapt,
                        value: params_.order.addressFrom,
                        onSelect: function (newAddress_) {
                            params_.order.addressFrom = newAddress_;
                            updateOrder();
                            uiBack();
                        }
                    });
                }
            }
        },
        onClick: function () {
            if (params_.onEdit)
                params_.onEdit("addressFrom", params_.order, updateOrder);
        },
    });


    // Куда
    var addressToItems = new Array();

    // Время
    divItemTime = cxCreateDiv({className: "listItem", parent: divListBody});
    var divItemTimeText = cxCreateListItemContent({
        parent: divItemTime,
        item: {
            Image: "time.png",
        },
        onClick: function () {
            if (params_.onEdit)
                params_.onEdit("time", params_.order, updateOrder);
        },
    });

    /*
     setBackground(cxCreateDiv({ className: "listItemIcon", parent: divItemTime }), "time.png", picSizeEm);
     var divItemTimeText = cxCreateDiv({ className: "listItemText", parent: divItemTime });
     addClickHandler(divItemTime, {
     onClick: function () {
     if (params_.onEdit)
     params_.onEdit("time", params_.order, updateOrder);
     }
     });*/

    // Тариф
    var divItemTariff = cxCreateDiv({className: "listItem", parent: divListBody});
    var divItemTariffText = cxCreateListItemContent({
        parent: divItemTariff,
        item: {
            Image: "tariff.png",
        },
        onClick: function () {
            if (params_.onEdit)
                params_.onEdit("tariff", params_.order, updateOrder);
        },
    });
    /*
     setBackground(cxCreateDiv({ className: "listItemIcon", parent: divItemTariff }), "tariff.png", picSizeEm);
     var divItemTariffText = cxCreateDiv({ className: "listItemText", parent: divItemTariff });
     addClickHandler(divItemTariff, {
     onClick: function () {
     if (params_.onEdit)
     params_.onEdit("tariff", params_.order, updateOrder);
     }
     });
     */
    // Пожелания
    /*
     divItemOptions = cxCreateDiv({ className: "listItem", parent: divListBody });
     var divItemOptionsText = cxCreateListItemContent({
     parent: divItemOptions,
     item: {
     Image: "options.png",
     },
     onClick: function () {
     if (params_.onEdit)
     params_.onEdit("options", params_.order, updateOrder);
     },
     onHold: function () {
     if (params_.onHold)
     params_.onHold("options", params_.order, updateOrder);
     },
     });
     /*
     setBackground(cxCreateDiv({ className: "listItemIcon", parent: divItemOptions }), "options.png", picSizeEm);
     var divItemOptionsText = cxCreateDiv({ className: "listItemText", parent: divItemOptions });

     addClickHandler(divItemOptions, {
     onClick: function () {
     if (params_.onEdit)
     params_.onEdit("options", params_.order, updateOrder);
     },
     onHold: function () {
     if (params_.onHold)
     params_.onHold("options", params_.order, updateOrder);
     }
     });
     */
    // Оплата
    /*
     divItemPayment = cxCreateDiv({ className: "listItem", parent: divListBody });
     var divItemPaymentText = cxCreateListItemContent({
     parent: divItemPayment,
     item: {
     Image: "payment.png",
     },
     onClick: function () {
     if (params_.onEdit)
     params_.onEdit("payment", params_.order, updateOrder);
     },
     });
     /*
     setBackground(cxCreateDiv({ className: "listItemIcon", parent: divItemPayment }), "payment.png", picSizeEm);
     var divItemPaymentText = cxCreateDiv({ className: "listItemText", parent: divItemPayment });
     addClickHandler(divItemPayment, {
     onClick: function () {
     if (params_.onEdit)
     params_.onEdit("payment", params_.order, updateOrder);
     }
     });
     */
    // Примечания
    divItemNotes = cxCreateDiv({className: "listItem", parent: divListBody});
    var divItemNotesText = cxCreateListItemContent({
        parent: divItemNotes,
        item: {
            Image: "notes.png",
        },
        onClick: function () {
            if (params_.onEdit)
                params_.onEdit("notes", params_.order, updateOrder);
        },
    });
    /*
     setBackground(cxCreateDiv({ className: "listItemIcon", parent: divItemNotes }), "notes.png", picSizeEm);
     var divItemNotesText = cxCreateDiv({ className: "listItemText", parent: divItemNotes });
     addClickHandler(divItemNotes, {
     onClick: function () {
     if (params_.onEdit)
     params_.onEdit("notes", params_.order, updateOrder);
     }
     });
     */
    // Стоимость
    divItemCost = cxCreateDiv({className: "listItem", parent: divListBody});
    var divItemCostText = cxCreateListItemContent({
        parent: divItemCost,
        item: {
            Image: "money.png",
            Clickable: false
        },
    });

    var divToolbarItem = cxCreateDiv({parent: divListBody, className: "listItem listItemPadding"});
    divToolbarItem.css("border", "none");
    divToolbarItem.css("text-align", "center");

    var divToolbarItem = cxCreateDiv({
        parent: divListBody,
        className: "listItem listItemPadding",
    });
    divToolbarItem.css("border", "none");
    divToolbarItem.css("text-align", "center");

    if (params_.company) {

        cxCreateDiv({
            parent: divToolbarItem,
            className: "payment-caption",
            html: ControlsConsts.paymentMethodCaption
        });
        var btApply = createCaptionButton(divToolbarItem, {
            className: "orderButtonBig",
            size: "1.5em",
            caption: ControlsConsts.cash,
            onClick: function () {
                if (params_.order.addressFrom.Kind == "Unknown" || params_.order.addressFrom.Kind == "Custom") {
                    showMessage({
                        message: ControlsConsts.selectAddressToMessage,
                        onButtonClick: function () {
                            uiBack();
                        }
                    });
                }
                else {
                    params_.onContinue(params_.order);
                }
            }
        });

        //cxCreateImg({parent: btApply, src: getLocalURLPrefix() + "Images/Applications/apply.png"});
        //cxCreateSpan({parent: btApply, html: ControlsConsts.cash});

        var btApplyCorporation = createCaptionButton(divToolbarItem, {
            className: "orderButtonBig",
            size: "1.5em",
            caption: ControlsConsts.businessTrip,
            onClick: function () {
                if (params_.order.addressFrom.Kind == "Unknown" || params_.order.addressFrom.Kind == "Custom") {
                    showMessage({
                        message: ControlsConsts.selectAddressToMessage,
                        onButtonClick: function () {
                            uiBack();
                        }
                    });
                }
                else {

                    params_.order.notes = params_.company.id + "companyId;" + params_.order.notes;
                    params_.order.corporative = true;
                    params_.onContinue(params_.order);
                }
            }
        });
        //cxCreateImg({parent: btApplyCorporation, src: getLocalURLPrefix() + "Images/Applications/apply.png"});
        //cxCreateSpan({parent: btApplyCorporation, html: ControlsConsts.businessTrip});
    } else {
        var btApply = createCaptionButton(divToolbarItem, {
            className: "orderButtonBig",
            size: "1.5em",
            onClick: function () {
                if (params_.order.addressFrom.Kind == "Unknown" || params_.order.addressFrom.Kind == "Custom") {
                    showMessage({
                        message: ControlsConsts.selectAddressToMessage,
                        onButtonClick: function () {
                            uiBack();
                        }
                    });
                }
                else {
                    params_.onContinue(params_.order);
                }
            }
        });

        //cxCreateImg({parent: btApply, src: getLocalURLPrefix() + "Images/Applications/apply.png"});
        cxCreateSpan({parent: btApply, html: ControlsConsts.getTaxiCaption});
    }
    /*
     var divContinue = cxCreateDiv({ className: "listItem", parent: divListBody });
     var btContinue = createButton(divContinue, {
     caption: "Заказать такси",
     onClick: function () {
     if (params_.order.addressFrom.Kind == "Unknown" || params_.order.addressFrom.Kind == "Custom") {
     showMessage({
     message: "Пожалуйста, укажите адрес Откуда",
     onButtonClick: uiBack
     });
     }
     else {
     params_.onContinue(params_.order);
     }
     }
     });
     btContinue.addClass("greenButton");
     btContinue.css("margin", "0");
     btContinue.css("width", "100%");
     divContinue.css("text-align", "center");
     divContinue.css("border", "none");
     */

    function updateOrder() {
        var htmlFrom = getNameDesctiptionHtml(params_.order.addressFrom, getAddressDescription(params_.order.addressFrom));
        divItemFromText.html(htmlFrom);

        $.each(addressToItems, function () {
            this.remove();
        });
        addressToItems = new Array();
        var prevItem = divItemFrom;
        var index = 0;
        $.each(params_.order.addressTo, function () {
            var address = this;
            var divItemTo = cxCreateDiv({className: "listItem"});
            prevItem.after(divItemTo);
            prevItem = divItemTo;

            var htmlTo = getNameDesctiptionHtml(address, getAddressDescription(address));
            var rightButton = null;

            if (address.Kind != "Custom") {
                rightButton = {
                    Image: "add.png",
                    onClick: function () {
                        var addedAddress = {};

                        showAddressFinderList({
                            allowedModes: "Address|My|History|Map|Custom",
                            defaultMode: "Address",
                            caption: ControlsConsts.toCaption,
                            showAddressComments: false,
                            value: $.extend({}, addedAddress),
                            onSelect: function (address_) {
                                params_.order.addressTo.splice(savedIndex + 1, 0, address_);
                                updateOrder();
                                uiBack();
                            },
                            onCallClick: params_.onCallClick,
                            buttons: params_.buttons
                        });
                    }, transparent: true
                }
            }

            var savedIndex = index;
            var divItemToText = cxCreateListItemContent({
                parent: divItemTo,
                html: htmlTo,
                item: {
                    Image: (index == params_.order.addressTo.length - 1 ? "addr_to.png" : "addr_inter.png"),
                    rightButton: rightButton,
                },
                onClick: function () {
                    if (params_.onEdit)
                        params_.onEdit("addressTo", params_.order, updateOrder, savedIndex);
                },
                onHold: function (item_) {
                    if (params_.order.addressTo.length <= 1) {
                        showConfirmation({
                            caption: ControlsConsts.confirmCaption,
                            message: ControlsConsts.clearAddressConfirmation + address.Name + "?",
                            onButtonYesClick: function () {
                                params_.order.addressTo[savedIndex].Kind = "Custom";
                                params_.order.addressTo[savedIndex].Name = ControlsConsts.toCaption;
                                params_.order.addressTo[savedIndex].Description = ControlsConsts.notNecessaryCaption;
                                updateOrder();
                                uiBack();
                            },
                            onButtonNoClick: function () {
                                uiBack();
                            }
                        });
                    }
                    else {
                        showConfirmation({
                            caption: ControlsConsts.confirmCaption,
                            message: ControlsConsts.deleteAddressConfirmation + address.Name + "?",
                            onButtonYesClick: function () {
                                params_.order.addressTo.splice(savedIndex, 1);
                                updateOrder();
                                uiBack();
                            },
                            onButtonNoClick: function () {
                                uiBack();
                            }
                        });
                    }
                },
            });
            index++;

            addressToItems.push(divItemTo);
        });


        var htmlTime = getNameDesctiptionHtml(params_.order.time);
        divItemTimeText.html(htmlTime);

        var htmlTariff = getNameDesctiptionHtml(params_.order.tariff);
        divItemTariffText.html(htmlTariff);

        var htmlOptions = "";

        if (params_.order.options) {
            $.each(params_.order.options, function () {
                htmlOptions += this.Name + ", ";
            });
        }

        if (htmlOptions.length > 2)
            htmlOptions = htmlOptions.substr(0, htmlOptions.length - 2);

        var notes = ControlsConsts.extraCaption;
        if (IsNullOrEmpty(htmlOptions)) {
            if (!IsNullOrEmpty(params_.order.notes))
                notes = params_.order.notes;
        }
        else {
            if (IsNullOrEmpty(params_.order.notes))
                notes = htmlOptions;
            else
                notes = htmlOptions + ", " + params_.order.notes;
        }

        var item = {
            Name: notes,
            Description: ControlsConsts.wishesPaymentPhoneNotesCaption
        }

        divItemNotesText.html(getNameDesctiptionHtml(item));

        divItemCost.hide();

        if (params_.order.addressTo.Kind != "Custom") {
            cxAjax({
                url: getDataURLPrefix() + "/data/GlobalAction?ActionName=TaxiClient_PreCalculateOrder",
                params: params_.order,
                background: true,
                onSuccess: function (result) {
                    console.log(result);
                    var r = $.parseJSON(result);
                    if (!IsNull(r) && !IsNullOrEmpty(r.Name)) {
                        var htmlCost = getNameDesctiptionHtml(r);
                        divItemCostText.html(htmlCost);
                        divItemCost.show();
                    }
                },
                onError: function (result) {
                    console.log('error');
                }
            });
        }
    }

    updateOrder();

    uiShow(divWrapper);

}

function showTimeSelector(params_) {
    log("showTimeSelector: " + params_.caption);

    var divWrapper = cxCreateDiv({className: "scrollableControl"});

    var customSelectedItem = null;

    createCaption(divWrapper, params_)

    var divListBody = cxCreateDiv({className: "listBody", parent: divWrapper});

    var listItems = new Array();

    listItems.push({
        Kind: "Now",
        Name: ControlsConsts.nowCapt,
        Image: "now.png"

    });
    listItems.push({
        Kind: "InMinutes",
        Minutes: 10,
        Name: ControlsConsts.inTenMinutes,
        Image: "10min.png"
    });
    listItems.push({
        Kind: "InMinutes",
        Minutes: 20,
        Name: ControlsConsts.inTwentyMinutes,
        Image: "20min.png"

    });
    listItems.push({
        Kind: "InMinutes",
        Minutes: 30,
        Name: ControlsConsts.inThirtyMinutes,
        Image: "30min.png"

    });

    $.each(listItems, function () {
        var listItem = this;

        var divItem = cxCreateDiv({className: "listItem", parent: divListBody});

        var divItemText = cxCreateListItemContent({
            parent: divItem,
            item: listItem,
            onClick: function () {
                params_.onContinue(listItem);
            }
        });
        /*
         setBackground(cxCreateDiv({ className: "listItemIcon", parent: divItem }), listItem.Image, picSizeEm);
         var divItemText = cxCreateDiv({ className: "listItemText", parent: divItem, html: html });
         addClickHandler(divItem, {
         onClick: function () {
         params_.onContinue(listItem);
         }
         });*/
    });


    var listCustomItem = {
        Kind: "Custom",
    };

    if (params_.initialValue && params_.initialValue.Kind == "Custom") {
        listCustomItem.Date = params_.initialValue.Date;
        listCustomItem.Hour = params_.initialValue.Hour;
        listCustomItem.Minute = params_.initialValue.Minute;
    }
    else {
        var today = new Date();
        var initialValue = new Date();
        initialValue.setMinutes(initialValue.getMinutes() + 20);
        initialValue.setMinutes(Math.round(initialValue.getMinutes() / 5) * 5);


        if (today.getDate() == initialValue.getDate())
            listCustomItem.Date = ControlsConsts.todayCaption;
        else
            listCustomItem.Date = ControlsConsts.tomorrowCaption;

        listCustomItem.Hour = initialValue.getHours();
        listCustomItem.Minute = initialValue.getMinutes();
    }

    listCustomItem.DateLocal = listCustomItem.Date;

    var divCaptions = cxCreateDiv({className: "listItem", parent: divListBody});
    divCaptions.css("border", "none");
    var divCaptionDate = cxCreateDiv({parent: divCaptions});
    divCaptionDate.html(ControlsConsts.dateCaption);
    divCaptionDate.addClass("timeSelectorCaption");
    divCaptionDate.css("width", "50%")
    var divCaptionHour = cxCreateDiv({parent: divCaptions});
    divCaptionHour.html(ControlsConsts.hoursCaption);
    divCaptionHour.addClass("timeSelectorCaption");
    divCaptionHour.css("width", "25%")
    var divCaptionMinutes = cxCreateDiv({parent: divCaptions});
    divCaptionMinutes.html(ControlsConsts.minutesCaption);
    divCaptionMinutes.addClass("timeSelectorCaption");
    divCaptionMinutes.css("width", "25%")


    var divListItem = cxCreateDiv({className: "listItem", parent: divListBody});
    var domDivListItem = divListItem.get(0);


    function updateCustom() {
        var min = listCustomItem.Minute;
        if (min < 10 && min.length < 2) {
            min = "0" + min;
        }

        listCustomItem.Name = listCustomItem.DateLocal + ", " + listCustomItem.Hour + ":" + min;

        customSelectedItem = $.extend({}, listCustomItem);
    }

    // Дата
    {
        var divDaySelector = cxCreateDiv({parent: divListItem});
        divDaySelector.addClass("timeSelector");
        divDaySelector.css("width", "50%");

        var divDayOverlay = cxCreateDiv({parent: divDaySelector});
        divDayOverlay.addClass("timeOverlay");


        var divDayWheel = cxCreateDiv({parent: divDaySelector});
        divDayWheel.addClass("timeWheel");
        divDayWheel.css("text-align", "center");

        var divDays = new Array();

        var divDayStub = cxCreateDiv({parent: divDayWheel});
        divDayStub.html("&nbsp");
        divDayStub.addClass("timeStub");
        var divDayToday = cxCreateDiv({parent: divDayWheel});
        divDayToday.html(ControlsConsts.todayCaption);
        divDayToday.addClass("timeItem");
        divDayToday.prop("cxDisplay", ControlsConsts.todayCaption);
        divDayToday.prop("cxDisplayEx", "Сегодня"); // не локализовать!
        divDays.push(divDayToday);
        var divDayTomorrow = cxCreateDiv({parent: divDayWheel});
        divDayTomorrow.html(ControlsConsts.tomorrowCaption);
        divDayTomorrow.addClass("timeItem");
        divDayTomorrow.prop("cxDisplay", ControlsConsts.tomorrowCaption);
        divDayTomorrow.prop("cxDisplayEx", "Завтра"); // не локализовать!
        divDays.push(divDayTomorrow);

        var d = new Date();
        d.setDate(d.getDate() + 1);

        for (var i = 0; i < 30; i++) {
            d.setDate(d.getDate() + 1);
            var html = d.getDate() + " " + _months[d.getMonth()];
            var descr = d.getDate() + " " + _monthsFull[d.getMonth()];
            var descrEx = d.getDate() + " " + _monthsFull[d.getMonth()];
            var divDay = cxCreateDiv({parent: divDayWheel});
            divDay.html(html);
            divDay.addClass("timeItem");
            divDay.prop("cxDisplay", descr);
            divDay.prop("cxDisplayEx", descrEx);
            divDays.push(divDay);
        }

        var divDayStub2 = cxCreateDiv({parent: divDayWheel});
        divDayStub2.html("&nbsp");
        divDayStub2.addClass("timeStub");

        var domDivDay = divDayStub.get(0);
        var domDivDaySelector = divDaySelector.get(0);

        var dayScrolling = false;
        var selectedDay = null;

        function selectDay(day_) {
            if (day_ == selectedDay)
                return;

            var duration = 200;
            if (selectedDay == null)
                duration = 0;

            selectedDay = day_;

            var top = day_.get(0).offsetTop - divDayWheel.get(0).offsetTop - day_.get(0).offsetHeight;
            dayScrolling = true;

            day_.parent().children().removeClass("timeItemSelected");
            day_.addClass("timeItemSelected");

            divDaySelector.animate({scrollTop: top}, duration, null, function () {
                dayScrolling = false;

                listCustomItem.Date = day_.prop("cxDisplayEx");
                listCustomItem.DateLocal = day_.prop("cxDisplay");
                updateCustom();
            });
        }

        var timerDaySelector = null;
        $.each(divDays, function () {
            var div = this;
            addClickHandler(div, {
                onClick: function () {
                    if (timerDaySelector)
                        clearTimeout(timerDaySelector);
                    selectDay(div);
                }, transparent: true
            });
        });

        divDaySelector.scroll(function () {
            if (timerDaySelector)
                clearTimeout(timerDaySelector);
            timerDaySelector = setTimeout(function () {
                var div = divDays[Math.max(0, Math.min(divDays.length - 1, Math.round(domDivDaySelector.scrollTop / domDivDay.offsetHeight)))];
                if (div) {
                    selectDay(div);
                }
            }, 300);
        });
    }

    // Часы
    {
        var divHourSelector = cxCreateDiv({parent: divListItem});
        divHourSelector.addClass("timeSelector");
        divHourSelector.css("width", "25%");


        var divHourOverlay = cxCreateDiv({parent: divHourSelector});
        divHourOverlay.addClass("timeOverlay");


        var divHourWheel = cxCreateDiv({parent: divHourSelector});
        divHourWheel.addClass("timeWheel");
        divHourWheel.css("text-align", "center");

        var divHours = new Array();

        var divHourStub = cxCreateDiv({parent: divHourWheel});
        divHourStub.html("&nbsp");
        divHourStub.addClass("timeStub");

        for (var i = 0; i < 24; i++) {
            var html = i;
            var descr = i;
            var divHour = cxCreateDiv({parent: divHourWheel});
            divHour.html(html);
            divHour.addClass("timeItem");
            divHour.prop("cxDisplay", descr);
            divHours.push(divHour);
        }

        var divHourStub2 = cxCreateDiv({parent: divHourWheel});
        divHourStub2.html("&nbsp");
        divHourStub2.addClass("timeStub");

        var domDivHour = divHourStub.get(0);
        var domDivHourSelector = divHourSelector.get(0);

        var hourScrolling = false;
        var selectedHour = null;

        function selectHour(hour_) {
            if (hour_ == selectedHour)
                return;

            var duration = 200;
            if (selectedHour == null)
                duration = 0;

            selectedHour = hour_;

            var top = hour_.get(0).offsetTop - divDayWheel.get(0).offsetTop - hour_.get(0).offsetHeight - 1;
            hourScrolling = true;

            hour_.parent().children().removeClass("timeItemSelected");
            hour_.addClass("timeItemSelected");

            divHourSelector.animate({scrollTop: top}, duration, null, function () {
                hourScrolling = false;

                listCustomItem.Hour = hour_.prop("cxDisplay");
                updateCustom();
            });
        }

        var timerHourSelector = null;
        $.each(divHours, function () {
            var div = this;
            addClickHandler(div, {
                onClick: function () {
                    if (timerHourSelector)
                        clearTimeout(timerHourSelector);
                    selectHour(div);
                }, transparent: true
            });
        });

        divHourSelector.scroll(function () {
            if (timerHourSelector)
                clearTimeout(timerHourSelector);
            timerHourSelector = setTimeout(function () {
                var div = divHours[Math.max(0, Math.min(divHours.length - 1, Math.round(domDivHourSelector.scrollTop / domDivHour.offsetHeight)))];
                if (div) {
                    selectHour(div);
                }
            }, 300);
        });
    }

    // Минуты
    {
        var divMinuteSelector = cxCreateDiv({parent: divListItem});
        divMinuteSelector.addClass("timeSelector");
        divMinuteSelector.css("width", "25%");


        var divMinuteOverlay = cxCreateDiv({parent: divMinuteSelector});
        divMinuteOverlay.addClass("timeOverlay");


        var divMinuteWheel = cxCreateDiv({parent: divMinuteSelector});
        divMinuteWheel.addClass("timeWheel");
        divMinuteWheel.css("text-align", "center");

        var divMinutes = new Array();

        var divMinuteStub = cxCreateDiv({parent: divMinuteWheel});
        divMinuteStub.html("&nbsp");
        divMinuteStub.addClass("timeStub");

        for (var i = 0; i < 12; i++) {
            var html = i * 5;
            if (html < 10)
                html = "0" + html;
            var descr = html;
            var divMinute = cxCreateDiv({parent: divMinuteWheel});
            divMinute.html(html);
            divMinute.addClass("timeItem");
            divMinute.prop("cxDisplay", descr);
            divMinutes.push(divMinute);
        }

        var divMinuteStub2 = cxCreateDiv({parent: divMinuteWheel});
        divMinuteStub2.html("&nbsp");
        divMinuteStub2.addClass("timeStub");

        var domDivMinute = divMinuteStub.get(0);
        var domDivMinuteSelector = divMinuteSelector.get(0);

        var MinuteScrolling = false;
        var selectedMinute = null;

        function selectMinute(minute_) {
            if (minute_ == selectedMinute)
                return;

            var duration = 200;
            if (selectedMinute == null)
                duration = 0;

            selectedMinute = minute_;

            var top = minute_.get(0).offsetTop - divDayWheel.get(0).offsetTop - minute_.get(0).offsetHeight - 1;
            MinuteScrolling = true;

            minute_.parent().children().removeClass("timeItemSelected");
            minute_.addClass("timeItemSelected");

            divMinuteSelector.animate({scrollTop: top}, duration, null, function () {
                MinuteScrolling = false;

                listCustomItem.Minute = minute_.prop("cxDisplay");
                updateCustom();
            });
        }

        var timerMinuteSelector = null;
        $.each(divMinutes, function () {
            var div = this;
            addClickHandler(div, {
                onClick: function () {
                    if (timerMinuteSelector)
                        clearTimeout(timerMinuteSelector);
                    selectMinute(div);
                }, transparent: true
            });
        });

        divMinuteSelector.scroll(function () {
            if (timerMinuteSelector)
                clearTimeout(timerMinuteSelector);
            timerMinuteSelector = setTimeout(function () {
                var div = divMinutes[Math.max(0, Math.min(divMinutes.length - 1, Math.round(domDivMinuteSelector.scrollTop / domDivMinute.offsetHeight)))];
                if (div) {
                    selectMinute(div);
                }
            }, 300);
        });
    }

    function calculateHeight() {
        var h = (domDivDay.offsetHeight + domDivListItem.offsetHeight - domDivListItem.clientHeight) * 3 + "px";

        divDaySelector.css("height", h);
        divDayOverlay.css("height", h);
        divDayOverlay.css("width", domDivDay.clientWidth + "px");

        divHourSelector.css("height", h);
        divHourOverlay.css("height", h);
        divHourOverlay.css("width", domDivHour.clientWidth + "px");

        divMinuteSelector.css("height", h);
        divMinuteOverlay.css("height", h);
        divMinuteOverlay.css("width", domDivMinute.clientWidth + "px");
    }

    var divContinue = cxCreateDiv({parent: divListBody, className: "listItem listItemPadding"});
    divContinue.css("border", "none");
    divContinue.css("text-align", "center")
    var btApply = createCaptionButton(divContinue, {
        className: "selectorButtonBig",
        size: "1.5em",
        onClick: function () {
            if (customSelectedItem) {
                params_.onContinue(customSelectedItem);
            }
        }
    });
    cxCreateImg({parent: btApply, src: getLocalURLPrefix() + "Images/Applications/apply.png"});
    cxCreateSpan({parent: btApply, html: ControlsConsts.applyCaption});

    /*
     var divContinue = cxCreateDiv({ className: "listItem", parent: divListBody });
     var btContinue = createButton(divContinue, {
     caption: "Применить",
     onClick: function () {
     if (customSelectedItem)
     params_.onContinue(customSelectedItem);
     }
     });
     btContinue.addClass("greenButton");
     btContinue.css("margin", "0");
     btContinue.css("width", "100%");
     divContinue.css("text-align", "center");
     divContinue.css("border", "none");
     */

    uiShow(divWrapper, null, function () {
        calculateHeight();

        var found = false;
        $.each(divDays, function () {
            if (found)
                return;
            if (listCustomItem.Date == this.prop("cxDisplay")) {
                found = true;
                selectDay(this);
            }
        });

        if (!found)
            selectDay(divDayToday);

        selectHour(divHours[listCustomItem.Hour]);
        selectMinute(divMinutes[Math.round(listCustomItem.Minute / 5)]);

        updateCustom();
    });
}

function InitialMap(myMap_, mapType_, mapOptions_, ready_) {

    switch (mapType_) {
        case "Yandex":
            InitialYandexMap(myMap_, mapOptions_, ready_);
            break;
        case "YandexNarod":
            mapOptions_.type = 'yandex#publicMap';
            InitialYandexMap(myMap_, mapOptions_, ready_);
            break;
        case "Google":
            InitialGoogleMap(myMap_, mapOptions_, ready_);
            break;
        case "OSM":
        case "DoubleGIS":
        default:
            console.log("InitialMap() : Map type " + mapType + " not supported");
            InitialYandexMap(myMap_, mapOptions_, ready_);
            mapType = "Yandex";
            break;
    }
}

function InitialYandexMap(myMap_, options_, callback_) {
    ymaps.ready(function init() {
        myMap_ = new ymaps.Map(options_.idMap, {
            center: options_.mapStartCenter,
            type: options_.type,
            zoom: options_.zoom ? options_.zoom : 18,
            controls: []
        });
        if (callback_) {
            callback_(myMap_);
        }
    });
}

function InitialGoogleMap(myMap_, options_, callback_) {
    myMap_ = new google.maps.Map(document.getElementById(options_.idMap), {
        zoom: options_.zoom ? options_.zoom : 18,
        center: convertPoint("Google", options_.mapStartCenter),
        disableDefaultUI: true
    });
    if (callback_) {
        callback_(myMap_);
    }
}


function translatParsedMessage(message) {
    var currentLanguage = localStorage.getItem('currentLanguage') || 'ru';
    console.debug("translatParsedMessage message =" + message + ";currentLanguage =" + currentLanguage);

    console.debug("translatParsedMessage arguments =" + JSON.stringify(arguments));


    if (!currentLanguage) {
        currentLanguage = 'ru';
    }

    if (online_messages[currentLanguage][message]) {
        return online_messages[currentLanguage][message];
    }
    else {
        if (message != undefined && message) {

            for (var p = 0; p < patterns.length; p++) {
                var pattern = patterns[p];
                var trnaslation = pattern.process(message);
                if (trnaslation != false) {
                    return trnaslation;
                }
            }
        }
        console.log("|" + message + "| Not Found");
        return message;
    }
}
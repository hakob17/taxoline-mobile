﻿var ControlsConsts_en = {
    entranceWithColonCaption: "Entrance: ",
    entranceWithCommaAndColonCaption: ", Entrance: ",
    serviceUnavailableMessage: "Service temporarily unavailable",
    cancelCapt: "Cancel",
    yesCaption: "Yes",
    noCaption: "No",
    applyCaption: "Apply",
    applyCaption: "Apply",
    continueCaption: "Continue",
    commentCaption: "Comment ",
    sendCaption: "Send",
    fromCaption: "From",
    futherCaption: "Request",
    searchAddress: "Address search",
    sayDriver: "Tell the driver",
    notesCapt: "Notes",
    cityCaption: "City",
    streetCaption: "Street",
    houseCaption: "Home",
    entranceCaption: "Entrance",
    addressInfo: "How to drive",
    houseCapt: "home",
    notInBase: " (not in database)",
    selectAddressToMessage: "Please enter address From",
    getTaxiCaption: "Request",
    getTaxiCorporationCaptionStart: "Order by",
    getTaxiCorporationCaptionEnd: "",
    toCaption: "To",
    confirmCaption: "Confirmation",
    clearAddressConfirmation: "Clear address ",
    notNecessaryCaption: "Not necessarily",
    deleteAddressConfirmation: "Delete address ",
    extraCaption: "Advanced",
    wishesPaymentPhoneNotesCaption: "Wishes, payment method, phone, notes",
    nowCapt: "Now",
    inTenMinutes: "10 minutes",
    inTwentyMinutes: "20 minutes",
    inThirtyMinutes: "30 minutes",
    todayCaption: "Today",
    tomorrowCaption: "Tomorrow",
    dateCaption: "Date",
    hoursCaption: "Hours",
    minutesCaption: "Minutes",
    businessTrip: "BUSINESS TRIP",
    cash: "Cash",
    paymentMethodCaption: "PAYMENT METHOD",

    //Новые
    searchCaption: "Search",
    nearCaption: "Near",
    historyCaption: "History",
    myCaption: "My",
    mapCaption: "Map",
    dontKnowCaption: "Don't know",
	errMessage: "Error"
};
var ControlsConsts_am = {
    entranceWithColonCaption: "Մուտք: ",
    entranceWithCommaAndColonCaption: ", Մուտք: ",
    serviceUnavailableMessage: "Ծառայությունը ժամանակավորապես հասանելի չէ",
    cancelCapt: "Չեղարկում",
    yesCaption: "Այո",
    noCaption: "Ոչ",
    applyCaption: "Կիրառել",
    continueCaption: "Շարունակել",
    commentCaption: "Նշում ",
    sendCaption: "Ուղարկել",
    fromCaption: "Որտեղից",
    futherCaption: "Շարունակել",
    searchAddress: "Հասցեի փնտրում",
    sayDriver: "Կասեմ վարորդին",
    notesCapt: "Նշում",
    cityCaption: "Քաղաք",
    streetCaption: "Փողոց",
    houseCaption: "Տուն",
    entranceCaption: "Մուտք",
    addressInfo: "Ինչպես մոտենալ",
    houseCapt: "Տուն(շինություն)",
    notInBase: " (բազայում չկա)",
    selectAddressToMessage: "Խնդրում ենք նշել հասցեն",
    getTaxiCaption: "Պատվիրել",
    getTaxiCorporationCaptionStart: "Պատվիրել",
    getTaxiCorporationCaptionEnd: "-ի Հաշվին",
    toCaption: "Ուր",
    confirmCaption: "Հաստատում",
    clearAddressConfirmation: "Մաքրել հասցեն ",
    notNecessaryCaption: "Պարտադիր չէ",
    deleteAddressConfirmation: "Հեռացենլ հասցեն",
    extraCaption: "Լրացուցիչ",
    wishesPaymentPhoneNotesCaption: "Ցանկություններ, Վճարման տարբերակներ, կապի հեռախոսահամար, նշումներ",
    nowCapt: "ՀԻմա",
    inTenMinutes: " 10 րոպեից",
    inTwentyMinutes: " 20 րոպեից",
    inThirtyMinutes: " 30 րոպեից",
    todayCaption: "Այսօր",
    tomorrowCaption: "Վաղը",
    dateCaption: "Ամսաթիվ",
    hoursCaption: "Ժամեր",
    minutesCaption: "րոպեներ",
    businessTrip: "ԸՆԿԵՐՈՒԹՅԱՆ ՀԱՇՎԻՆ",
    cash: "ԿԱՆԽԻԿ",
    paymentMethodCaption: "վճարման կարգ",

    //Новые
    searchCaption: "Փնտրել",
    nearCaption: "Մոտակա",
    historyCaption: "Պատմություն",
    myCaption: "Իմ",
    mapCaption: "Քարտեզ",
    dontKnowCaption: "Չգիտեմ",
	errMessage: "Սխալ"
};
var ControlsConsts_ru = {
    entranceWithColonCaption: "Подъезд: ",
    entranceWithCommaAndColonCaption: ", Подъезд: ",
    serviceUnavailableMessage: "Сервис временно недоступен",
    cancelCapt: "Отмена",
    yesCaption: "Да",
    noCaption: "Нет",
    applyCaption: "Применить",
    continueCaption: "Продолжить",
    commentCaption: "Комментарий ",
    sendCaption: "Отправить",
    fromCaption: "Откуда",
    futherCaption: "Далее",
    searchAddress: "Поиск адреса",
    sayDriver: "Скажу водителю",
    notesCapt: "Примечания",
    cityCaption: "Город",
    streetCaption: "Улица",
    houseCaption: "Дом",
    entranceCaption: "Подъезд",
    addressInfo: "Как лучше подъехать",
    houseCapt: "дом",
    notInBase: " (нет в базе)",
    selectAddressToMessage: "Пожалуйста, укажите адрес Откуда",
    getTaxiCaption: "Заказать",
    getTaxiCorporationCaptionStart: "Заказать такси за счет",
    getTaxiCorporationCaptionEnd: "",
    toCaption: "Куда",
    confirmCaption: "Подтверждение",
    clearAddressConfirmation: "Очистить адрес ",
    notNecessaryCaption: "Не обязательно",
    deleteAddressConfirmation: "Удалить адрес ",
    extraCaption: "Дополнительно",
    wishesPaymentPhoneNotesCaption: "Пожелания, способ оплаты, телефон для связи, примечания",
    nowCapt: "Сейчас",
    inTenMinutes: "Через 10 минут",
    inTwentyMinutes: "Через 20 минут",
    inThirtyMinutes: "Через 30 минут",
    todayCaption: "Сегодня",
    tomorrowCaption: "Завтра",
    dateCaption: "Дата",
    hoursCaption: "Часы",
    minutesCaption: "Минуты",
    businessTrip: "ЗА СЧЕТ КОМПАНИИ",
    cash: "НАЛИЧНЫЕ",
    paymentMethodCaption: "метод оплаты",

    //Новые
    searchCaption: "Поиск",
    nearCaption: "Рядом",
    historyCaption: "История",
    myCaption: "Мои",
    mapCaption: "Карта",
    dontKnowCaption: "Не знаю",
    errMessage: "Произошла ошибка"
	
};

 

var currentLanguage = localStorage.getItem('currentLanguage');

var ControlsConsts = ControlsConsts_ru;


switch(currentLanguage)
{
	case 'en':
		ControlsConsts = ControlsConsts_en;
		 
	break;
	case 'ru':
		ControlsConsts = ControlsConsts_ru;
	break;
	case 'am':
		ControlsConsts = ControlsConsts_am;
	break;
}


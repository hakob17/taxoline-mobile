﻿; function MxPlacemark(mapType_, geometry_, options_) {

    var placemark = null;

    if (mapType_ && geometry_) {
        switch (mapType_) {
            case "Yandex": 
            case  "YandexNarod":
                placemark = new ymaps.Placemark(geometry_,null, options_);
            break;
            case "Google": 
                placemark = new google.maps.Marker({
                    position: convertPoint(mapType_, geometry_),
                    size: new google.maps.Size(options_.iconImageSize[0], options_.iconImageSize[1]), // TODO не срабатывает ?
                    icon: options_.iconImageHref
                });
                break;
            case "OSM":
                break;
            default:
                break;
        }
    }
    return placemark;

}

function MxGetZoom(myMap_, mapType_) {

    var zoom = null;

    if (myMap_ && mapType_) {

        switch (mapType_) {
            case "Yandex": 
            case "YandexNarod": 
                zoom = myMap_.getZoom();
            case "Google": 
                zoom = myMap_.getZoom();
                break;
            case "OSM":
                break;
            default:
                break;
        }

    }
    return zoom;
}

function MxSetZoom(myMap_, mapType_, zoom_, options_) {
    
    if (myMap_ && mapType_) {

        switch (mapType_) {
            case "Yandex": 
            case "YandexNarod": 
                myMap_.setZoom(zoom_, options_);
                break;
            case "Google": 
                myMap_.setZoom(zoom_);
                break;
            case "OSM":
                break;
            default:
                break;
        }

    }

}

function MxSetBounds(myMap_, mapType_, points_, options_) {
    
    if (myMap_ && mapType_ && points_) {

        switch (mapType_) {
            case "Yandex": 
            case "YandexNarod":
                myMap_.setBounds(points_, options_);
                break;
            case "Google": 
                var markersBounds = new google.maps.LatLngBounds();
                for (var i = 0; i < points_.length; i++) {
                    var markerPosition = new google.maps.LatLng(points_[i][0], points_[i][1]);
                    markersBounds.extend(markerPosition);
                }

                myMap_.setCenter(markersBounds.getCenter(), myMap_.fitBounds(markersBounds));
                break;
            case "OSM":
                break;
            default:
                break;
        }

    }
}

function MxGetCenter(myMap_, mapType_) {
    var center = null;
    if (myMap_ && mapType_) {

        switch (mapType_) {
            case "Yandex": 
            case "YandexNarod":
                center = myMap_.getCenter();
                break;
            case "Google": 
                center = myMap_.getCenter();
                return [center.lat(), center.lng()];
                break;
            case "OSM":
                break;
            default:
                break;
        }

    }
    return center;
}

function MxGeocode(myMap_, mapType_, request_, options_, callbackSuccess_, callbackError_) {
    log(arguments);
    if (mapType_ && request_) {

        switch (mapType_) {
            case "Yandex": 
            case "YandexNarod":
                ymaps.geocode(request_, options_).then(callbackSuccess_, callbackError_);
                break;
            case "Google": 
                var geocoder = new google.maps.Geocoder();
				
				console.log("google GEOCODER");
				console.log(request_);
				console.log(request_[0]);
				console.log(request_[1]);
				
				var latlng = {lat: request_[0], lng: request_[1]};
				geocoder.geocode({ 'location': latlng }, function (results, status) {
				
				console.log("geocoder results");					
				console.log(results);
				console.log(status);

				
                    if (status == google.maps.GeocoderStatus.OK) {
                        callbackSuccess_(results);

                    } else {

                        callbackError_(results);
                    }
                });
				/*
                var address = request_.toString();
                geocoder.geocode({ 'address': address }, function (results, status) {

                    if (status == google.maps.GeocoderStatus.OK) {

                        callbackSuccess_(results);

                    } else {

                        callbackError_(results);
                    }
                });
				*/
                break;
            case "OSM":
                break;
            default:
                break;
        }
    }

}

function MxRoute(mapType_, points_, callbackSuccess_, callbackError_) {
    if (mapType_ && points_) {
        switch (mapType_) {
            case "Yandex": 
            case "YandexNarod":
                ymaps.route(points_).then(callbackSuccess_, callbackError_);
                break;
            case "Google": 
                var directionsService = new google.maps.DirectionsService();

                var start = convertPoint(mapType_, points_[0]);
                var end = convertPoint(mapType_, points_[points_.length - 1]);
                var waypoints = [];
                     
                for (var i = 0; i < points_.length; i++) {
                    waypoints.push({
                        location: convertPoint("Google",points_[i]),
                        stopover: true
                    });
                }

                var request = {
                    origin: start,
                    destination: end,
                    waypoints: waypoints, 
                    optimizeWaypoints: true,
                    travelMode: google.maps.TravelMode.DRIVING
                };

                directionsService.route(request, function (response, status) {
                    if (status == google.maps.DirectionsStatus.OK) {
                        var route = response.routes[0];
                        // там нужен весь response
                        callbackSuccess_(response);
                    }
                    else
                        callbackError_();
                });

                break;
            case "OSM":
                break;
            default:
                break;
        }
    }
}

function MxGetDistance(mapType_) {
    
    switch (mapType_) {
        case "Yandex": 
        case "YandexNarod": 
            break;
        case "Google": 
            break;
        case "OSM":
            break;
        default:
            break;
    }
}

function convertPoint(mapType_, point_) {
    
    if (mapType_ && point_ && point_.length == 2) {

        var lat = point_[0];
        var lon = point_[1];

        switch (mapType_) {
            case "Yandex": 
            case "YandexNarod":
                return point_;
                break;
            case "Google":
                return new google.maps.LatLng(parseFloat(lat).toFixed(5), parseFloat(lon).toFixed(5))//{ lat: lat, lng: lon }
                break;
            case "OSM":
                break;
            default:
                break;
        }
    }
}

function MxPanTo(myMap_, mapType_, center_, options_, callbackSuccess_, callbackError_ ) {
    if (myMap_ && mapType_) {

        switch (mapType_) {
            case "Yandex": 
            case "YandexNarod":
                myMap_.panTo(center_, options_).then(callbackSuccess_, callbackError_);
                break;
            case "Google": 
                if (center_) {
                    myMap_.panTo(center_);
                    callbackSuccess_();
                }
                else
                    callbackError_();
                break;
            case "OSM":
                break;
            default:
                break;
        }

    }
}


function AddressParser(mapType_, addressComponents_) {
    var address;
    var place = null;
    var locality = null;
    var area = null;
    var street = null;
    var building = null;

    if (mapType_ && addressComponents_) {

        switch (mapType_) {
            case "Yandex": 
            case "YandexNarod": 
                break;
            case "Google": 

                // TODO возможно другие типы

                var placeArr = ['establishment', 'natural_feature'];

                var localityArr = ['locality'];

                var areaArr = ['administrative_area_level_2', 'administrative_area_level_1'];

                var streetArr = ['route'];

                var buildingArr = ['street_number'];

                place = getComponentAddressByType(addressComponents_, placeArr);
                locality = getComponentAddressByType(addressComponents_, localityArr);
                area = getComponentAddressByType(addressComponents_, areaArr);
                street = getComponentAddressByType(addressComponents_, streetArr);
                building = getComponentAddressByType(addressComponents_, buildingArr);

                if (locality && area && area.indexOf(locality) != -1) { // чтоб Москву два раза не показывал
                    locality = null;
                }
                /*
                if (!building) {  // если не определил здание, не дать возможность перейти Далее
                    return null;
                }*/
                if (area && street && building) {
                    area = null;                    // убрать населенный пункт
                }

                address = getAddress(/*place,*/ locality, area, street, building);

                break;
            case "OSM":
                break;
            default:
                break;
        }

    }

    return address;
}

function getAddress() {
    var address;
    if (arguments.length > 0) {

        for (var i = 0; i < arguments.length; i++) {
            if (arguments[i] && arguments[i].indexOf('Unnamed') == -1) {

                if (address) {
                    address += ', ' + arguments[i];
                }
                else {
                    address = arguments[i];
                }
            }
        }
    }

    return address;
}

function getComponentAddressByType(components_, types_) {
    var component = null;
    $.each(components_, function () {
        if (components_.length > 0) {
            var inters = Intersection(this.types, types_);
            if (inters.length > 0) {
                component = this.short_name;
            }
        }
    });
    return component;
}

function MxSetCoordinates(placemark_, mapType_, coordinate_) {
    if (placemark_ && mapType_ && coordinate_) {

        switch (mapType_) {
            case "Yandex": 
            case "YandexNarod":
                placemark_.geometry.setCoordinates(coordinate_);
                break;
            case "Google": 
                placemark_.setPosition(convertPoint( mapType_, coordinate_));
                break;
            case "OSM":
                break;
            default:
                break;
        }

    }
}

function MxAddPlacemarkOnMap(myMap_, mapType_, placemark_) {
    if (myMap_ && placemark_ && mapType_) {

        switch (mapType_) {
            case "Yandex": 
            case "YandexNarod":
                myMap_.geoObjects.add(placemark_);
                break;
            case "Google": 
                placemark_.setMap(myMap_);
                break;
            case "OSM":
                break;
            default:
                break;
        }
    }
}

function MxRemovePlacemarkFromMap(myMap_, mapType_, placemark_) {
    if (myMap_ && placemark_ && mapType_) {

        switch (mapType_) {
            case "Yandex": 
            case "YandexNarod":
                myMap_.geoObjects.remove(placemark_);
                break;
            case "Google": 
                placemark_.setMap(null);
                break;
            case "OSM":
                break;
            default:
                break;
        }
    }
}

function MxAddRoute(myMap_, mapType_, route_) {
    if (myMap_ && route_ && mapType_) {

        switch (mapType_) {
            case "Yandex": 
            case "YandexNarod":
                myMap_.geoObjects.add(route_);
                route_.getWayPoints().removeAll();
                route_.getPaths().options.set({
                    strokeColor: '0000ffff'
                });
                break;
            case "Google": 
                directionsDisplay = new google.maps.DirectionsRenderer();
                directionsDisplay.setDirections(route_);
                directionsDisplay.setMap(myMap_);
                directionsDisplay.setOptions({ suppressMarkers: true });
                break;
            case "OSM":
                break;
            default:
                break;
        }
    }
}

function MxAddEvent(myMap_, mapType_, event_, callback_) {
    if (myMap_ && mapType_ && event_ && callback_) {

        var myEvent = getEventByType(mapType_, event_);

        switch (mapType_) {
            case "Yandex":
            case "YandexNarod":
                myMap_.events.add(myEvent, callback_);
                break;
            case "Google":
                google.maps.event.addListener(myMap_, myEvent, callback_);
                break;
            case "OSM":
                break;
            default:
                break;
        }
    }
    else {
        log('function MxAddEvent() has 4 parameters: myMap, mapType, event, callback');
    }
}

function getEventByType(mapType_, event_) {

    // имена событий, которые роазличаются в различных типах карт
    //TODO позже придумаю, как по человечески сделать :) 
    
    var myEvent = null;

    switch (mapType_) {
        case "Yandex":
        case "YandexNarod":

            switch (event_) {
                case "actionbegin":
                case "dragstart":
                    myEvent = "actionbegin";
                    break;
                case "actionend":
                case "dragend":
                    myEvent = "actionend";
                    break;
                case "sizechange":
                case "resize":
                    myEvent = "sizechange";
                    break;
                default:
                    myEvent = event_;
                    break;
            }

            break;

        case "Google":

            switch (event_) {
                case "actionbegin":
                case "dragstart":
                    myEvent = "dragstart";
                    break;
                case "actionend":
                case "dragend":
                    myEvent = "dragend";
                    break;
                case "sizechange":
                case "resize":
                    myEvent = "resize";
                    break;
                default:
                    myEvent = event_;
                    break;
            }

            break;
        case "OSM":
            myEvent = event_;
            break;
        default:
            myEvent = event_;
            break;
    }

    return myEvent;
}

function MxFitToViewport(myMap_, mapType_) {

    if (myMap_ && mapType_ ) {

        switch (mapType_) {
            case "Yandex":
            case "YandexNarod":
                myMap_.container.fitToViewport();
                break;
            case "Google":
                google.maps.event.trigger(myMap_, "resize");
                break;
            case "OSM":
                break;
            default:
                break;
        }
    }
    else {
        log('function MxFitToViewport() has 2 parameters: myMap, mapType');
    }
}

function MxGetCoords(obj_, mapType_) {

    if (obj_ && mapType_) {

        switch (mapType_) {
            case "Yandex":
            case "YandexNarod":
                return obj_.get('coords');
                break;
            case "Google":
                return obj_.latLng;
                break;
            case "OSM":
                break;
            default:
                break;
        }
    }
}

function MxGetAddress(mapType_, addressObj_) {

    var address = null;

    if (addressObj_ && mapType_) {

        switch (mapType_) {
            case "Yandex":
            case "YandexNarod":
                $.each(addressObj_.GeoObjectCollection.featureMember, function () {
                    address = this.GeoObject.name;
                });
                break;
            case "Google":
                address = AddressParser(mapType_, addressObj_[0].address_components);
                break;
            case "OSM":
                break;
            default:
                break;
        }
    }

    return address;
}

function MxGetAddressDescription(mapType_, addressObj_, addr_) {

    var addressDescr = null;

    if (addressObj_ && mapType_) {

        switch (mapType_) {
            case "Yandex":
            case "YandexNarod":
                $.each(addressObj_.GeoObjectCollection.featureMember, function () {
                    addressDescr = this.GeoObject.description + ", " + addr_;
                });
                break;
            case "Google":
                addressDescr = addressObj_[0].formatted_address;
                break;
            case "OSM":
                break;
            default:
                break;
        }
    }

    return addressDescr;
}
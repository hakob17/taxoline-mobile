﻿var UtilsConsts_en = {
    januaryShort: "Jan",
    februaryShort: "Feb",
    marchShort: "Mar",
    aprilShort: "Apr",
    mayShort: "May",
    juneShort: "Jun",
    julyShort: "Jul",
    augustShort: "Aug",
    septemberShort: "Sep",
    octoberShort: "Oct",
    novemberShort: "Nov",
    decemberShort: "Dec",
    januaryLong: "January",
    februaryLong: "February",
    marchLong: "March",
    aprilLong: "April",
    mayLong: "May",
    juneLong: "June",
    julyLong: "July",
    augustLong: "August",
    septemberLong: "September",
    octoberLong: "October",
    novemberLong: "November",
    decemberLong: "December"
};
var UtilsConsts_ru = {
    januaryShort: "янв",
    februaryShort: "фев",
    marchShort: "мар",
    aprilShort: "апр",
    mayShort: "май",
    juneShort: "июн",
    julyShort: "июл",
    augustShort: "авг",
    septemberShort: "сен",
    octoberShort: "окт",
    novemberShort: "ноя",
    decemberShort: "дек",
    januaryLong: "января",
    februaryLong: "февраля",
    marchLong: "марта",
    aprilLong: "апреля",
    mayLong: "мая",
    juneLong: "июня",
    julyLong: "июля",
    augustLong: "августа",
    septemberLong: "сентября",
    octoberLong: "октября",
    novemberLong: "ноября",
    decemberLong: "декабря"
};
var UtilsConsts_am = {
    januaryShort: "հնվ",
    februaryShort: "փտր",
    marchShort: "մրտ",
    aprilShort: "ապր",
    mayShort: "մյս",
    juneShort: "հնս",
    julyShort: "հլս",
    augustShort: "օգս",
    septemberShort: "սպտ",
    octoberShort: "հկտ",
    novemberShort: "նմբ",
    decemberShort: "դկտ",
    januaryLong: "հունվարի",
    februaryLong: "փետրվարի",
    marchLong: "մարտի",
    aprilLong: "ապրիլի",
    mayLong: "մայիսի",
    juneLong: "հունիսի",
    julyLong: "հուլիսի",
    augustLong: "օգոստոսի",
    septemberLong: "սեպտեմբերի",
    octoberLong: "հոկտեմբերի",
    novemberLong: "նոյեմբերի",
    decemberLong: "դեկտեմբերի"
};

var currentLanguage = localStorage.getItem('currentLanguage');

var UtilsConsts = UtilsConsts_ru;


switch (currentLanguage) {
    case 'en':
        UtilsConsts = UtilsConsts_en;
        break;
    case 'ru':
        UtilsConsts = UtilsConsts_ru;
        break;
    case 'am':
        UtilsConsts = UtilsConsts_am;
        break;
}



var online_messages = new Array();
online_messages['am'] = new Array();
online_messages['en'] = new Array();
online_messages['ru'] = new Array();

online_messages['am']['Ереван'] = "Երևան";
online_messages['am']['Наличные'] = "Կանխիկ";
online_messages['am']['Оплата водителю наличными'] = "Կանխիկ վարորդին";

online_messages['en']['Ереван'] = "Yerevan";
online_messages['en']['Наличные'] = "Cash";
online_messages['en']['Оплата водителю наличными'] = "Pay the driver in cash";

online_messages['en']["Заказа принят"] = "Order accepted";
online_messages['en']["Машина назначена"] = "The car assigned";
online_messages['en']["Машина выехала"] = "The car left";
online_messages['en']["Машина приехала — пожалуйста, выходите"] = "The car arrived - please come out";
online_messages['en']["Машина ожидает вас. Пожалуйста, выходите"] = "The car is waiting for you. Please come out";
online_messages['en']["Заказ выполняется"] = "The order is carried";
online_messages['en']["Заказ завершен"] = "Order completed";
online_messages['en']["Заказ отменен"] = "Order cancelled";
online_messages['en']["Откуда"] = "From";
online_messages['en']["Ошибка"] = "Error";


online_messages['am']["Заказ принят"] = "Պատվերը ընդունված է";
online_messages['am']["Машина назначена"] = "Մեքենան նշանակված է";
online_messages['am']["Машина выехала"] = "Մեքենան ճանապարհին է";
online_messages['am']["Машина приехала — пожалуйста, выходите"] = "Մեքենան տեղում է - խնդրում ենք դուրս գալ";
online_messages['am']["Машина ожидает вас. Пожалуйста, выходите"] = "Մեքենան տեղում է - խնդրում ենք դուրս գալ";
online_messages['am']["Заказ выполняется"] = "Պատվերը կատարվում է";
online_messages['am']["Заказ завершен"] = "Պատվերը ավարտված է";
online_messages['am']["Заказ отменен"] = "Պատվերը մերժված է";
online_messages['am']["Откуда"] = "Որտեղից";
online_messages['am']["Ошибка"] = "Սխալ";
online_messages['am']["Дополнительное описание машины"] = "Մեքնայի լրացուցիչ նկարագրություն";
online_messages['en']["Дополнительное описание машины"] = "Additional description of the car";


online_messages['am']["6-местный/VIP-Аэропорт-За город"] = "6-տեղ/VIP-Օդանավակայան-Քաղաքից դուրս";
online_messages['am']["Мин. 5000др/15км, туда 1км/150др, обратно 1км/75др, ожидание 5мин/150др"] = "Մին. 5000դր/15կմ, միակողմանի 1կմ/150դր, հետադարձ 1կմ/75դր, սպասում 5րոպ/150դր";
online_messages['am']["6 местный-Город"] = "6 տեղ-Քաղաք";
online_messages['am']["Мин. 1000др/4км, 150др/км, ожидание 5мин/100др"] = "Մին. 1000դր/4կմ, 150դր/կմ, սպասում 5րոպ/100դր";
online_messages['am']["6 местный-За город"] = "6 տեղ-Քաղաքից դուրս";
online_messages['am']["Мин. 1000др/4км, туда 150др/км, обратно 75др/км, ожидание 5 мин/100др"] = "Մին. 1000դր/4կմ, միակողմանի 150դր/կմ, հետադարձ 75դր/կմ, սպասում 5րոպ/100դր";
online_messages['am']["VIP Город"] = "VIP Քաղաք";
online_messages['am']["Мин. 800др/4км, 150др/км, ожидание 5мин/150др"] = "Մին. 800դր/4կմ, 150դր/կմ, սպասում 5րոպ/150դր";
online_messages['am']["VIP За город"] = "VIP Քաղաքից դուրս";
online_messages['am']["Мин. 800др/4км, туда 150др/км, обратно 75др/км, ожидание 5мин/150др"] = "Մին. 800դր/4կմ, միակողմանի 150դր/կմ, հետադարձ 75դր/կմ, սպասում 5րոպ/150դր";
online_messages['am']["Аэропорт"] = "Օդանավակայան";
online_messages['am']["Мин. 2000др/15км, 100др/км, ожидание 5мин/100др"] = "Մին. 2000դր/15կմ, 100դր/կմ, սպասում 5րոպ/100դր";
online_messages['am']["Аэропорт-6 местный"] = "Օդանավակայան-6 տեղ";
online_messages['am']["Мин. 5000др/15км, за 150др/км, ожидание 5мин/150др"] = "Մին. 5000դր/15կմ,  150դր/կմ, սպասում 5րոպ/150դր";
online_messages['am']["Аэропорт-VIP"] = "Օդանավակայան-VIP";
online_messages['am']["Мин. 3000др/15км, 150др/км, ожидание 5мин/150др"] = "Մին. 3000դր/15կմ, 150դր/կմ, սպասում 5րոպ/150դր";
online_messages['am']["Аэропорт-За город"] = "Օդանավակայան-Քաղաքից դուրս";
online_messages['am']["Мин. 2000др/15км, туда 1км/120др, обратно 1км/60др, ожидание 5мин/100др"] = "Մին. 2000դր/15կմ, միակողմանի 1կմ/120դր, հետադարձ 1կմ/60դր, սպասում 5րոպ/100դր";
online_messages['am']["Город"] = "Քաղաք";
online_messages['am']["Мин. 600др/4км, 100др/км, ожидание 5мин/100др"] = "Մին. 600դր/4կմ, 100դր/կմ, սպասում 5րոպ/100դր";
online_messages['am']["Грузовые больше 3т"] = "Բեռնատար Մեծ 3տ";
online_messages['am']["Мин 5000др./6км, 1км/500др."] = "Մին 5000դր./6կմ, 1կմ/500դր․";
online_messages['am']["Грузовые до 3т"] = "Բեռնատար մինչև 3տ";
online_messages['am']["Мин 2400др./6км, 1км/300др."] = "ՄԻն 2400դր./6կմ, 1կմ/300դր․";
online_messages['am']["За Город"] = "Քաղաքից դուրս";
online_messages['am']["Мин. 600др/4км, 100др/км, ожидание 5мин/100др"] = "Մին. 600դր/4կմ, 100դր/կմ, սպասում 5րոպ/100դր";
online_messages['am']["Трансфер из аэропорта"] = "Տրանսֆեր օդանավակայանից";
online_messages['am']["Мин. 5000др/15км, 100др/км, ожидание 5мин/100др"] = "Մին. 5000դր/15կմ, 100դր/կմ, սպասում 5րոպ/100դր";
online_messages['am']["Эвакуатор Джип"] = "Էվակուատոր Ջիպ";
online_messages['am']["А/м Джип 8000др."] = "Ա/մ Ջիպ 8000դր․";
online_messages['am']["Эвакуатор За город"] = "Էվակուատոր Քաղաքից դուրս";
online_messages['am']["А/м Седан и Джип, Мин. 7000др./6км, 1км/350др."] = "Ա/մ  Սեդան և Ջիպ, Մին. 7000դր./6կմ, 1կմ/350դր․";
online_messages['am']["Эвакуатор Седан"] = "Էվակուատոր Սեդան";
online_messages['am']["А/м Седан, днем 7000др., ночью 8000др."] = "Ա/մ Սեդան, ցերեկ 7000դր., գիշեր 8000դր․";

online_messages['am']["Лимузин"] = "Լիմուզին";
online_messages['am']["Мин. 10000др/8км, туда 1000др/км, обратно 500др/км, ожид 5мин/450др"] = "Մին. 10000դր/8կմ,  1000դր/8կմ, հետադարձ 500դր/8կմ, սպասում 5րոպ/450դր";

//Messages 
online_messages['am']["Не удалось заказать звонок"] = "Զանգի պատվերը չստացվեց";
online_messages['am']["Спасибо! Водитель свяжется с вами в ближайшее время"] = "Շնորհակալություն! վարորդը կկապնվի Ձեր հետ";
online_messages['am']["Стоимость рассчитана таксометром"] = "Արժեքը հաշվարկված է տաքսաչափով";
online_messages['am']["Не обязательно"] = "Պարտադիր չէ";
online_messages['am']["Ваш заказ принят. Спасибо!"] = "Ձեր պատվերը ընդունված է: Շնորհակալություն";
online_messages['am']["Заказ принят"] = "Ձեր պատվերը ընդունված է";
online_messages['am']["Машина выехала к вам"] = "Մեքենան դուրս է եկել";
online_messages['en']["Ваш заказ принят. Спасибо!"] = "Your order is accepted. Thank you!";
online_messages['en']["Заказ принят"] = "Your order is accepted.";
online_messages['en']["Машина выехала к вам"] = "The car drove to you";

// JavaScript Document
online_messages['en']["6-местный/VIP-Аэропорт-За город"] = "6-seater/VIP-airport-Out of City";
online_messages['en']["Мин. 5000др/15км, туда 1км/150др, обратно 1км/75др, ожидание 5мин/150др"] = "Min. 5000AMD/15km, there 1km/150AMD, back 1km/75AMD, waiting 5m/150AMD";
online_messages['en']["6 местный-Город"] = "6-seater-City";
online_messages['en']["Мин. 1000др/4км, 150др/км, ожидание 5мин/100др"] = "Min. 1000AMD/4km, 150AMD/km, waiting 5m/100AMD";
online_messages['en']["6 местный-За город"] = "6-seater-Out of City";
online_messages['en']["Мин. 1000др/4км, туда 150др/км, обратно 75др/км, ожидание 5 мин/100др"] = "Min. 1000AMD/4km, there 150AMD/km, back 75AMD/km, waiting 5m/100AMD";
online_messages['en']["VIP Город"] = "VIP City";
online_messages['en']["Мин. 800др/4км, 150др/км, ожидание 5мин/150др"] = "Min. 800AMD/4km, 150AMD/km, waiting 5m/150AMD";
online_messages['en']["VIP За город"] = "VIP Out of City";
online_messages['en']["Мин. 800др/4км, туда 150др/км, обратно 75др/км, ожидание 5мин/150др"] = "Min. 800AMD/4km, there 150AMD/km, back 75AMD/km, waiting 5m/150AMD";
online_messages['en']["Аэропорт"] = "Airoport";
online_messages['en']["Мин. 2000др/15км, 100др/км, ожидание 5мин/100др"] = "Min. 2000AMD/15km, 100AMD/km, waiting 5m/100AMD";
online_messages['en']["Аэропорт-6 местный"] = "Airoport-6 seater";
online_messages['en']["Мин. 5000др/15км, за 150др/км, ожидание 5мин/150др"] = "Min. 5000AMD/15km,  150AMD/km, waiting 5m/150AMD";
online_messages['en']["Аэропорт-VIP"] = "Airoport-VIP";
online_messages['en']["Мин. 3000др/15км, 150др/км, ожидание 5мин/150др"] = "Min. 3000AMD/15km, 150AMD/km, waiting 5m/150AMD";
online_messages['en']["Аэропорт-За город"] = "Airoport-Out of City";
online_messages['en']["Мин. 2000др/15км, туда 1км/120др, обратно 1км/60др, ожидание 5мин/100др"] = "Min. 2000AMD/15km, there 1km/120AMD, back 1km/60AMD, waiting 5m/100AMD";
online_messages['en']["Город"] = "City";
online_messages['en']["Мин. 600др/4км, 100др/км, ожидание 5мин/100др"] = "Min. 600AMD/4km, 100AMD/km, waiting 5m/100AMD";
online_messages['en']["Грузовые больше 3т"] = "Сargo transportation more 3T";
online_messages['en']["Мин 5000др./6км, 1км/500др."] = "Min 5000AMD./6km, 1km/500AMD․";
online_messages['en']["Грузовые до 3т"] = "Сargo transportation up to 3T";
online_messages['en']["Мин 2400др./6км, 1км/300др."] = "Min 2400AMD./6km, 1km/300AMD․";
online_messages['en']["За Город"] = "Out of City";
online_messages['en']["Мин. 600др/4км, 100др/км, ожидание 5мин/100др"] = "Min. 600AMD/4km, 100AMD/km, waiting 5m/100AMD";
online_messages['en']["Трансфер из аэропорта"] = "Airport transfer";
online_messages['en']["Мин. 5000др/15км, 100др/км, ожидание 5мин/100др"] = "Min. 5000AMD/15km, 100AMD/km, waiting 5m/100AMD";
online_messages['en']["Эвакуатор Джип"] = "Tow truck jeep";
online_messages['en']["А/м Джип 8000др."] = "jeep 8000AMD․";
online_messages['en']["Эвакуатор За город"] = "Tow truck out of City";
online_messages['en']["А/м Седан и Джип, Мин. 7000др./6км, 1км/350др."] = "Sedan and Jeep, Min. 7000AMD./6km, 1km/350AMD․";
online_messages['en']["Эвакуатор Седан"] = "Tow truck Sedan";
online_messages['en']["А/м Седан, днем 7000др., ночью 8000др."] = "Sedan, day 7000AMD., night 8000AMD";
online_messages['en']["Лимузин"] = "Limousine";
online_messages['en']["Мин. 10000др/8км, туда 1000др/км, обратно 500др/км, ожид 5мин/450др"] = "Min. 10000amd/8km,  1000amd/km, back 500amd/km, waiting 5m/450amd";
//Messages 

online_messages['en']["Не удалось заказать звонок"] = "Failed to request a call";
online_messages['en']["Спасибо! Водитель свяжется с вами в ближайшее время"] = "Thank you! The driver will contact you shortly";
online_messages['en']["Стоимость рассчитана таксометром"] = "The cost calculated by the taximeter";
online_messages['en']["Не обязательно"] = "Not necessarily";
online_messages['en']["Стоимость рассчитана предварительно. Окончательный расчет по таксометру."] = "The cost is calculated beforehand. The final payment on the taximeter.";
online_messages['am']["Стоимость рассчитана предварительно. Окончательный расчет по таксометру."] = "Արժեքը հաշվարկված է մոտավոր։ Վերջնական վճարումը տաքսաչափով։";

function openUrl(url) {
    window.open(url,'_system');
}
online_messages['ru']["ABOUT"] = "<center>" +
	"<img src='" + getLocalURLPrefix() + "Images/Applications/logo_big.png' style='padding:0.5em' />" +
	"</center>" +
	"<center class='smallBodyText'>TaxoLine — мобильное приложение для заказа такси." +
	" История поездок, избранные адреса, информация о заказах, " +
	"уведомления о прибытии машины и многое другое.<br> Контакты: <br>+374 60 644 644, <br>" +
	"Web:" +
	"<a href='#' onclick='openUrl(\"http://taxoline.am\");'>www.taxoline.am</a>" +
	"<br>" +
	"<a href='#' onclick='openUrl(\"http://Instagram.com/TaxoLine\");'>Instagram.com/TaxoLine/</a><br>" +
	"<a href='#' onclick='openUrl(\"http://facebook.com/taxoline\");'>facebook.com/taxoline </a><br>" +
	"</center><br>";
online_messages['en']["ABOUT"] = "<center>" +
	"<img src='" + getLocalURLPrefix() + "Images/Applications/logo_big.png' style='padding:0.5 em' /></center>" +
	"<center class='smallBodyText'>TaxoLine — mobile application for ordering taxi. " +
	"Travel history, favorites, information about orders, notification of arrival of car and many more.<br> " +
	"Contacts: <br>+374 60 644 644 this <br>" +
	"Web:" +
    "<a href='#' onclick='openUrl(\"http://taxoline.am\");'>www.taxoline.am</a>" +
    "<br>" +
    "<a href='#' onclick='openUrl(\"http://Instagram.com/TaxoLine\");'>Instagram.com/TaxoLine/</a><br>" +
    "<a href='#' onclick='openUrl(\"http://facebook.com/taxoline\");'>facebook.com/taxoline </a><br>" +
    "</center><br>";
online_messages['am']["ABOUT"] = "<center>" +
	"<img src='" + getLocalURLPrefix() + "Images/Applications/logo_big.png' style='padding:0.5em' /></center>" +
	"<center class='smallBodyText'>TaxoLine — բջջային հավելված է նախատեսված տաքսի պատվիրելու համար. " +
	"Ուղևորությունների պատմություն, ընտրված հասցեներ,պատվերի մասին ինֆորմացիա,մեքենայի մոտեցման մասին " +
	"ծանուցում և շատ այլ հնարավորություններ Ձեր հարմարության համար :<br> Կոնտակտներ: <br>+374 60 644 644, <br>" +
    "<a href='#' onclick='openUrl(\"http://taxoline.am\");'>www.taxoline.am</a>" +
    "<br>" +
    "<a href='#' onclick='openUrl(\"http://Instagram.com/TaxoLine\");'>Instagram.com/TaxoLine/</a><br>" +
    "<a href='#' onclick='openUrl(\"http://facebook.com/taxoline\");'>facebook.com/taxoline </a><br>" +
    "</center><br>";

online_messages['ru']["Телефон водителя: "] = "Телефон водителя: ";
online_messages['am']["Телефон водителя: "] = "Վարորդի հեռախոսահամարը՝ ";
online_messages['en']["Телефон водителя: "] = "Driver Phone: ";

online_messages['ru']["номер"] = "номер";
online_messages['en']["номер"] = ": ";
online_messages['am']["номер"] = "` ";

online_messages['ru']["TaxoLine: Заказ такси"] = "TaxoLine: Заказ такси";
online_messages['en']["TaxoLine: Заказ такси"] = "TaxoLine: Taxi Order";
online_messages['am']["TaxoLine: Заказ такси"] = "TaxoLine: Տաքսու պատվեր";

online_messages['ru']["На ваш заказ назначена машина"] = "На ваш заказ назначена машина";
online_messages['en']["На ваш заказ назначена машина"] = "Your Car";
online_messages['am']["На ваш заказ назначена машина"] = "Ձեր մեքենան";


online_messages['ru']["Add Company"] = "Добавить компанию";
online_messages['am']["Add Company"] = "Ավելացնել ընկերություն";
online_messages['en']["Add Company"] = "Add Company";

var colors = new Array();
colors['ru'] =   new Array();
colors['en'] =   new Array();
colors['am'] =   new Array();

colors['ru'] = "бежевый|белый|бордовый|голубой|желтый|зеленый|золотой|индиго|коричневый|красный|оранжевый|розовый|светло-голубой|светло-зеленый|светло-коричневый|светло-красный|светло-серый|светло-синий|серебристый|серый|синий|темно-голубой|темно-зеленый|темно-коричневый|темно-красный|темно-серый|темно-синий|фиолетовый|черный".split("|");
colors['am'] = "բեժ|սպիտակ|Burgundy|կապույտ|դեղին|Կանաչ|ոսկի|Ինդիգո|շագանակագույն|կարմիր|նարնջագույն|վարդագույն|երկնագույն|կանաչ լույս|թեթեւ շագանակագույն|թեթեւ կարմիր|թեթեւ մոխրագույն|երկնագույն|Արծաթագույն|մոխրագույն|կապույտ|մուգ երկնագույն|մուգ կանաչ|մուգ շագանակագույն|մուգ կարմիր|մուգ մոխրագույն|մուգ կապույտ|մանուշակագույն|սեւ".split("|");
colors['en'] = "beige|white|Burgundy|blue|yellow|green|gold|Indigo|brown|red|orange|pink|light blue|light green|light brown|light red|light grey|light blue|silver|gray|blue|dark cyan|dark green|dark brown|dark red|dark grey|dark blue|purple|black".split("|");

for(var i = 0; i < colors['ru'].length; i++)
{
	online_messages['am'][colors['ru'][i]] = colors['am'][i];
	online_messages['en'][colors['ru'][i]] = colors['en'][i];
	online_messages['ru'][colors['ru'][i]] = colors['ru'][i];
}
//На ваш заказ назначена машина:Mercedes-Benz черный номер 35 DO 047
var carAssigned = 
{
	getPattern: function()
	{
		
		var cols = colors['ru'].join("|")
		var _pattern = '(.*)\\:(.*)\\s('+cols+')\\s(номер)\\s(.*)';
		var re = new RegExp(_pattern, "gi");
		return re;
	},
	process:function(str)
	{
		var _re = this.getPattern();
		var found = _re.exec(str);
		 
		var currentLanguage = localStorage.getItem('currentLanguage') || 'ru';
		if(found != null)
		{
			return translatParsedMessage(found[1])+": " + found[2] +" "+ translatParsedMessage(found[3]) +" "+ translatParsedMessage(found[4])+" "+ found[5];
		}else
		{
			return false;
		}
	}
}
 
/*[Авто.Модель] [Авто.Цвет] номер [Авто.Номер]*/
var carDescription = 
{
	getPattern: function()
	{
		
		var cols = colors['ru'].join("|")
		var _pattern = '(.*)\\s('+cols+')\\s(номер)\\s(.*)';
		var re = new RegExp(_pattern, "gi");
		return re;
	},
	process:function(str)
	{
		var _re = this.getPattern();
		var found = _re.exec(str);
		 
		var currentLanguage = localStorage.getItem('currentLanguage') || 'ru';
		if(found != null)
		{
			return found[1] +" "+ translatParsedMessage(found[2]) +" "+ translatParsedMessage(found[3])+" "+ found[4];
		}else
		{
			return false;
		}
	}
}
//Телефон водителя: [Водитель.Телефон]

var driverPhone = 
{
	getPattern: function()
	{
		var _pattern = '(Телефон водителя: )(.*)'
		re = new RegExp(_pattern, "gi");
		return re; 
	},
	process:function(str)
	{
		var _re = this.getPattern();
		var found = _re.exec(str);
		 
		var currentLanguage = localStorage.getItem('currentLanguage') || 'ru';
		if(found != null)
		{
			return online_messages[currentLanguage][found[1]]+found[2];
		}else
		{
			return false;
		}
	}
}
//Прибытие: [Заказ.ВремяДоПрибытия]
//Ориентировочное время прибытия машины [Заказ.ВремяДоПрибытия]
online_messages ['am']['Прибытие: менее минуты'] = "Ժամանումը՝ րոպեից քիչ";
online_messages ['en']['Прибытие: менее минуты'] = "Arriving less than min";
online_messages ['ru']['Прибытие: менее минуты'] = "Прибытие: менее мин";

online_messages ['am']['прибытие: менее минуты'] = "ժամանումը՝ րոպեից քիչ";
online_messages ['en']['прибытие: менее минуты'] = "arriving less than min";
online_messages ['ru']['прибытие: менее минуты'] = "прибытие: менее мин";

online_messages ['am']['Прогноз прибытие: менее минуты'] = "Մոտավոր ժամանումը՝ րոպեից քիչ";
online_messages ['en']['Прогноз прибытие: менее минуты'] = "Forecast of arrival than a minute";
online_messages ['ru']['Прогноз прибытие: менее минуты'] = "Прогноз прибытие: менее минуты";

online_messages ['am']['Прибытие:'] = "Ժամանումը՝";
online_messages ['en']['Прибытие:'] = "Arrival:";
online_messages ['ru']['Прибытие:'] = "Прибытие: ";

online_messages ['am']['прибытие:'] = "ժամանումը՝";
online_messages ['en']['прибытие:'] = "arrival:";
online_messages ['ru']['прибытие:'] = "прибытие: ";

online_messages ['am']['Ориентировочное время прибытия машины'] = "Մեքենայի մոտավոր ժամանումը՝ ";
online_messages ['en']['Ориентировочное время прибытия машины'] = "Approximate time of arrival the car";
online_messages ['ru']['Ориентировочное время прибытия машины'] = "Ориентировочное время прибытия машины ";


online_messages ['am']['Ориентировочное время прибытия машины менее минуты'] = "Մեքենայի մոտավոր ժամանումը՝ րոպեից քիչ";
online_messages ['en']['Ориентировочное время прибытия машины менее минуты'] = "Approximate time of arrival of the car less than a minute";
online_messages ['ru']['Ориентировочное время прибытия машины менее минуты'] = "Ориентировочное время прибытия машины менее минуты";



online_messages ['am']['мин'] = "րոպե";
online_messages ['en']['мин'] = "min.";
online_messages ['ru']['мин'] = "мин.";
var ApproximateArrival1 = 
{
	getPattern: function()
	{
		var _pattern = '(Ориентировочное время прибытия машины)\\s(.*)\\s(мин)\.';
		re = new RegExp(_pattern, "gi");
		return re; 
	},
	process:function(str)
	{
		var _re = this.getPattern();
		var found = _re.exec(str)
		
		var currentLanguage = localStorage.getItem('currentLanguage') || 'ru';
		if(found != null)
		{
			return translatParsedMessage(found[1])+" "+translatParsedMessage(found[2])+" "+translatParsedMessage(found[3]);
		}else
		{
			return false;
		}
	}
}

var ApproximateArrival2 = 
{
	getPattern: function()
	{
		var _pattern = '(Прибытие:)\\s(.*)\\s(мин)\.';
		re = new RegExp(_pattern, "gi");
		return re; 
	},
	process:function(str)
	{
		var _re = this.getPattern();
		var found = _re.exec(str)
		
		var currentLanguage = localStorage.getItem('currentLanguage') || 'ru';
		if(found != null)
		{
			return translatParsedMessage(found[1])+" "+translatParsedMessage(found[2])+" "+translatParsedMessage(found[3]);
		}else
		{
			return false;
		}
	}
}
var Monts_am = 'հունվարի_փետրվարի_մարտի_ապրիլի_մայիսի_հունիսի_հուլիսի_օգոստոսի_սեպտեմբերի_հոկտեմբերի_նոյեմբերի_դեկտեմբերի'.split('_');
var Monts_ru = 'января_февраля_марта_апреля_мая_июня_июля_августа_сентября_октября_ноября_декабря'.split('_');
var Monts_en = 'January_February_March_April_May_June_July_August_September_October_November_December'.split('_');

for(var i = 0; i < Monts_ru.length; i++ )
{
		online_messages ['am'][Monts_ru[i]] = Monts_am[i];
		online_messages ['en'][Monts_ru[i]] = Monts_en[i];
		online_messages ['ru'][Monts_ru[i]] = Monts_ru[i];
		 
}
//2 июня 02:12
var DateTimeOnly = 
{
	getPattern: function()
	{
		var _pattern = '(.*)\\s('+Monts_ru.join('|')+')(.*)';
		re = new RegExp(_pattern, "gi");
		return re; 
	},
	process:function(str)
	{
		var _re = this.getPattern();
		var found = _re.exec(str)
		
		var currentLanguage = localStorage.getItem('currentLanguage') || 'ru';
		if(found != null)
		{
			return found[1]+" "+translatParsedMessage(found[2])+" "+found[3];
		}else
		{
			return false;
		}
	}
}
//2 июня 02:12 (Заказ отменен)
var DateTimeState = 
{
	getPattern: function()
	{
		var _pattern = '(.*)\\s('+Monts_ru.join('|')+')(.*)\\s\\((Заказ отменен)\\)';
		re = new RegExp(_pattern, "gi");
		return re; 
	},
	process:function(str)
	{
		var _re = this.getPattern();
		var found = _re.exec(str)
		
		var currentLanguage = localStorage.getItem('currentLanguage') || 'ru';
		if(found != null)
		{
			return found[1]+" "+translatParsedMessage(found[2])+" "+found[3] +" ("+translatParsedMessage(found[4])+")";
		}else
		{
			return false;
		}
	}
}
var patterns = [carAssigned,carDescription,ApproximateArrival2,ApproximateArrival1, driverPhone,DateTimeState,DateTimeOnly];
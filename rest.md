###Request

```json
{
  "url": "http://cloud.infinity.ru/App?SiteName=InfinityCity&AppName=TaxiCatalog",
  "type": "GET"
}
```
#####Response
```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, target-densitydpi=device-dpi" />
    <meta name="format-detection" content="telephone=no">
    <meta name="msapplication-tap-highlight" content="no" />

    <title>Infinity Taxi</title>

    <link rel="stylesheet" type="text/css" href="/Styles/Applications/Main.css" />


    <script src='/Scripts/Applications/GlobalConsts.js'></script>
    <script src='/Scripts/Applications/Platform.js'></script>
    <script src='/Scripts/Applications/mobile-detect.min.js'></script>
    <script src='/Scripts/Applications/jquery-1.10.2.js'></script>
    <script src='/Scripts/Applications/jquery-ui.js'></script>
    <script src='/Scripts/Applications/StylesManager.js'></script>
    <script src='/Scripts/Applications/UtilsConsts.js'></script>
    <script src='/Scripts/Applications/Utils.js'></script>
    <script src='/Scripts/Applications/Controls.js'></script>
	<script src='/Scripts/Applications/ControlsConsts.js'></script>
    <script src='/Scripts/Applications/TaxiClient/TaxiCatalogApplication.js'></script>
    <script src='/Scripts/Applications/TaxiClient/TaxiClientApplication.js'></script>
	<script src='/Scripts/Applications/TaxiClient/TaxiClientConsts.js'></script>
    <script src='https://api-maps.yandex.ru/2.1/?lang=ru_RU'></script>

    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
    <script src='/Scripts/Applications/MxMap.js'></script>
    <script data-main='/Scripts/Applications/require' src='/Scripts/Applications/require.js'></script>
	
<script>
    
function splashHide() {
    $("#dvSplash").hide();
    $("#dvHourglass").hide();
}

function splashMessage(message_) {
    $("#dvSplashText").html(message_);
    $("#dvSplash").show();
}

</script>
    <script>

        var yandexAPI = false;
        if (ymaps)
            yandexAPI = true;

        function bodyLoaded() {
            log("Body loaded");

            if (history && history.pushState) {
                log("push first state");
                history.pushState({}, applicationTitle);
            }

            platformStartApplication(function () {
                log("PlatformStartApplication");

                appTaxiCatalog.startApplication();
            });
        }

    </script>

</head>
<body onload="bodyLoaded()">

    <div id="dvBody" class="dvBody">
        <div id="dvSplash" class="centeredContentWrapper">
            <div id="dvSplashText" class="centeredContent">
            </div>
        </div>
    </div>

    <div id="dvHourglass" class="hourglass">
    </div>
    <div id="dvDisabler" class="disabler">
    </div>
</body>
</html>
```

###Request
```json
{
  "url": "http://81.16.7.27:10888/App?SiteName=&AppName=TaxiClient",
  "type": "GET"
}
```

#####Response
```json
{
  "Result": "OK"
}
```

###Request
```json
{
  "url": "http://81.16.7.27:10888/data/GlobalAction?ActionName=TaxiClient_GetGlobalParams"
}
```

#####Response
```json
{
  "TaxiStationName": "TaxoLine",
  "TaxiStationPhone": "+37460644644",
  "TaxiStationPhoneDisplay": "0(60) 644-644",
  "PhonePlaceholder": "+___________",
  "ConfirmationCodePlaceholder": "____",
  "KladrDefaultRegionID": "5101556798",
  "KladrDefaultCityID": "5101556799",
  "KladrDefaultCityName": "Ереван",
  "KladrDefaultCityDesription": "Город Ереван",
  "CreateOrderMode": "Map",
  "IgnoreAddressPrefixes": [
    "Москва, Россия, ",
    "Город Москва, ",
    "Область Московская,"
  ],
  "MapType": "Yandex",
  "Payments": [
    {
      "ID": "Cash",
      "Name": "Наличные",
      "Description": "Оплата водителю наличными",
      "Image": "payment.png"
    }
  ],
  "DefaultPaymentID": "Cash",
  "DefaultPaymentName": "Наличные",
  "DefaultPaymentDescription": "Оплата водителю наличными",
  "Tariffs": [
    {
      "ID": "5003621472",
      "Name": "6 местный-Город",
      "Description": "Мин. 1000др/4км, 150др/км, ожидание 5мин/100др",
      "Image": "tariff.png"
    },
    {
      "ID": "5118690416",
      "Name": "Shopping",
      "Description": "400 драм с любой точки Еревана до ТЦ Dalma",
      "Image": "tariff.png"
    },
    {
      "ID": "5025555996",
      "Name": "VIP Город",
      "Description": "Мин. 800др/4км, 150др/км, ожидание 5мин/150др",
      "Image": "tariff.png"
    },
    {
      "ID": "5034595350",
      "Name": "Аэропорт",
      "Description": "Мин. 2000др/15км, 100др/км, ожидание 5мин/100др",
      "Image": "tariff.png"
    },
    {
      "ID": "5076056819",
      "Name": "Аэропорт-6 местный",
      "Description": "Мин. 5000др/15км, за 150др/км, ожидание 5мин/150др",
      "Image": "tariff.png"
    },
    {
      "ID": "5034595356",
      "Name": "Аэропорт-VIP",
      "Description": "Мин. 3000др/15км, 150др/км, ожидание 5мин/150др",
      "Image": "tariff.png"
    },
    {
      "ID": "5000440024",
      "Name": "Город",
      "Description": "Мин. 600др/4км, 100др/км, ожидание 5мин/100др",
      "Image": "tariff.png"
    },
    {
      "ID": "5108938516",
      "Name": "За Город",
      "Description": "Мин. 600др/4км, 100др/км, ожидание 5мин/100др",
      "Image": "tariff.png"
    },
    {
      "ID": "5097749176",
      "Name": "Трансфер из аэропорта",
      "Description": "Мин. 5000др/15км, 100др/км, ожидание 5мин/100др",
      "Image": "tariff.png"
    }
  ],
  "DefaultTariffID": "5000440024",
  "DefaultTariffName": "Город",
  "DefaultTariffDescription": "Мин. 600др/4км, 100др/км, ожидание 5мин/100др",
  "Options": []
}
```

#####/data/GlobalAction?ActionName=TaxiClient_CheckUserAuthorized
<details>
<summary>Request</summary>
<pre>
{
  "url": "http://81.16.7.27:10888/data/GlobalAction?ActionName=TaxiClient_CheckUserAuthorized",
  "params": {
    "DevicePushCookie": "",
    "ApplicationName": "ru.infinity.taxi.client.taxoline",
    "ApplicationVersion": "1.1.26"
  }
}
</pre>
</details>


<details>
<summary>Response</summary>
<pre>
{
  "IDClient": "",
  "DefaultPhone": "37441686488",
  "DefaultPhoneDisplay": "37441686488",
  "ClientName": "37441686488"
}
</pre>
</details>
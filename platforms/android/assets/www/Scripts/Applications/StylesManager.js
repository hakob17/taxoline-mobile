﻿var browserMode = "desktop";
var fontSize = null;
var picSize = 1.5;
var picSizeEm = "1.5em";
var picSizeEx = 1.5;
var picSizeExEm = "1.5em";
var overlayDefaultBackground = "rgba(255,255,255,0.5)";
var overlayTransparentBackground = "rgba(255,255,255,0.0)";

function addCSSRule(target_, name_, value_) {
		console.log("addCSSRule",target_, name_, value_,this);
		
    if (document.styleSheets) {
        if (document.styleSheets[0]) {

            $.each(document.styleSheets[0].cssRules, function () {
                if (this.selectorText == target_) {
                    if (name_ == "background")
                        this.style.background = value_;
                    else if (name_ == "backgroundImage")
                        this.style.backgroundImage = value_;
                    else if (name_ == "backgroundColor")
                        this.style.backgroundColor = value_;
                    else if (name_ == "backgroundRepeat")
                        this.style.backgroundRepeat = value_;
                    else if (name_ == "backgroundPositionX")
                        this.style.backgroundPositionX = value_;
                    else if (name_ == "backgroundPositionY")
                        this.style.backgroundPositionY = value_;
                    else if (name_ == "backgroundSize")
                        this.style.backgroundSize = value_;
                    else if (name_ == "fontSize")
                        this.style.fontSize = value_;
                    else if (name_ == "maxWidth")
                        this.style.maxWidth = value_;
                    else if (name_ == "cursor")
                        this.style.cursor = value_;
                    else if (name_ == "width")
                        this.style.width = value_;
                    else if (name_ == "height")
                        this.style.height = value_;
                    else if (name_ == "color")
                        this.style.color = value_;
                    else if (name_ == "border")
                        this.style.border = value_;
                    else if (name_ == "borderBottom")
                        this.style.borderBottom = value_;
                    else if (name_ == "padding")
                        this.style.padding = value_;

                    return false;
                }
            });
        }
    }
}

addCSSRule(".hourglass", "backgroundImage", "url(" + getLocalURLPrefix() + "../../Images/Applications/loading.gif)");

function styleSetDay() {
    addCSSRule("body", "color", "#444");
    addCSSRule("body", "backgroundColor", "#eee");
    addCSSRule("input, textarea", "color", "#444");
    addCSSRule("input, textarea", "backgroundColor", "white");
    addCSSRule(".border", "border", "1px solid #ccc");
    addCSSRule(".dvBody", "backgroundColor", "white");
    addCSSRule(".hourglass", "backgroundColor", "rgba(255,255,255,0.9)");
    addCSSRule(".fullScreenWrapper", "backgroundColor", "white");
    addCSSRule(".fullScreenOverlay", "backgroundColor", "rgba(255,255,255,0.5)");
    overlayDefaultBackground = "rgba(255,255,255,0.5)";
    overlayTransparentBackground = "rgba(255,255,255,0.0)";
    addCSSRule(".centeredContentWrapper", "backgroundColor", "white");
    //addCSSRule(".button", "border", "1px solid #ccc");
    addCSSRule(".disabledButton", "color", "#999 !important");
    addCSSRule(".button-highlited", "backgroundColor", "rgba(100, 184, 223, 0.5)");
    addCSSRule(".button-hover", "backgroundColor", "rgba(100, 184, 223, 0.5)");
    addCSSRule(".greenButton", "backgroundColor", "#8ecb60");
    addCSSRule(".greenButton", "сolor", "white");
    addCSSRule(".button-hover.greenButton", "backgroundColor", "#74bc48");
    addCSSRule(".redButton", "backgroundColor", "#ED6E72");
    addCSSRule(".redButton", "сolor", "white");
    addCSSRule(".button-hover.redButton", "backgroundColor", "#E1232A");
    addCSSRule(".scrollableControl", "backgroundColor", "white");
    addCSSRule(".listCaption", "borderBottom", "1px solid #eee");
    addCSSRule(".listItem", "borderBottom", "1px solid #eee");
    addCSSRule(".smallComment", "color", "#999");
    addCSSRule(".timeSelectorCaption", "color", "#999");
    addCSSRule(".timeOverlay", "background", "linear-gradient(to top, rgba(255,255,255,1.0), rgba(255,255,255,0.0), rgba(255,255,255,1.0))");
}

function styleSetNight() {

    // 444 -> bbb
    // white -> black
    // ccc -> 444
    // 255,255,255 -> 0,0,0
    // 999 -> 777
    // eee -> 333

    addCSSRule("body", "color", "#bbb");
    addCSSRule("body", "backgroundColor", "#222");
    addCSSRule("input, textarea", "color", "#bbb");
    addCSSRule("input, textarea", "backgroundColor", "black");
    addCSSRule(".border", "border", "1px solid #444");
    addCSSRule(".dvBody", "backgroundColor", "black");
    addCSSRule(".hourglass", "backgroundColor", "rgba(0,0,0,0.9)");
    addCSSRule(".fullScreenWrapper", "backgroundColor", "black");
    addCSSRule(".fullScreenOverlay", "backgroundColor", "rgba(50,50,50,0.5)");
    overlayDefaultBackground = "rgba(50,50,50,0.5)";
    overlayTransparentBackground = "rgba(50,50,50,0.2)";
    addCSSRule(".centeredContentWrapper", "backgroundColor", "black");
    //addCSSRule(".button", "border", "1px solid #444");
    addCSSRule(".disabledButton", "color", "#777 !important");
    addCSSRule(".button-highlited", "backgroundColor", "rgba(100, 184, 223, 0.5)");
    addCSSRule(".button-hover", "backgroundColor", "rgba(100, 184, 223, 0.5)");
    addCSSRule(".greenButton", "backgroundColor", "#46732B");
    addCSSRule(".greenButton", "color", "black");
    addCSSRule(".button-hover.greenButton", "backgroundColor", "#2C471B");
    addCSSRule(".redButton", "backgroundColor", "#A32327");
    addCSSRule(".redButton", "color", "black");
    addCSSRule(".button-hover.redButton", "backgroundColor", "#851317");
    addCSSRule(".scrollableControl", "backgroundColor", "black");
    addCSSRule(".listCaption", "borderBottom", "1px solid #333");
    addCSSRule(".listItem", "borderBottom", "1px solid #333");
    addCSSRule(".smallComment", "color", "#777");
    addCSSRule(".timeSelectorCaption", "color", "#777");
    addCSSRule(".timeOverlay", "background", "linear-gradient(to top, rgba(0,0,0,1.0), rgba(0,0,0,0.0), rgba(0,0,0,1.0))");
}

var md = new MobileDetect(window.navigator.userAgent);
if (md) {
    if (md.mobile() == "iPad")
        browserMode = "iPad";
    else if (md.mobile() == "iPhone")
        browserMode = "iPhone";
    else if (md.phone())
        browserMode = "defaultPhone";
    else if (md.tablet())
        browserMode = "defaultTablet";


    log("mobile-detect success");
    log(browserMode);


    var maxWidth = null;
    var buttonCursor = null;
    var listCaptionPadding = null;
    var captionCenterTDPadding = null;
    //var selectorButtonPadding = null;
    //var selectorButtonBigPadding = "0.5em 0.5em 0 2.0em";

    if (browserMode == "desktop") {
        fontSize = "170%";
        maxWidth = "25em";
        buttonCursor = "pointer";
        //selectorButtonPadding = "1.5em 0.1em 0 0.1em";
        picSize = 1.2;
    }
    else if (browserMode == "iPad") {
        fontSize = "200%";
        //maxWidth = "25em";
        //selectorButtonPadding = "1.5em 0.1em 0 0.1em";
    }
    else if (browserMode == "iPhone") {
        fontSize = "120%";
        //maxWidth = "25em";
        listCaptionPadding = "0.125em 0.125em 0 0.125em";
        //selectorButtonPadding = "1.5em 0.1em 0 0.1em";
        captionCenterTDPadding = "0.125em 0 0 0";
    }
    else if (browserMode == "defaultPhone") {
        fontSize = "120%";
        //maxWidth = "25em";
        listCaptionPadding = "0.125em 0.125em 0 0.125em";
        //selectorButtonPadding = "1.5em 0.1em 0 0.1em";
        captionCenterTDPadding = "0.125em 0 0 0";

        if (md.version('Webkit') == "534.3" || md.version('Webkit') == "533.1") { // маленькие телефоны
            var res = window.devicePixelRatio || 1;

            fontSize = (120 * res) + "%";

        }

    }
    else if (browserMode == "defaultTablet") {
        fontSize = "170%";
        //maxWidth = "25em";
        listCaptionPadding = "0.125em 0.125em 0 0.125em";
        //selectorButtonPadding = "1.5em 0.1em 0 0.1em";
        //selectorButtonBigPadding = "0.6em 0.5em 0 2.0em";
    }

    picSizeEm = picSize + "em";

    if (fontSize)
        addCSSRule("body", "fontSize", fontSize);
    if (maxWidth) {
        addCSSRule("body", "maxWidth", maxWidth);
        addCSSRule(".mainMenu", "maxWidth", maxWidth);
        addCSSRule(".dvTopToolbar", "maxWidth", maxWidth);
        addCSSRule(".fullScreenOverlay", "maxWidth", maxWidth);
        addCSSRule(".fullScreenWrapper", "maxWidth", maxWidth);
    }
    if (buttonCursor) {
        addCSSRule(".button", "cursor", buttonCursor);
        addCSSRule(".captionButton", "cursor", buttonCursor);
        addCSSRule(".textButton", "cursor", buttonCursor);
        addCSSRule(".transparentButton", "cursor", buttonCursor);
    }
    if (listCaptionPadding)
        addCSSRule(".listCaption", "padding", listCaptionPadding);
    //if (selectorButtonPadding)
    //    addCSSRule(".selectorButton", "padding", selectorButtonPadding);
    //if (selectorButtonBigPadding)
    //    addCSSRule(".selectorButtonBig", "padding", selectorButtonBigPadding);
    if (captionCenterTDPadding)
        addCSSRule(".captionCenterTD", "padding", captionCenterTDPadding);

    picSizeEx = picSize + 0.1;
    picSizeExEm = picSizeEx + "em";

    addCSSRule(".listItemIcon", "width", picSizeExEm);
    addCSSRule(".listItemIcon", "height", picSizeExEm);
    addCSSRule(".listItemIconRatingStar", "width", picSizeExEm);
    addCSSRule(".listItemIconRatingStar", "height", picSizeExEm);

    addCSSRule(".selectorButtonBig img", "width", picSize * 1.25 + "em");
    addCSSRule(".selectorButtonBig img", "height", picSize * 1.25 + "em");
    addCSSRule(".selectorButton img", "width", picSize * 1.25 + "em");
    addCSSRule(".selectorButton img", "height", picSize * 1.25 + "em");

    addCSSRule(".listItemLeftTD, .listItemRightTD", "width", picSizeExEm);
    addCSSRule(".listItemLeftTD, .listItemRightTD", "height", picSizeExEm);
    
    addCSSRule(".hourglass", "backgroundSize", picSize*2+"em");

}
else {
    log("mobile-detect failed");
}

﻿var nativeTransitions = true;
var nativeTransitionsSleep = 50;
var nativeNotifications = false;

var _dataURLPrefix;
var _dataPromoPrefix;

function getDataURLPrefix() {
    return _dataURLPrefix;
}
function getDataPromoPrefix() {
    return _dataPromoPrefix;
}

function getLocalURLPrefix() {
    return "";
}

function setDataURLPrefix(value_) {
    _dataURLPrefix = value_;
}
function setDataPromoPrefix(value_) {
    _dataPromoPrefix = value_;
}

function addZero(x, n) {
    while (x.toString().length < n) {
        x = "0" + x;
    }
    return x;
}

var logID = 0;

function log(message_) {

    return;
    //console.log(message_);
    /*

     if (logID == 0) {
     logID = Math.round(1000 + 8000 * Math.random());
     }

     var d = new Date();
     var h = addZero(d.getHours(), 2);
     var m = addZero(d.getMinutes(), 2);
     var s = addZero(d.getSeconds(), 2);
     var ms = addZero(d.getMilliseconds(), 3);
     var time = h + ":" + m + ":" + s + "." + ms;

     $.ajax({
     type: "GET",
     url: "https://cloud.infinity.ru:9999/log?" + logID + " [" + time + "]: " + message_,
     async: true,
     });
     */
}

function cxDial(number_) {
    window.open('tel:' + number_, '_system');
}

var startCallBack = null;
var deviceReady = false;

var pushCookie = "";
var pushCookieReceivedCallBack = null;

var positionLat = 0;
var positionLon = 0;

function startAppWrapperEx(callback_) {
    log("startAppWrapperEx");
    /*
     if (navigator.geolocation) {

     var canceled = false;
     var timer = null;

     navigator.geolocation.getCurrentPosition(
     function (position) {
     log("geolocation success");
     positionLat = position.coords.latitude;
     positionLon = position.coords.longitude;

     if (timer)
     clearTimeout(timer);

     if (!canceled)
     startAppWrapper(callback_);
     },
     function handle_error(err) {
     log("geolocation failed: " + err.message);

     if (timer)
     clearTimeout(timer);

     if (!canceled)
     startAppWrapper(callback_);
     });

     timer = setTimeout(function () {
     canceled = true;
     startAppWrapper(callback_);
     }, 5000);

     }
     else {
     startAppWrapper(callback_);
     }
     */

    cxGeoLocate(function (lat_, lon_, acc_) {
        log("geolocation success lat=" + lat_ + " lon=" + lon_ + " acc=" + acc_);
        positionLat = lat_;
        positionLon = lon_;

        startAppWrapper(callback_);
    }, function () {
        log("geolocation failed");
        startAppWrapper(callback_);
    }, 5000);

}

function startAppWrapper(callback_) {
    log("startAppWrapper");

    // StatusBar.backgroundColorByHexString("#333");

    callback_(); // вызываем колбек сразу, не дожидаясь регистрации в GCM

    var deviceInfo = "";
    if (!IsNull(device))
        deviceInfo = JSON.stringify(device);

    var deviceID = "";
    var serviceName = "APN";

    if (cordova.platformId == 'android') {
        serviceName = "GCM";
    }

    try {

        var counter = 0;
        var pushTimer = null;

        function pushRegister() {
            log("Push service registering... trying=" + counter);

            var push = PushNotification.init({
                android: {
                    senderID: pushGCMSenderID
                },
                ios: {
                    alert: "true",
                    badge: "true",
                    sound: "true"
                },
                windows: {}
            });

            push.on('registration', function (data) {
                log("Push service registered success");
                deviceID = data.registrationId;
                log("Push device ID=" + deviceID);

                if (pushTimer != null) {
                    clearInterval(pushTimer);
                    pushTimer = null;
                }

                var params = {
                    DeviceID: deviceID,
                    DeviceInfo: deviceInfo,
                    ApplicationName: applicationName,
                    ApplicationVersion: applicationVersion,
                    PositionLat: positionLat,
                    PositionLon: positionLon,
                    ServiceName: serviceName
                };

                cxAjax({
                    url: "https://cloud.infinity.ru/data/GlobalAction?SiteName=InfinityCity&ActionName=PushService_GetPushCookie",
                    params: params,
                    background: true,
                    onSuccess: function (data) {

                        var parsed = null;
                        if (data != null && data != "") {
                            parsed = $.parseJSON(data);
                            pushCookie = parsed.PushCookie;
                        }

                        log("Push register at cloud.infinity.ru success pushCookie=" + pushCookie);
                        //callback_();

                        if (pushCookieReceivedCallBack != null)
                            pushCookieReceivedCallBack(pushCookie);
                    },
                    onError: function (data) {
                        log("Push register at cloud.infinity.ru failed");
                        //callback_();
                    }
                });
            });

            push.on('error', function (e) {
                log("Push service register failed");
                log(e.message);
            });

            push.on('notification', function (data) {


                // data.message,
                // data.title,
                // data.count,
                // data.sound,
                // data.image,
                // data.additionalData

                log("Push message: " + data.message);

                log(data);

                if (data.additionalData && data.additionalData.foreground) {
                    navigator.notification.alert(
                        translatParsedMessage(data.message),
                        function () {
                        },
                        translatParsedMessage(applicationTitle),
                        'OK'
                    );

                    if (cordova.platformId == 'android') {
                        navigator.notification.beep(1);
                    }
                }
            });
        }

        pushTimer = setInterval(function () {
            counter++;
            if (counter > 5) {
                log("Push registering canceled: trying count exceed");
                if (pushTimer != null) {
                    clearInterval(pushTimer);
                    pushTimer = null;
                }
            }
            else {
                pushRegister();
            }
        }, 60000);

        setTimeout(pushRegister, 10000);

    }
    catch (exception) {
        log("Push service register exception: " + exception.message);
        //callback_();
    }
}

var deviceReadyCalled = false;

function doDeviceReady() {
    if (deviceReadyCalled)
        return;

    deviceReadyCalled = true;

    if (startCallBack) {
        log("deviceready has callback");
        startAppWrapperEx(startCallBack);
    }
    else {
        log("deviceready does not have callback");
        deviceReady = true;
    }
}

var deviceReadyTimer = null;

function platformStartApplication(callback_) {
    if (deviceReady) {
        log("platformStartApplication: ready");
        startAppWrapperEx(callback_);
    }
    else {
        log("platformStartApplication: not ready");
        startCallBack = callback_;

        deviceReadyTimer = setTimeout(function () {
            deviceReadyTimer = null;
            log("platformStartApplication: device ready timeout!");
            doDeviceReady();
        }, 5000);

    }
}

document.addEventListener('deviceready', function () {
    if (deviceReadyTimer != null) {
        clearTimeout(deviceReadyTimer);
        deviceReadyTimer = null;
    }

    doDeviceReady();
});

document.addEventListener('touchend', function (e) {
    var cxUnhoverMatched = (!e.target || !e.target.hasOwnProperty('cxUnhover')) ? '-' : e.target.attr('cxUnhover');
    $('[cxUnhover]').each(function () {
        if ($(this).attr('cxUnhover') != cxUnhoverMatched) {
            $(this).addClass($(this).attr('cxUnhoverClass'));
        }
    });
}, false);
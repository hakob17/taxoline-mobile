﻿_months = [UtilsConsts.januaryShort, UtilsConsts.februaryShort, UtilsConsts.marchShort, UtilsConsts.aprilShort, UtilsConsts.mayShort, UtilsConsts.juneShort,
    UtilsConsts.julyShort, UtilsConsts.augustShort, UtilsConsts.septemberShort, UtilsConsts.octoberShort, UtilsConsts.novemberShort, UtilsConsts.decemberShort];
_monthsFull = [UtilsConsts.januaryLong, UtilsConsts.februaryLong, UtilsConsts.marchLong, UtilsConsts.aprilLong, UtilsConsts.mayLong, UtilsConsts.juneLong,
    UtilsConsts.julyLong, UtilsConsts.augustLong, UtilsConsts.septemberLong, UtilsConsts.octoberLong, UtilsConsts.novemberLong, UtilsConsts.decemberLong];


// Safari doesn't know startsWith :)
String.prototype.startsWith = function (str) {
    return (this.match("^" + str) == str)
}

function scrollToTop() {
    window.scrollTo(0, 0);
    $("#dvHourglass").css("margin-top", "0px");
}

window.onscroll = window.onresize = function () {
    var doc = document.documentElement;
    var top = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);
    $("#dvHourglass").css("margin-top", top + "px");
}

var hourGlassState = "hidden";

window.onpopstate = function () {

    if (hourGlassState != "hidden")
        return false;

    if ($("#dvDisabler").is(':visible'))
        return false;

    if (uiStack && uiStack.length > 1) {

        var handled = false;

        var elements = uiStack[uiStack.length - 1].find('*');
        elements.each(function () {
            if ($(this).prop("cxKind") == "Back") {

                var params = $(this).prop("cxBackParams");
                if (params && params.onClick)
                    params.onClick();

                handled = true;
                return false;
            }
        });

        if (!handled)
            uiBack();
    }
    return false;
}

function IsNull(value_) {
    return (value_ == null || value_ == undefined);
}

function IsNullOrEmpty(value_) {
    return IsNull(value_) || value_ === "";
}

function IsNullOrEmptyOrFalse(value_) {
    return IsNullOrEmpty(value_) || value_ === false ? true : false;
}

function CxCoalesce(value_, nullValue_) {
    if (IsNull(value_))
        return nullValue_;
    else
        return value_;
}

function generateUUID() {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return uuid;
};

function cxShowHourglass(callback_) {
    log("cxShowHourglass");

    if (callback_)
        callback_();

    if (hourGlassState == "hideSoon") {
        hourGlassState = "visible";
        return;
    }

    var hg = $("#dvHourglass");
    hourGlassState = "deferred";

    setTimeout(function () {
        if (hourGlassState == "deferred") {
            $('#dvBody').addClass("blur");

            hg.show();
            hourGlassState = "visible";
        }
    }, 500);
}

function cxHideHourglass(callback_) {
    log("cxHideHourglass");

    var hg = $("#dvHourglass");

    if (hourGlassState == "hidden") {
        if (callback_)
            callback_();
    }
    else if (hourGlassState == "deferred") {
        hourGlassState = "hidden";
        hg.hide();
        $('#dvBody').removeClass("blur");

        if (callback_)
            callback_();
    }
    else if (hourGlassState == "visible") {

        hourGlassState = "hideSoon";

        setTimeout(function () {

            if (hourGlassState != "hideSoon")
                return;

            hg.hide();
            hourGlassState = "hidden";

            $('#dvBody').removeClass("blur");

            if (callback_)
                callback_();
        }, 100);
    }
}

function cxAjax(params_) {

    var dvDisabler = $("#dvDisabler");

    if (params_.type == undefined) {
        params_.type = "POST";
    }
    if (params_.contentType == undefined) {
        params_.contentType = "application/json";
    }

    var data = null;
    if (params_.params)
        data = JSON.stringify(params_.params);

    var hgShow = cxShowHourglass;
    var hgHide = cxHideHourglass;

    if (params_.background) {
        hgHide = hgShow = function (callback_) {
            callback_();
        }
    }
    else {
        dvDisabler.show();
    }

    var async = true;
    if (params_.async == false)
        async = false;

    function main() {
        $.ajax({
            type: params_.type,
            url: params_.url,
            data: data,
            async: async,
            xhrFields: {
                withCredentials: true
            },
            contentType: params_.contentType,
            timeout: 10000,
            success: function (result, status, xhr) {
                hgHide(function () {
                    dvDisabler.hide();
                    if (params_.onSuccess)
                        params_.onSuccess(result, status, xhr);
                    if (params_.onComplete)
                        params_.onComplete();
                });
            },
            error: function (result) {
                hgHide(function () {
                    dvDisabler.hide();
                    if (params_.onError)
                        params_.onError(result);
                    if (params_.onComplete)
                        params_.onComplete();
                });
            },
        });
    }

    if (async == false) {
        hgShow();
        main();
    }
    else {
        hgShow(main);
    }
}

var uiStack = new Array();

function uiHide(content_, cb_) {
    content_.fadeOut(200, function () {
        if (cb_)
            cb_();
    });
}

function uiRefresh(content_) {
    $.each(content_, function () {
        if (this.cxRefresh) {
            this.cxRefresh();
        }
    });
}


function uiShow(content_, cb_, cbStarted_) {

    log("uiShow: uiStack.length (before push)=" + uiStack.length);

    if (history && history.pushState) {
        log("push state");
        history.pushState({}, applicationTitle);
    }

    var dvBody = $("#dvBody");
    var dvDisabler = $("#dvDisabler");
    dvDisabler.show();

    var oldContent = null;
    if (uiStack.length)
        oldContent = uiStack[uiStack.length - 1];

    function doShow() {
        dvBody.append(content_);
        uiStack.push(content_);
        log("uiShow: uiStack.length (after push)=" + uiStack.length);

        if (cbStarted_)
            cbStarted_();

        scrollToTop();

        content_.show();

        if (!nativeTransitions) {
            content_.fadeIn(200, function () {

                uiRefresh(content_);

                checkMustRemoveAllButLastAfterFirstShow();

                if (cb_)
                    cb_();
                dvDisabler.hide();
            });
        }
    }

    if (!nativeTransitions) {
        if (oldContent) {
            uiHide(oldContent, function () {
                doShow();
            });
        }
        else {
            doShow();
        }
    }
    else {

        var direction = "left";

        if (!IsNullOrEmpty(content_.attr("flipDirection"))) {
            direction = content_.attr("flipDirection");
        }
        var options = {
            "direction": direction, // 'left|right|up|down', default 'left' (which is like 'next')
            "duration": 300, // in milliseconds (ms), default 400
            "slowdownfactor": 4, // overlap views (higher number is more) or no overlap (1), default 4
            "iosdelay": 60, // ms to wait for the iOS webview to update before animation kicks in, default 60
            "androiddelay": 70, // same as above but for Android, default 70
            "winphonedelay": 200, // same as above but for Windows Phone, default 200,
            "fixedPixelsTop": 0, // the number of pixels of your fixed header, default 0 (iOS and Android)
            "fixedPixelsBottom": 0  // the number of pixels of your fixed footer (f.i. a tab bar), default 0 (iOS and Android)
        };

        log("slide left");
        window.plugins.nativepagetransitions.slide(
            options,
            function (msg) {
                log("success slide: " + msg);
                checkMustRemoveAllButLastAfterFirstShow();
                if (cb_)
                    cb_();
                dvDisabler.hide();
                scrollToTop();
            }, // called when the animation has finished
            function (msg) {
                log("failed slide: " + msg);
                checkMustRemoveAllButLastAfterFirstShow();
                if (cb_)
                    cb_();
                dvDisabler.hide();
                scrollToTop();
            } // called in case you pass in weird values
        );

        if (nativeTransitionsSleep > 0) {
            setTimeout(function () {
                if (oldContent) {
                    oldContent.hide();
                }
                doShow();
            }, nativeTransitionsSleep);
        }
        else {
            if (oldContent) {
                oldContent.hide();
            }
            doShow();
        }
    }
}

function uiBack(count_, cb_) {
    var dvDisabler = $("#dvDisabler");
    dvDisabler.show();

    log("uiBack (" + count_ + "): uiStack.length (before)=" + uiStack.length);
    var lastContent = uiStack.pop();

    if (lastContent) {
        var direction = "right";

        if (!IsNullOrEmpty(lastContent.attr("flipDirection"))) {
            var dir = lastContent.attr("flipDirection");
            if (dir == "left")
                direction = "right";
            else if (dir == "right")
                direction = "left";
            else if (dir == "up")
                direction = "down";
            else if (dir == "down")
                direction = "up";
        }

        if (!nativeTransitions) {

            uiHide(lastContent, function () {
                lastContent.remove();

                if (count_) {
                    while ((count_ > 1 || count_ < 0) && uiStack.length > 0) {
                        var prevContent = uiStack.pop();
                        prevContent.remove();
                        count_--;
                    }
                }

                log("uiBack (" + count_ + "): uiStack.length (after)=" + uiStack.length);

                if (uiStack.length) {
                    var newContent = uiStack[uiStack.length - 1];

                    scrollToTop();

                    newContent.show();

                    newContent.fadeIn(200, function () {
                        uiRefresh(newContent);
                        if (cb_)
                            cb_();
                        dvDisabler.hide();
                    });
                }
                else {
                    if (cb_)
                        cb_();
                    dvDisabler.hide();
                }
            });
        }
        else {

            if (count_) {
                while ((count_ > 1 || count_ < 0) && uiStack.length > 0) {
                    var prevContent = uiStack.pop();
                    prevContent.remove();
                    count_--;
                }
            }

            log("uiBack (" + count_ + "): uiStack.length (after)=" + uiStack.length);

            if (uiStack.length) {

                var options = {
                    "direction": direction, // 'left|right|up|down', default 'left' (which is like 'next')
                    "duration": 300, // in milliseconds (ms), default 400
                    "slowdownfactor": 4, // overlap views (higher number is more) or no overlap (1), default 4
                    "iosdelay": 60, // ms to wait for the iOS webview to update before animation kicks in, default 60
                    "androiddelay": 70, // same as above but for Android, default 70
                    "winphonedelay": 200, // same as above but for Windows Phone, default 200,
                    "fixedPixelsTop": 0, // the number of pixels of your fixed header, default 0 (iOS and Android)
                    "fixedPixelsBottom": 0  // the number of pixels of your fixed footer (f.i. a tab bar), default 0 (iOS and Android)
                };
                log("slide right");
                window.plugins.nativepagetransitions.slide(
                    options,
                    function (msg) {
                        log("success slide right: " + msg);
                        dvDisabler.hide();
                        scrollToTop();
                        if (cb_)
                            cb_();
                    }, // called when the animation has finished
                    function (msg) {
                        log("failed slide right: " + msg);
                        dvDisabler.hide();
                        scrollToTop();
                        if (cb_)
                            cb_();
                    } // called in case you pass in weird values
                );


                if (nativeTransitionsSleep > 0) {
                    setTimeout(function () {
                        lastContent.remove();
                        var newContent = uiStack[uiStack.length - 1];
                        scrollToTop();
                        newContent.show();
                        uiRefresh(newContent);
                    }, nativeTransitionsSleep);
                }
                else {
                    lastContent.remove();
                    var newContent = uiStack[uiStack.length - 1];
                    scrollToTop();
                    newContent.show();
                    uiRefresh(newContent);
                }

            }
            else {
                lastContent.remove();
                if (cb_)
                    cb_();
                dvDisabler.hide();
            }

        }
    }
    else {

        log("uiBack: no content!");

        if (cb_)
            cb_();
        dvDisabler.hide();
    }
}

function uiRemovePreLast() {
    if (uiStack.length >= 2) {

        log("uiRemovePreLast: uiStack.length (before)=" + uiStack.length);

        var last = uiStack.pop();
        var garbage = uiStack.pop();
        garbage.remove();
        uiStack.push(last);

        log("uiRemovePreLast: uiStack.length (after)=" + uiStack.length);

    }
}

var _mustRemoveAllButLastAfterFirstShow = false;
function uiRemoveAllButLastAfterFirstShow() {
    _mustRemoveAllButLastAfterFirstShow = true;
}

function checkMustRemoveAllButLastAfterFirstShow() {
    if (_mustRemoveAllButLastAfterFirstShow) {
        _mustRemoveAllButLastAfterFirstShow = false;
        uiRemoveAllButLast();
    }
}

function uiRemoveAllButLast() {
    if (uiStack.length >= 2) {

        log("uiRemoveAllButLast: uiStack.length (before)=" + uiStack.length);

        var last = uiStack.pop();
        $.each(uiStack, function () {
            var garbage = uiStack.pop();
            garbage.remove();
        });
        uiStack.push(last);

        log("uiRemoveAllButLast: uiStack.length (after)=" + uiStack.length);

    }
}

function leftFirstAndLast() {
    console.log(">>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<");
    log("uiRemoveAllButLast: uiStack.length (before)=" + uiStack.length);

    var last = uiStack.pop();
    var first = uiStack.shift();
    console.log(last, first);
    $.each(uiStack, function () {
        var garbage = uiStack.pop();
        garbage.remove();
    });
    uiStack.push(first);
    uiStack.push(last);
    console.log(last, first,uiStack);
    uiBack();
    log("uiRemoveAllButLast: uiStack.length (after)=" + uiStack.length);

}

function addClickHandler(element_, params_) {

    var id = generateUUID();

    $.each(element_, function () {
        this.cxID = id;

        var main = this;
        $.each($(main).find('*'), function () {
            this.cxID = id;
        });
    });

    var lastClickTime = new Date();

    function doClick() {
        if (element_.hasClass("disabledButton"))
            return;

        var currentTime = new Date();

        if (currentTime.getTime() - lastClickTime.getTime() < 200)
            return;

        lastClickTime = currentTime;

        if (params_.onClick)
            params_.onClick(element_);
    }

    function doHold() {
        if (element_.hasClass("disabledButton"))
            return;
        if (params_.onHold)
            params_.onHold(element_);
    }

    function doEnter() {
        if (element_.hasClass("disabledButton"))
            return;

        if (params_.transparent != true)
            element_.addClass("button-hover");
    }

    function doLeave() {
        if (element_.hasClass("disabledButton"))
            return;

        if (params_.transparent != true)
            element_.removeClass("button-hover");
    }

    var supportsTouch = ('ontouchstart' in document.documentElement);

    if (supportsTouch) {

        var isTouch = false;
        var target = null;
        var moveX = 0;
        var moveY = 0;

        var touchDistance = 20;
        var touchDuration = 500;

        var touchTimer = null;

        function cancelTouch() {
            isTouch = false;
            if (touchTimer) {
                clearTimeout(touchTimer);
                touchTimer = null;
            }
            doLeave();
        }

        element_.on("touchstart", function (e) {
            isTouch = true;
            doEnter();
            target = e.target;

            var touchMove = e.originalEvent.touches[0];
            moveX = touchMove.pageX;
            moveY = touchMove.pageY;

            touchTimer = setTimeout(function () {
                cancelTouch();
                doHold();
            }, touchDuration);

            e.stopPropagation();
        });

        element_.on("touchmove", function (e) {
            if (!isTouch)
                return;
            var touchMove = e.originalEvent.touches[0];
            newMoveX = touchMove.pageX;
            newMoveY = touchMove.pageY;

            if (Math.abs(moveX - newMoveX) > touchDistance || Math.abs(moveY - newMoveY) > touchDistance) {
                cancelTouch();
            }

            e.stopPropagation();
        });

        element_.on("touchend", function (e) {
            if (isTouch) {
                cancelTouch();
                if (e.target == target)
                    doClick();
            }

            element_.trigger("mouseleave");

            e.stopPropagation();
        });

        element_.on("touchcancel", function (e) {
            if (isTouch)
                cancelTouch();

            element_.trigger("mouseleave");

            e.stopPropagation();
        });
    }

    if (browserMode == "desktop") {

        element_.css("cursor", "pointer");

        var canClick = true;
        element_.click(function (e) {
            if (canClick)
                doClick();

            e.preventDefault();
            return false;
        });

        element_.on("mouseleave", function (e) {
            doLeave();
        });

        element_.on("mouseenter", function (e) {
            doEnter();
        });

        var isMouseDown = false;
        var mouseX = 0;
        var mouseY = 0;

        var mouseDistance = 20;
        var mouseDuration = 500;

        var mouseTimer = null;

        function cancelMouse() {
            isMouseDown = false;
            if (mouseTimer) {
                clearTimeout(mouseTimer);
                mouseTimer = null;
            }
            //doLeave();
        }

        element_.on("mousedown", function (e) {
            canClick = true;
            isMouseDown = true;

            mouseX = e.pageX;
            mouseY = e.pageY;

            mouseTimer = setTimeout(function () {
                canClick = false;
                cancelMouse();
                doHold();
            }, mouseDuration);

            e.preventDefault();
            return false;
        });

        element_.on("mousemove", function (e) {
            if (!isMouseDown)
                return;
            newMoveX = e.pageX;
            newMoveY = e.pageY;

            if (Math.abs(mouseX - newMoveX) > mouseDistance || Math.abs(mouseY - newMoveY) > mouseDistance) {
                cancelMouse();
            }

            e.preventDefault();
            return false;
        });

        element_.on("mouseup", function (e) {
            if (isMouseDown) {
                cancelMouse();
            }

            e.preventDefault();
            return false;
        });

    }
}

function cxGeoLocate(success_, failed_, timeout_) {
    log("cxGeolocation started...");

    var success = false;
    var timeout = false;

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
            function (position) {
                if (!timeout && !success) {
                    log("cxGeolocation accuracy success: " + position.coords.latitude + " " + position.coords.longitude + " " + position.coords.accuracy);
                    success = true;
                    if (success_)
                        success_(position.coords.latitude, position.coords.longitude, position.coords.accuracy);
                }

            },
            function handle_error(err) {
                log("cxGeolocation accuracy failed: " + err.message);
            }, {
                timeout: timeout_,
                enableHighAccuracy: true
            });

        navigator.geolocation.getCurrentPosition(
            function (position) {
                if (!timeout && !success) {
                    log("cxGeolocation not accuracy success: " + position.coords.latitude + " " + position.coords.longitude + " " + position.coords.accuracy);
                    success = true;
                    if (success_)
                        success_(position.coords.latitude, position.coords.longitude, position.coords.accuracy);
                }

            },
            function handle_error(err) {
                log("cxGeolocation not accuracy failed: " + err.message);
            }, {
                timeout: timeout_,
                enableHighAccuracy: false
            });

        setTimeout(function () {
            if (success)
                return;
            log("cxGeolocation timeout");
            timeout = true;
            if (failed_)
                failed_();
        }, timeout_);

    }
    else {
        if (failed_)
            failed_();
    }

}
function Intersection(arr1, arr2) {
    var intersection = [];
    arr1.forEach(function (item, i, arr1) {
        if (arr2.indexOf(item) != -1) {
            intersection.push(i);
        }
    });
    return intersection;
}

function compareWithPrecision(val1_, val2_, precision_) {

    var defaultPrecision = 5;

    if (val1_ && val2_) {
        if (!precision_) {
            precision_ = defaultPrecision;
        }

        var v1 = val1_.toFixed(precision_);
        var v2 = val2_.toFixed(precision_);

        if (v1 === v2) {
            return true;
        }
        else {
            return false;
        }

    }
    else {
        log("warning, args v1 or/and v2 is null");
        return false;
    }

}


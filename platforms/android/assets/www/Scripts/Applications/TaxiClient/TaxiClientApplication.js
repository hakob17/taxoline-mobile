﻿var globalParams = {};
var userParams = {};
var mapStartCenter = [40.177610, 44.512542];
var catalogMapStartCenter = null;
var mapStartCenterSet = false;

function TaxiClientApplication(dataURLPrefix_, dataPromoPrefix_, siteName_) {

    var that = this;

    var btPhoneCall = {
        backgroundData: "call.png",
        onClick: function () {
            phoneCall({flipDirection: "up"});
        }
    }
    /*
     var btPhoneCallLeft = {
     backgroundData: "call.png",
     onClick: function () {
     phoneCall({ flipDirection: "right" });
     }
     }
     */
    var btMenu = {
        backgroundData: "menu.png",
        onClick: mainMenu
    }

    btBackMenu = {
        left: btBack,
        right: btMenu
    }

    btBackOnly = {
        left: btBack,
        right: btPhoneCall
    }

    btPhoneMenu = {
        left: btPhoneCall,
        right: btMenu
    }

    var _lastGeoLocate = 0;

    function TaxiClientUpdatePushCookie(pushCookie_) {
        log("UpdatePushCookie: " + pushCookie_);

        cxAjax({
            url: getDataURLPrefix() + "/data/GlobalAction?ActionName=TaxiClient_UpdatePushCookie",
            params: {
                DevicePushCookie: pushCookie_,
            },
            background: true,
            onSuccess: function () {
                log("UpdatePushCookie success");
            },
            onError: function () {
                log("UpdatePushCookie failed");
            }
        });
    }

    function geoLocate() {

        var ticks = (new Date()).getTime();

        if (_lastGeoLocate > 0) {
            if (ticks - _lastGeoLocate < 60000) {
                log("geolocation canceled: too often");
                return;
            }
        }

        _lastGeoLocate = ticks;

        cxGeoLocate(function (lat_, lon_, acc_) {
            log("geolocation success lat=" + lat_ + " lon=" + lon_ + " acc=" + acc_);
            mapStartCenter = [lat_, lon_];
            mapStartCenterSet = true;
        }, function () {
            log("geolocation failed");
        }, 30000);

        /*
         if (navigator.geolocation) {
         navigator.geolocation.getCurrentPosition(
         function (position) {
         mapStartCenter = [position.coords.latitude, position.coords.longitude];
         mapStartCenterSet = true;
         },
         function handle_error(err) {
         // локатор
         });
         }
         else {
         // локатор
         }
         */
    }

    this.startApplication = function (params_) {


        log("startApplication [Client]");

        setDataURLPrefix(dataURLPrefix_);
        setDataPromoPrefix(dataPromoPrefix_);

        splashHide();

        var currentLanguage = localStorage.getItem('currentLanguage')

        geoLocate();

        cxAjax({
            url: "http://cloud.infinity.ru/App?SiteName=InfinityCity&AppName=TaxiCatalog", // цель этого запроса - получить куки для каталога, чтобы потом регистрироваться в пушах
            type: "GET",
            onComplete: function () {
                cxAjax({
                    url: getDataURLPrefix() + "/App?SiteName=" + siteName_ + "&AppName=TaxiClient", // проверяем доступность
                    type: "GET",
                    onSuccess: function (result) {
                        cxAjax({
                            url: getDataURLPrefix() + "/data/GlobalAction?ActionName=TaxiClient_GetGlobalParams",
                            onSuccess: function (result) {

                                globalParams = $.parseJSON(result);
                                if (mapType && globalParams.MapType) {
                                    mapType = globalParams.MapType;
                                }

                                checkGlobalParams();
                                translateGlobalParams();
                                var pushCookieIsNull = IsNullOrEmpty(pushCookie);

                                cxAjax({
                                    url: getDataURLPrefix() + "/data/GlobalAction?ActionName=TaxiClient_CheckUserAuthorized",
                                    params: {
                                        DevicePushCookie: pushCookie,
                                        ApplicationName: applicationName,
                                        ApplicationVersion: applicationVersion
                                    },
                                    onSuccess: function (result) {

                                        if (pushCookieIsNull) {
                                            log("Push cookie is null - update callback assigned");
                                            pushCookieReceivedCallBack = TaxiClientUpdatePushCookie;
                                        }

                                        userParams = $.parseJSON(result);

                                        mainPage(params_);
                                    },
                                    onError: function () {
                                        startAutorization(params_);
                                    }
                                });
                            },
                            onError: function () {
                                showMessage({
                                    message: TaxiClientAppConsts.serviceTemporaryUnavailableMessage,
                                    onButtonClick: function () {

                                        if (typeof appTaxiCatalog !== "undefined") {
                                            gotoTaxiCatalog({
                                                noCallback: function () {
                                                    uiBack(-1, function () {
                                                        that.startApplication();
                                                    });
                                                }
                                            });
                                        }
                                        else {
                                            that.startApplication();
                                        }
                                    }
                                });
                            }
                        });
                    },
                    onError: function () {
                        showMessage({
                            message: TaxiClientAppConsts.serviceTemporaryUnavailableMessage,
                            onButtonClick: function () {
                                that.startApplication();
                            }
                        });
                    }
                });
            }
        });


    }

    function translateGlobalParams() {
        if (globalParams == null || globalParams == undefined)
            globalParams = {};
        globalParams.KladrDefaultCityName = translatParsedMessage(globalParams.KladrDefaultCityName);
        for (var _Payment in globalParams.Payments) {
            globalParams.Payments[_Payment].Name = translatParsedMessage(globalParams.Payments[_Payment].Name);
        }


        for (var _Tariff in globalParams.Tariffs) {
            globalParams.Tariffs[_Tariff].Name = translatParsedMessage(globalParams.Tariffs[_Tariff].Name);
            globalParams.Tariffs[_Tariff].Description = translatParsedMessage(globalParams.Tariffs[_Tariff].Description);
        }

        globalParams.DefaultTariffName = translatParsedMessage(globalParams.DefaultTariffName);
        globalParams.DefaultTariffDescription = translatParsedMessage(globalParams.DefaultTariffDescription);
    }

    function checkGlobalParams() {
        log(globalParams)

        if (globalParams == null || globalParams == undefined)
            globalParams = {};
        if (IsNullOrEmpty(globalParams))
            globalParams.PhonePlaceholder = "__________";

        if (!mapStartCenterSet) {
            if (!IsNull(catalogMapStartCenter) && catalogMapStartCenter.length == 2) {
                mapStartCenter = [catalogMapStartCenter[0], catalogMapStartCenter[1]];
            }
            else if (!IsNull(globalParams.MapDefaultCenterLat) && !IsNull(globalParams.MapDefaultCenterLat)) {
                mapStartCenter = [globalParams.MapDefaultCenterLat, globalParams.MapDefaultCenterLon];
            }
        }
    }

    function startAutorization(params_) {
        var flipDirection = "left";
        if (!IsNull(params_) && !IsNullOrEmpty(params_.flipDirection))
            flipDirection = params_.flipDirection;

        var buttons = {};

        if (typeof appTaxiCatalog !== "undefined") {
            buttons = {
                left: {
                    backgroundData: "left.png",
                    onClick: gotoTaxiCatalog
                }
            };
        }

        showInputDigitsControl({
            flipDirection: flipDirection,
            transparent: true,
            prompt: TaxiClientAppConsts.yourPhoneNumberMessage,
            placeholder: globalParams.PhonePlaceholder,
            onClick: startPhoneConfirmation,
            buttons: buttons,
            autorization: true,
        });
    }

    function startPhoneConfirmation(phoneNumber_) {
        cxAjax({
            url: getDataURLPrefix() + "/data/GlobalAction?ActionName=TaxiClient_SendConfirmation",
            params: {
                PhoneNumber: phoneNumber_,
                DevicePushCookie: pushCookie,
                ApplicationName: applicationName,
                ApplicationVersion: applicationVersion
            },
            onSuccess: function () {
                showInputDigitsControl({
                    transparent: true,
                    prompt: TaxiClientAppConsts.SMSCodeMessage,
                    placeholder: globalParams.ConfirmationCodePlaceholder,
                    onClick: function (value) {
                        checkConfirmationCode(phoneNumber_, value);
                    },
                    buttons: {
                        left: btBack
                    }
                });
            },
            onError: tryLater
        });
    }

    function checkConfirmationCode(phoneNumber_, confirmationCode_) {
        var pushCookieIsNull = IsNullOrEmpty(pushCookie);

        cxAjax({
            url: getDataURLPrefix() + "/data/GlobalAction?ActionName=TaxiClient_CheckConfirmation",
            params: {
                PhoneNumber: phoneNumber_,
                ConfirmationCode: confirmationCode_,
                DevicePushCookie: pushCookie,
                ApplicationName: applicationName,
                ApplicationVersion: applicationVersion
            },
            onSuccess: function (result) {

                if (pushCookieIsNull) {
                    log("Push cookie is null - update callback assigned");
                    pushCookieReceivedCallBack = TaxiClientUpdatePushCookie;
                }

                userParams = $.parseJSON(result);
                mainPage();
            },
            onError: function (result) {
                if (result.status == 403) {
                    showMessage({
                        message: TaxiClientAppConsts.incorrectCodeMessage,
                        onButtonClick: function () {
                            uiBack();
                        }
                    });
                }
                else {
                    tryLater();
                }
            }
        });
    }

    function gotoTaxiCatalog(params_) {

        showConfirmation({
            flipDirection: "right",
            caption: TaxiClientAppConsts.confirmationCaption,
            message: TaxiClientAppConsts.goToSelectTaxiServiceQuestion,
            onButtonYesClick: function () {

                pushCookieReceivedCallBack = null;

                uiRemoveAllButLastAfterFirstShow();
                appTaxiCatalog.clearMyTaxiStation();
                /*
                 uiBack(-1, function () {
                 appTaxiCatalog.clearMyTaxiStation();
                 });*/
            },
            onButtonNoClick: function () {
                if (params_.noCallback)
                    params_.noCallback();
                else
                    uiBack();
            }
        });
    }

    function mainPage(params_) {
        updateActiveOrders(function () {
            if (!IsNull(params_) && params_.alwaysToOrders || _activeOrders && _activeOrders.length) {

                uiRemoveAllButLastAfterFirstShow();
                activeOrdersPage(params_);

                /*
                 uiBack(-1, function () {
                 activeOrdersPage(flipDirection_);
                 });*/
            }
            else {

                uiRemoveAllButLastAfterFirstShow();
                newOrder();

                /*
                 uiBack(-1, function () {
                 newOrder();
                 });*/
            }
        });
    }

    function newOrder() {

        if (globalParams.CreateOrderMode == "Map") {
            showNewOrderControl({
                onFindAddressFromClick: function () {
                    findAddress({
                        caption: TaxiClientAppConsts.fromWhereCaption,
                        allowedModes: "Near|My|History|Map|Address",
                        defaultMode: "Near",
                        onSelect: confirmOrder
                    });
                },
                onNextClick: function (addressFrom_) {
                    confirmOrder(addressFrom_);
                },
                onBackClick: function () {
                    activeOrdersPage({flipDirection: "right"});
                },
                onMenuClick: mainMenu
            });
        }
        else {
            var addressFrom = {
                Kind: "Unknown",
                Image: "question.png",
                Point: null,
                Name: TaxiClientAppConsts.fromWhereCaption
            }
            confirmOrder(addressFrom, {
                left: {
                    backgroundData: "left.png",
                    onClick: function () {
                        uiRemoveAllButLastAfterFirstShow();
                        activeOrdersPage({flipDirection: "right"});
                    }
                },
                right: btMenu
            }); // туда надо передать: если нажали назад - вызвать activeOrdersPage!
        }

    }

    function findAddress(params_) {
        log("findAddressFrom");

        showAddressFinderList({
            allowedModes: params_.allowedModes,
            defaultMode: params_.defaultMode,
            showAddressComments: params_.showAddressComments,
            caption: params_.caption,
            value: params_.value,
            onSelect: params_.onSelect,
            buttons: btBackMenu,
            onCallClick: phoneCall
        });
    }

    function phoneCall(params_) {
        log("phoneCall");

        var flipDirection = "left";
        if (!IsNull(params_) && !IsNullOrEmpty(params_.flipDirection))
            flipDirection = params_.flipDirection;

        showSelectableList({
            caption: TaxiClientAppConsts.callCaption,
            flipDirection: flipDirection,
            items: [
                {
                    ID: "Phone",
                    Name: TaxiClientAppConsts.phoneWithColonCaption + globalParams.TaxiStationPhoneDisplay,
                    Image: "info.png",
                    Clickable: false
                },
                {
                    ID: "MakeCall",
                    Name: TaxiClientAppConsts.callOperatorCaption,
                    Image: "MakeCall.png"
                },
                {
                    ID: "CallBack",
                    Name: TaxiClientAppConsts.orderOperatorCallCaption,
                    Image: "CallBack.png"
                },
            ],
            onSelect: function (item_) {
                if (item_.ID == "MakeCall") {

                    try {
                        cxDial(globalParams.TaxiStationPhone);
                    }
                    catch (e) {

                    }

                }
                else if (item_.ID == "CallBack") {
                    cxAjax({
                        url: getDataURLPrefix() + "/data/GlobalAction?ActionName=TaxiClient_RequestCallBack",
                        onSuccess: function (result) {
                            var parsed = $.parseJSON(result);

                            showMessage({
                                message: translatParsedMessage(parsed.Message),
                                onButtonClick: function () {
                                    uiBack(2);
                                }
                            });

                        },
                        onError: function () {
                            tryLater(2);
                        }
                    });
                }
            },
            buttons: {
                left: btBack
            }
        });
    }

    function callDriver(order_) {
        log("callDriver");

        var canCallDriver = order_.State.CanCallDriver && !IsNull(order_.driverInfo) && !IsNull(order_.driverInfo.Phone);
        var canCallBackDriver = order_.State.CanRequestDriverCallBack;

        if (!canCallDriver && !canCallBackDriver)
            return;

        var items = new Array();

        if (canCallDriver) {
            items.push({
                ID: "Phone",
                Name: TaxiClientAppConsts.driverPhoneWithColonCaption + order_.driverInfo.Phone,
                Image: "info.png",
                Clickable: false
            });
            items.push({
                ID: "MakeCall",
                Name: TaxiClientAppConsts.callDriverCaption,
                Image: "makecall.png"
            });
        }

        if (canCallBackDriver) {
            items.push({
                ID: "CallBack",
                Name: TaxiClientAppConsts.orderDriverCallCaption,
                Image: "callback.png"
            });
        }

        showSelectableList({
            caption: TaxiClientAppConsts.callCaption,
            items: items,
            onSelect: function (item_) {

                if (item_.ID == "MakeCall") {

                    try {
                        cxDial(order_.driverInfo.Phone);
                    }
                    catch (e) {

                    }

                }
                else if (item_.ID == "CallBack") {
                    cxAjax({
                        url: getDataURLPrefix() + "/data/GlobalAction?ActionName=TaxiClient_RequestDriverCallBack",
                        params: {ID: order_.ID},
                        onSuccess: function (result) {
                            var parsed = $.parseJSON(result);

                            showMessage({
                                message: translatParsedMessage(parsed.Message),
                                onButtonClick: function () {
                                    uiBack(2);
                                }
                            });

                        },
                        onError: function () {
                            tryLater(2);
                        }
                    });
                }
            },
            buttons: {
                left: btBack
            }
        });
    }

    function confirmOrder(addressFrom_, buttons_) {
        log("confirmOrder");

        var currentOrder = {};
        currentOrder.addressFrom = addressFrom_;
        currentOrder.addressTo = new Array();
        currentOrder.addressTo.push({
            Kind: "Custom",
            Image: "Custom",
            Point: null,
            Name: TaxiClientAppConsts.toWhereCaption,
            Description: TaxiClientAppConsts.notNecessarilyCaption,
        });
        currentOrder.time = {
            Kind: "Now",
            Name: TaxiClientAppConsts.nowCaption
        };
        currentOrder.tariff = {
            ID: globalParams.DefaultTariffID,
            Name: globalParams.DefaultTariffName,
            Description: globalParams.DefaultTariffDescription
        };
        currentOrder.options = new Array();
        currentOrder.payment = {
            ID: globalParams.DefaultPaymentID,
            Name: globalParams.DefaultPaymentName,
            Description: globalParams.DefaultPaymentDescription
        };
        currentOrder.phone = userParams.DefaultPhone;
        currentOrder.displayPhone = userParams.DefaultPhoneDisplay;
        if (IsNull(currentOrder.displayPhone))
            currentOrder.displayPhone = currentOrder.phone;
        currentOrder.notes = "";

        var buttons = buttons_;
        if (IsNull(buttons))
            buttons = btBackMenu;

        getCompanyPromo(function (company) {
            showOrderConfirmation({
                caption: TaxiClientAppConsts.newOrderCaption,
                order: currentOrder,
                onEdit: editOrder,
                onContinue: createOrder,
                buttons: buttons,
                onCallClick: phoneCall,
                company: company,
            });

        });
    }

    function copyOrder(order_, toggle_) {
        log("copyOrder");

        order_.time = {
            Kind: "Now",
            Name: TaxiClientAppConsts.nowCaption
        };

        if (toggle_) {
            var from = order_.addressFrom;

            var addrTo = order_.addressTo;
            if (addrTo.length > 0)
                addrTo = addrTo[addrTo.length - 1];

            order_.addressFrom = addrTo;
            order_.addressTo = new Array();
            order_.addressTo.push(from);
        }


        getCompanyPromo(function (company) {
            showOrderConfirmation({
                caption: TaxiClientAppConsts.newOrderCaption,
                order: order_,
                onEdit: editOrder,
                onContinue: createOrder,
                buttons: btBackMenu,
                company: company
            });
        });
    }

    function createOrder(order_) {
        // order_.notes += "b;";
        cxAjax({
            url: getDataURLPrefix() + "/data/GlobalAction?ActionName=TaxiClient_CreateOrder",
            params: order_,
            onSuccess: function (result) {

                var r = $.parseJSON(result);
                if (!IsNull(r) && r.Result == "ERR") {
                    var message = r.Message;
                    if (IsNull(message))
                        message = ControlsConsts.errMessage;

                    showMessage({
                        message: translatParsedMessage(message),
                        onButtonClick: function () {
                            uiBack();
                        }
                    });
                    return;
                }

                mainPage({alwaysToOrders: true, flipDirection: "right", corporative: order_.corporative});
            },
            onError: function () {
                showMessage({
                    message: TaxiClientAppConsts.serviceTemporaryUnavailableMessage,
                    onButtonClick: function () {
                        uiBack();
                    }
                });
            }
        });
    }

    function editOrder(editKind_, order_, callBack_, toIndex_) {

        function apply(count_) {
            if (callBack_)
                callBack_();
            uiBack(count_);
        }

        if (editKind_ == "addressFrom") {
            findAddress({
                caption: TaxiClientAppConsts.fromWhereCaption,
                value: order_.addressFrom,
                allowedModes: "Near|Address|My|History|Map",
                defaultMode: "Near",
                onSelect: function (address_) {
                    order_.addressFrom = address_;
                    apply();
                }
            });
        }
        else if (editKind_ == "addressTo") {

            if (toIndex_ >= 0 && toIndex_ < order_.addressTo.length) {
                findAddress({
                    caption: TaxiClientAppConsts.toWhereCaption,
                    showAddressComments: false,
                    value: $.extend({}, order_.addressTo[toIndex_]),
                    allowedModes: "Address|My|History|Map|Custom",
                    defaultMode: "Address",
                    onSelect: function (address_) {
                        order_.addressTo[toIndex_] = address_;
                        apply();
                    }
                });
            }
        }
        else if (editKind_ == "time") {
            showTimeSelector({
                caption: TaxiClientAppConsts.whenCaption,
                initialValue: order_.time,
                onContinue: function (time_) {
                    order_.time = time_;
                    apply();
                },
                buttons: btBackMenu
            });
        }
        else if (editKind_ == "tariff") {
            showSelectableList({
                caption: TaxiClientAppConsts.tariffCaption,
                items: globalParams.Tariffs,
                onSelect: function (tariff_) {
                    order_.tariff = $.extend({}, tariff_);
                    // РАНЬШЕ НЕ БЫЛО
                    cxAjax({
                        url: getDataURLPrefix() + "/data/GlobalAction?ActionName=TaxiClient_GetOptions",
                        params: {
                            IDService: tariff_.ID
                        },
                        onSuccess: function (result) {
                            var r = $.parseJSON(result);
                            globalParams.Options = r;
                        },
                        onError: function (result) {
                            log('error');
                            log(getDataURLPrefix());
                        }
                    });
                    //
                    apply();

                },
                buttons: btBackMenu
            });
        }
        else if (editKind_ == "notes") {

            var phone = order_.displayPhone;
            if (IsNullOrEmpty(phone))
                phone = order_.phone;

            var simpleItems = [{
                Name: translatParsedMessage(order_.payment.Name),
                Description: translatParsedMessage(order_.payment.Description),
                Image: "payment.png",
                onSelect: function (listItem_, fillItems_) {
                    showSelectableList({
                        caption: TaxiClientAppConsts.paymentCaption,
                        items: globalParams.Payments,
                        onSelect: function (payment_) {
                            order_.payment = $.extend({}, payment_);
                            apply();
                            listItem_.Name = order_.payment.Name;
                            listItem_.Description = order_.payment.Description;
                            fillItems_(simpleItems);
                        },
                        buttons: btBackMenu
                    });
                },
            },
                {
                    Name: phone,
                    Description: TaxiClientAppConsts.phoneToConnectWithYou,
                    Image: "makecall.png",
                    onSelect: function (listItem_, fillItems_) {
                        showInputDigitsControl({
                            transparent: true,
                            prompt: TaxiClientAppConsts.phoneToConnect,
                            placeholder: globalParams.PhonePlaceholder,
                            onClick: function (phone_, displayPhone_) {
                                order_.phone = phone_;
                                order_.displayPhone = displayPhone_;
                                apply();
                                listItem_.Name = displayPhone_;
                                fillItems_(simpleItems);
                            },
                            buttons: btBackMenu
                        });
                    }
                }];

            showSuperList({
                memo: true,
                caption: TaxiClientAppConsts.notesCaption,
                placeholder: TaxiClientAppConsts.notesCaption,
                value: order_.notes,
                buttons: btBackMenu,
                items: simpleItems,
                multiItems: globalParams.Options,
                multiItemsSelected: order_.options,
                onContinue: function (options_, notes_) {
                    order_.options = options_;
                    order_.notes = notes_;
                    apply();
                    // РАНЬШЕ НЕ БЫЛО
                    cxAjax({
                        url: getDataURLPrefix() + "/data/GlobalAction?ActionName=TaxiClient_PreCalculateOrder",
                        params: order_,
                        background: true,
                        onSuccess: function (result) {
                            var r = $.parseJSON(result);
                            if (!IsNull(r) && !IsNullOrEmpty(r.Name)) {
                                var htmlCost = getNameDesctiptionHtml(r);
                                log(htmlCost);
                                divItemCostText.html(htmlCost);
                                divItemCost.show();
                            }
                        }
                    });
                    //
                },
            });
            /*    function getVisibilityMargins() {
             var selected = [];
             if (globalParams.Options) {
             $.each(globalParams.Options, function () {
             if (IsNull(this.Visibility) || this.Visibility == "Client" || this.Visibility == "Anywhere") {
             selected.push(this);
             }
             });
             }
             return selected;
             }*/
            /*
             {
             Name: htmlOptions,
             Image: "options.png",
             onSelect: function () {
             showMultiSelectableList({
             caption: "Пожелания",
             items: globalParams.Options,
             selected: order_.options,
             onContinue: function (options_) {
             order_.options = options_;
             order_.notes = notes_;
             apply();
             },
             buttons: btBackMenu
             });
             },
             onHold: function () {
             showConfirmation({
             caption: "Подтверждение", message: "Очистить список пожеланий?",
             onButtonYesClick: function () {
             order_.options = new Array();
             if (callBack_) {
             callBack_();
             }
             uiBack();
             },
             onButtonNoClick: function () {
             uiBack();
             }
             });

             apply();
             },
             }
             /*
             var htmlOptions = "";
             if (order_.options) {
             $.each(order_.options, function () {
             htmlOptions += this.Name + ", ";
             });
             }
             if (htmlOptions.length > 2)
             htmlOptions = htmlOptions.substr(0, htmlOptions.length - 2);

             if (htmlOptions.length == 0)
             htmlOptions = "Пожелания";

             /*
             showMemoEditor({
             caption: "Примечания",
             placeholder: "Примечания",
             value: order_.notes,
             onContinue: function (notes_) {
             order_.notes = notes_;
             apply();
             },
             buttons: btBackMenu,
             items: [{
             Name: htmlOptions,
             Image: "options.png",
             onSelect: function () {
             showMultiSelectableList({
             caption: "Пожелания",
             items: globalParams.Options,
             selected: order_.options,
             onContinue: function (options_) {
             order_.options = options_;
             apply();
             },
             buttons: btBackMenu
             });
             },
             onHold: function () {
             showConfirmation({
             caption: "Подтверждение", message: "Очистить список пожеланий?",
             onButtonYesClick: function () {
             order_.options = new Array();
             if (callBack_) {
             callBack_();
             }
             uiBack();
             },
             onButtonNoClick: function () {
             uiBack();
             }
             });

             apply();
             },
             },
             {
             Name: translatParsedMessage(order_.payment.Name),
             Description: translatParsedMessage(order_.payment.Description),
             Image: "payment.png",
             onSelect: function () {
             showSelectableList({
             caption: "Оплата",
             items: globalParams.Payments,
             onSelect: function (payment_) {
             order_.payment = $.extend({}, payment_);
             apply();
             },
             buttons: btBackMenu
             });
             },
             },
             {
             Name: translatParsedMessage(order_.phone),
             Description: "Телефон для связи",
             Image: "makecall.png",
             onSelect: function () {
             showInputDigitsControl({
             transparent: true,
             prompt: "Телефон для связи",
             placeholder: globalParams.PhonePlaceholder,
             onClick: function (phone_, displayPhone_) {
             order_.phone = displayPhone_;
             apply(2);
             },
             buttons: btBackMenu
             });
             }
             }]
             });
             */
        }
    }

    var _activeOrders = null;
    var _activeOrdersTimer = null;
    var _activeOrdersCallBack = null;
    var _activeOrdersCallBack2 = null;

    function enableActiveOrdersTimer() {
        if (_activeOrdersTimer == null) {
            log("enableActiveOrdersTimer");
            _activeOrdersTimer = setInterval(function () {
                updateActiveOrders(function () {
                    if (IsNull(_activeOrders) || _activeOrders.length == 0)
                        disableActiveOrdersTimer();
                });
            }, 5000);
        }
    }

    function disableActiveOrdersTimer() {
        if (_activeOrdersTimer != null) {
            log("disableActiveOrdersTimer");
            clearInterval(_activeOrdersTimer);
            _activeOrdersTimer = null;
        }
    }

    function checkActiveOrdersTimer() {
        if (_activeOrdersTimer == null) {
            enableActiveOrdersTimer();
        }
    }

    var sessionID = generateUUID();

    function updateMyPosition() {

    }

    function updateActiveOrders(callback_) {
        geoLocate();

        cxAjax({
            url: getDataURLPrefix() + "/data/GlobalAction?ActionName=TaxiClient_GetCurrentOrders",
            params: {
                SessionID: sessionID,
                Lat: mapStartCenter[0],
                Lon: mapStartCenter[1],
            },
            onSuccess: function (result, status, xhr) {

                if (xhr.status == 304)
                    return;

                _activeOrders = $.parseJSON(result);


                if (_activeOrdersCallBack)
                    _activeOrdersCallBack(getActiveOrdersData());
                if (_activeOrdersCallBack2)
                    _activeOrdersCallBack2(getActiveOrdersData());
            },
            onComplete: callback_,
            background: true
        });
    }

    function prepareOrders(data_) {
        var items = new Array();

        if (data_ && data_.length) {
            $.each(data_, function () {

                var addressTo = "";
                $.each(this.addressTo, function () {
                    var addrName = this.Name;
                    if (addrName == TaxiClientAppConsts_ru.toWhereCaption) {
                        addrName = TaxiClientAppConsts.sayToDriver;
                    }
                    if (IsNullOrEmpty(addressTo))
                        addressTo = addrName;
                    else
                        addressTo += " — " + addrName;
                });
                var name = this.State.Name;
                var descr = this.time.Name + ": " + this.addressFrom.Name + " — " + addressTo;

                if (!IsNull(this.approxTime)) {
                    name = TaxiClientAppConsts.comingPredictionDriver + this.approxTime.Name.toLowerCase();
                }

                items.push({
                    Kind: "CurrentOrder",
                    Name: translatParsedMessage(name),
                    Description: descr,
                    Image: this.State.Value + ".png",
                    Data: this
                });
            });
        }

        return items;
    }

    function prepareArchiveOrders(data_) {
        var items = new Array();

        if (data_ && data_.length) {
            $.each(data_, function () {

                var addressTo = "";
                $.each(this.addressTo, function () {
                    var addrName = this.Name;
                    if (addrName == TaxiClientAppConsts_ru.toWhereCaption) {
                        addrName = TaxiClientAppConsts.sayToDriver;
                    }
                    if (IsNullOrEmpty(addressTo))
                        addressTo = addrName;
                    else
                        addressTo += " — " + addrName;
                });

                var name = this.addressFrom.Name + " — " + addressTo;
                var descr = this.time.Name;

                if (this.State.Value == "Canceled")
                    descr += " (" + this.State.Name + ")";

                /*
                 var name = this.time.Name;

                 if (this.State.Value = "Canceled")
                 name += " (" + this.State.Name + ")";

                 var descr = this.addressFrom.Name + " — " + addressTo;
                 */
                items.push({
                    Kind: "CurrentOrder",
                    Name: translatParsedMessage(name),
                    Description: translatParsedMessage(descr),
                    Image: this.State.Value.toLowerCase() + ".png",
                    Data: this
                });
            });
        }

        return items;
    }


    function getActiveOrdersData() {
        var items = prepareOrders(_activeOrders);

        //items.splice(0, 0, {
        items.push({
            Style: "Buttons",
            Buttons: [{
                Kind: "CreateOrder",
                Name: TaxiClientAppConsts.newOrderCaption,
                Image: "add.png",
                Style: "Big"
            }]
        });

        return items;
    }

    function activeOrdersPage(params_) {
        updateActiveOrders(function () {
            checkActiveOrdersTimer();

            var items = getActiveOrdersData();

            log(items);

            var flipDirection = "left";
            if (!IsNull(params_) && !IsNullOrEmpty(params_.flipDirection))
                flipDirection = params_.flipDirection;

            var list = new showSelectableList({
                flipDirection: flipDirection,
                caption: TaxiClientAppConsts.myOrders,
                items: items,
                onSelect: function (item_) {
                    if (item_.Kind == "CreateOrder") {
                        newOrder();
                    }
                    else {
                        showOrder(item_);
                    }
                },
                buttons: btPhoneMenu
            });
            _activeOrdersCallBack = list.loadData;
        });
    }

    function mainMenu() {
        var items = new Array();
        items.push({
            Kind: "TaxiStationPhone",
            Name: TaxiClientAppConsts.connectWithOperator,
            Description: TaxiClientAppConsts.gladToHelpYou,
            Image: "MakeCall.png",
        });
        /*
         items.push({
         Kind: "MyPhone",
         Name: "Мой телефон: " + userParams.DefaultPhoneDisplay,
         Image: "mobile.png",
         Clickable: false
         });*/
        var name = {
            Kind: "MyName",
            Name: userParams.ClientName,
            Description: TaxiClientAppConsts.myNameCaption,
            Image: "user.png",
        };
        items.push(name);
        items.push({
            Kind: "MyAddresses",
            Name: TaxiClientAppConsts.myAddressesCaption,
            Description: TaxiClientAppConsts.loveAddressesForOften,
            Image: "my.png",
        });
        items.push({
            Kind: "MyHistory",
            Name: TaxiClientAppConsts.ordersHistory,
            Description: TaxiClientAppConsts.repeatRememberRate,
            Image: "Created.png",
        });
        items.push({
            Kind: "Language",
            Name: TaxiClientAppConsts.Languages_switch,
            Image: "globe-button.png",
        });

        var list;
        getCompanyPromo(function (company) {
            var itemParams_ = {};
            if(company == null){
                itemParams_ = {
                    Kind: "AddCompany",
                    Name: TaxiClientAppConsts.addCaption,
                    Description: TaxiClientAppConsts.addCompanyDescription,
                    Image: "caption.png"
                };
            }else{
                itemParams_ =  {
                    Kind: "EditCompany",
                    Name: company.name,
                    Description: TaxiClientAppConsts.addCompanyDescription,
                    Image: "caption.png"
                };
            }
            items.splice(5, 0, itemParams_);
            list.loadData(items);
        });
        items.push({
            Kind: "Logout",
            Name: TaxiClientAppConsts.exitCaption,
            Description: TaxiClientAppConsts.forgetMyNumber,
            Image: "Logout.png",
        });

        if (typeof appTaxiCatalog !== "undefined") {
            items.push({
                Kind: "GoToCatalog",
                Name: TaxiClientAppConsts.catalogCaption,
                Description: TaxiClientAppConsts.goToAnotherTaxiService,
                Image: "Catalog.png",
            });
        }

        items.push({
            Kind: "About",
            Name: TaxiClientAppConsts.aboutProgram,
            Image: "infinity.png",
        });

        list = new showSelectableList({
            caption: TaxiClientAppConsts.menuCaption,
            items: items,
            flipDirection: "up",
            onSelect: function (item_) {
                if (item_.Kind == "TaxiStationPhone") {
                    phoneCall();
                }
                else if (item_.Kind == "MyName") {
                    showMemoEditor({
                        caption: TaxiClientAppConsts.myNameCaption,
                        placeholder: TaxiClientAppConsts.myNameCaption,
                        value: userParams.ClientName,
                        onContinue: function (name_) {
                            if (name_ != userParams.ClientName) {
                                cxAjax({
                                    url: getDataURLPrefix() + "/data/GlobalAction?ActionName=TaxiClient_SetUserName",
                                    params: {Name: name_},
                                    onSuccess: function () {
                                        userParams.ClientName = name_;
                                        name.Name = userParams.ClientName;
                                        if (list && list.loadData)
                                            list.loadData(items);
                                    },
                                    onComplete: function () {
                                        uiBack();
                                    }
                                });
                            }
                            else
                                uiBack();
                        },
                        buttons: btBackOnly
                    });
                }
                else if (item_.Kind == "MyHistory") {
                    showMyHistory();
                }
                else if (item_.Kind == "MyHistoryByCorporation") {
                    showMyHistoryByCorporation();
                }
                else if (item_.Kind == "MyAddresses") {
                    showMyAddresses();
                }
                else if (item_.Kind == "Logout") {
                    logout();
                }
                else if (item_.Kind == "GoToCatalog") {
                    gotoTaxiCatalog();
                }
                else if (item_.Kind == "Language") {
                    showLanguageSwitch()
                }
                else if (item_.Kind == "AddCompany" || item_.Kind == "EditCompany") {
                    getCompanyPromo(function (company) {
                        if (company) {
                            editCompanyPage({
                                caption: TaxiClientAppConsts.addCaption,
                                message: "",
                                buttons: {
                                    left: {
                                        backgroundData: "left.png",
                                        onClick: function () {
                                            leftFirstAndLast();
                                        }
                                    }
                                },
                                company: company
                            });
                        }else{
                                addCompanyPage({
                                caption: TaxiClientAppConsts.corporativePackets,
                                message: translatParsedMessage(TaxiClientAppConsts.packetDescription),
                                buttons: {
                                    left: btBack,
                                    right: btPhoneCall
                                }
                            });
                        }
                    });
                }
                else if (item_.Kind == "About") {
                    showAbout();
                }
            },
            onHold: function (item_) {
                if (item_.Kind == "MyAddresses") {
                    showConfirmation({
                        caption: TaxiClientAppConsts.confirmationCaption,
                        message: TaxiClientAppConsts.deleteAllAddressesMessage,
                        onButtonYesClick: function () {
                            cxAjax({
                                url: getDataURLPrefix() + "/data/GlobalAction?ActionName=TaxiClient_ClearMyAddresses",
                                onComplete: function () {
                                    uiBack();
                                }
                            });
                        },
                        onButtonNoClick: function () {
                            uiBack();
                        }
                    });
                }
                else if (item_.Kind == "MyHistory") {
                    showConfirmation({
                        caption: TaxiClientAppConsts.confirmationCaption,
                        message: TaxiClientAppConsts.clearOrderHistoryMessage,
                        onButtonYesClick: function () {
                            cxAjax({
                                url: getDataURLPrefix() + "/data/GlobalAction?ActionName=TaxiClient_ClearArchiveOrders",
                                onComplete: function () {
                                    uiBack();
                                }
                            });
                        },
                        onButtonNoClick: function () {
                            uiBack();
                        }
                    });
                }
            },
            buttons: btBackOnly
        });
    }

    function showLanguageSwitch() {
        var items = new Array();
        for (var i = 0; i < TaxiClientAppConsts.Languages.length; i++) {
            var _lang = TaxiClientAppConsts.Languages[i]
            items.push({
                Kind: _lang.Kind,
                Name: _lang.Name,
                Image: _lang.Kind + ".png"
            });
        }

        var list = new showSelectableList({
                caption: TaxiClientAppConsts.LanguagesCaption,
                items: items,
                flipDirection: "up",
                buttons: btBackOnly,
                onSelect: function (item_) {


                    _selectedLanguage = item_.Kind;

                    showConfirmation({
                        caption: TaxiClientAppConsts.LanguageSwitchConfirmationCaption,
                        message: TaxiClientAppConsts.LanguageSwitchConfirmationMessage,
                        onButtonYesClick: function () {

                            localStorage.setItem('currentLanguage', _selectedLanguage);
                            window.location.reload();
                        },
                        onButtonNoClick: function () {
                            uiBack();
                        }
                    });

                }
            }
        )
    }
    function success(phonenumber) {
        console.log("______)()()()()()()");
        console.log("My number is " + phonenumber);
        localStorage.setItem("phoneNumber",phonenumber);
    }
    function failed(asd) {
        console.log("______)()()()()()()");
        console.log(asd);
    }
    function editCompanyPage(params_) {
        var items = new Array();


        var divWrapper = cxCreateDiv({className: "scrollableControl"});
        createCaption(divWrapper, params_.caption);
        var divListBody = cxCreateDiv({className: "infoBody", parent: divWrapper});
        var companyInfo = TaxiClientAppConsts.corporationDescription;

        var divListBodyInfo = cxCreateDiv({
            className: "smallBodyText listItemPadding",
            html: companyInfo
        });
        items.push({
            Kind: "Text",
            Name: divListBodyInfo,
            Clickable: false
        });
        items.push({
            Kind: "showMyHistoryByCorporation",
            Name: TaxiClientAppConsts.ordersHistory,
            Description: TaxiClientAppConsts.repeatRememberRate,
            Image: "burning.png"
        });
        items.push({
            Kind: "showDeleteButton",
            Name: TaxiClientAppConsts.removeCompany,
            Style: "Buttons",
            Buttons: [{
                Kind: "removeCompany",
                Name: TaxiClientAppConsts.removeCompany,
                Image: "canceled.png",
                Style: "Big"
            }]
        });

        var list = new showSelectableList({
                caption: params_.caption,
                items: items,
                flipDirection: "up",
                buttons: params_.buttons,
                onSelect: function (item_) {
                    if (item_.Kind == "showMyHistoryByCorporation") {
                        showMyHistoryByCorporation(params_.company);
                    }
                    if (item_.Kind == "removeCompany") {
                        navigator.notification.confirm(
                            translatParsedMessage(TaxiClientAppConsts.promoDelete),
                            removeCompany,
                            "Taxoline",
                            [TaxiClientAppConsts.yes,TaxiClientAppConsts.no])
                    }
                }
            }
        )
    }


    function removeCompany(confirmed) {
        if(confirmed !== 1){
            return;
        }
        getCompanyPromo(function (company) {
            if(company != null){
                promoAjax({
                    url: "/promo/promo/remove",
                    data: {
                        corporationId: company.id
                    },
                    success: function (res) {
                        leftFirstAndLast();
                    },
                    error: function (res) {
                        console.log(res);
                    }
                });
            }else{
                mainMenu();
                stackPop(2);
            }
        });
    }

    function showPromoAddPage(params_) {
        log("showInfoPage: " + params_.caption);
        var items = [];
        // var divListBody = cxCreateDiv({className: "infoBody"});
            var divListBodyInfo = cxCreateDiv({
                //parent: divListBody,
                className: "smallBodyText",
                html: params_.message
            });
            items.push({
                Kind: "Text",
                Name: divListBodyInfo,
                Clickable: false

            });

            // var inputHolder = cxCreateDiv({parent: divListBody});

            var teInput = cxCreateInput({/*parent: inputHolder*/});
            teInput.val(params_.value);
            teInput.attr("placeholder", TaxiClientAppConsts.enterPromo);
            teInput.attr("rows", "1");
            items.push({
                Kind: "Input",
                Name: teInput,
                Image: "caption.png"
            });
            items.push({
                Kind: "addButton",
                Name: TaxiClientAppConsts.addCaption,
                Style: "Buttons",
                Buttons: [{
                    Kind: "addButton",
                    Image: "apply.png",
                    Name: TaxiClientAppConsts.addCompany,
                    Style: "Big"
                }]
            });

            var list = new showSelectableList({
                caption: TaxiClientAppConsts.addCaption,
                items: items,
                flipDirection: "up",
                buttons: btBackOnly,
                onSelect: function (item_) {

                    if (item_.Kind == "showMyHistoryByCorporation") {
                        showMyHistoryByCorporation();
                    }
                    if (item_.Kind == "addButton") {

                        var data = {
                            promoText: teInput.val(),
                        };

                        if(data.promoText.length == 0){
                            somethingWentWrong({
                                caption: "Taxoline",
                                message: translatParsedMessage(TaxiClientAppConsts.wrongPromo),
                                buttons: {
                                    left: btBack,
                                    right: btPhoneMenu
                                }
                            });
                            return;
                        }

                        promoAjax({
                            url: "/promo/promo/add",
                            data: data,
                            onlyOneTimeRequest: true,
                            success: function (res) {
                                navigator.notification.alert(
                                    translatParsedMessage(TaxiClientAppConsts.promoSuccess),
                                    params_.onButtonClick,
                                    translatParsedMessage("Taxoline"),
                                    TaxiClientAppConsts.ok
                                );
                                editCompanyPage({
                                    company: res.content,
                                    caption: TaxiClientAppConsts.addCaption,
                                    message: "",
                                    buttons: {
                                        left: {
                                            backgroundData: "left.png",
                                            onClick: function () {
                                                leftFirstAndLast();
                                            }
                                        },
                                        right: btPhoneMenu
                                    }
                                });
                            },
                            error: function (res) {
                                somethingWentWrong({
                                    caption: "Taxoline",
                                    message: translatParsedMessage( res.status == 5002 ? TaxiClientAppConsts.policyPromo : TaxiClientAppConsts.wrongPromo),
                                    buttons: {
                                        left: btBack,
                                        right: btPhoneMenu
                                    }
                                });
                            }
                        });
                    }
                }
            })
    }
    // add company page
    function addCompanyPage(params_) {
        log("showInfoPage: " + params_.caption);
        var divWrapper = cxCreateDiv({className: "scrollableControl"});
        createCaption(divWrapper, params_)

        var divListBody = cxCreateDiv({className: "infoBody listItem", parent: divWrapper});
        var divListBodyInfo = cxCreateDiv({
            parent: divListBody,
            className: "smallBodyText listItemPadding",
            html: params_.message
        });

        var dvListBodyButtonSave = createButton(divListBody, {
            caption: "" +
            "<img width='20' height='20' style='padding: 10px' src='Images/Applications/add.png'>" + TaxiClientAppConsts.addCompany,
            backgroundSize: '1em', className: "save_company selectorButtonBig", onClick: function () {
                showPromoAddPage({
                    caption: TaxiClientAppConsts.addCaption,
                    message: TaxiClientAppConsts.packetAddInfo
                });
            }
        });

        uiShow(divWrapper);
    }

    function logout() {
        showConfirmation({
            caption: TaxiClientAppConsts.confirmationCaption,
            message: TaxiClientAppConsts.exitAndForgetNumberMessage,
            onButtonYesClick: function () {
                cxAjax({
                    url: getDataURLPrefix() + "/data/GlobalAction?ActionName=TaxiClient_Logout",
                    onComplete: function () {

                        uiRemoveAllButLastAfterFirstShow();
                        that.startApplication({flipDirection: "right"});

                        //uiBack(-1, that.startApplication);
                    }
                });
            },
            onButtonNoClick: function () {
                uiBack();
            }
        });
    }

    function showAboutDebug() {
        showInfoPage({
            caption: TaxiClientAppConsts.aboutProgram,
            message: translatParsedMessage("ABOUT"),
            buttons: {
                left: btBack,
                right: {
                    backgroundData: "settings.png",
                    onClick: showDebug
                }
            }
        });
    }

    function showAbout() {
        showInfoPage({
            caption: TaxiClientAppConsts.aboutProgram,
            message: translatParsedMessage("ABOUT"),
            buttons: {
                left: btBack,
                right: {
                    backgroundData: "settings.png",
                    onClick: showDebug
                }
            }
        });
    }

    function showMyHistory() {
        cxAjax({
            url: getDataURLPrefix() + "/data/GlobalAction?ActionName=TaxiClient_GetArchiveOrders",
            onSuccess: function (result) {
                var parsed = $.parseJSON(result);

                if (parsed) {
                    showSelectableList({
                        caption: TaxiClientAppConsts.ordersHistory,
                        items: prepareArchiveOrders(parsed),
                        onSelect: function (order_) {
                            showOrder(order_);
                        },
                        onHold: function (item_, fillItems_, element_) {
                            showConfirmation({
                                caption: TaxiClientAppConsts.confirmationCaption,
                                message: TaxiClientAppConsts.deleteOrderFromHistoryQuestion,
                                onButtonYesClick: function () {
                                    cxAjax({
                                        url: getDataURLPrefix() + "/data/GlobalAction?ActionName=TaxiClient_DeleteArchiveOrder",
                                        params: item_,
                                        onComplete: function () {
                                            element_.remove();
                                            uiBack();
                                        }
                                    });
                                },
                                onButtonNoClick: function () {
                                    uiBack();
                                }
                            });
                        },
                        buttons: btBackOnly
                    });
                }
                else {
                    showMessage({
                        caption: TaxiClientAppConsts.errorCaption,
                        message: TaxiClientAppConsts.CityListEmpty,
                        onButtonClick: function () {
                            uiBack();
                        }
                    });
                }
            },
            onError: tryLater
        });
    }

    function filerOrdersFromCurrentCorporation(company, allOrders, _callback) {

        promoAjax({
            url: "/promo/order/list",
            data: {
                corporationId: company.id
            },
            success: function (res) {
                var filtered = [];
                for (var iAll = 0; iAll < allOrders.length; iAll++) {
                    var found = false;
                    for (var iActive = 0; iActive < res.content.active.length; iActive++) {
                        if (allOrders[iAll].ID == res.content.active[iActive].id) {
                            filtered.push(allOrders[iAll]);
                            found = true;
                            break;
                        }
                    }
                    if (found) {
                        break;
                    }
                    for (var iStat = 0; iStat < res.content.stat.length; iStat++) {
                        if (allOrders[iAll].ID == res.content.stat[iStat].id) {
                            filtered.push(allOrders[iAll]);
                            break;
                        }
                    }
                }
                _callback(filtered);
            },
            error: function (res) {
                console.log(res);
            }
        });
    }

    function showMyHistoryByCorporation(company) {
        cxAjax({
            url: getDataURLPrefix() + "/data/GlobalAction?ActionName=TaxiClient_GetArchiveOrders",
            onSuccess: function (result) {
                var parsed = $.parseJSON(result);

                if (parsed) {
                    filerOrdersFromCurrentCorporation(company, parsed, function (filteredOrders) {
                        showSelectableList({
                            caption: TaxiClientAppConsts.ordersHistory,
                            items: prepareArchiveOrders(filteredOrders),
                            onSelect: function (order_) {
                                showOrder(order_);
                            },
                            onHold: function (item_, fillItems_, element_) {
                                showConfirmation({
                                    caption: TaxiClientAppConsts.confirmationCaption,
                                    message: TaxiClientAppConsts.deleteOrderFromHistoryQuestion,
                                    onButtonYesClick: function () {
                                        cxAjax({
                                            url: getDataURLPrefix() + "/data/GlobalAction?ActionName=TaxiClient_DeleteArchiveOrder",
                                            params: item_,
                                            onComplete: function () {
                                                element_.remove();
                                                uiBack();
                                            }
                                        });
                                    },
                                    onButtonNoClick: function () {
                                        uiBack();
                                    }
                                });
                            },
                            buttons: btBackOnly
                        });
                    });
                }
                else {
                    showMessage({
                        caption: TaxiClientAppConsts.errorCaption,
                        message: TaxiClientAppConsts.CityListEmpty,
                        onButtonClick: function () {
                            uiBack();
                        }
                    });
                }
            },
            onError: tryLater
        });
    }

    function showMyAddresses() {
        cxAjax({
            url: getDataURLPrefix() + "/data/GlobalAction?ActionName=TaxiClient_GetMyAddresses",
            onSuccess: function (result) {

                function prepareData(data_) {
                    var parsed = $.parseJSON(data_);

                    //parsed.splice(0, 0, {
                    parsed.push({
                        Style: "Buttons",
                        Buttons: [{
                            Kind: "Add",
                            Name: TaxiClientAppConsts.addNewAddress,
                            Image: "add.png",
                            Style: "Big"
                        }]
                    });
                    return parsed;
                }


                var list = new showSelectableList({
                    caption: TaxiClientAppConsts.myAddressesCaption,
                    items: prepareData(result),
                    onSelect: function (item_) {
                        if (item_.Kind == "Add") {
                            findAddress({
                                caption: TaxiClientAppConsts.newAddress,
                                showMyAddresses: false,
                                allowedModes: "Near|Address|History|Map",
                                defaultMode: "Near",
                                onSelect: function (data_) {

                                    var address = $.extend({}, data_);

                                    address.Description = address.Name;
                                    address.Description = getAddressDescription(address);
                                    //address.Image = "my.png";
                                    //address.Kind = address.RealKind;

                                    cxAjax({
                                        url: getDataURLPrefix() + "/data/GlobalAction?ActionName=TaxiClient_CreateMyAddress",
                                        params: address,
                                        onComplete: function () {
                                            if (list && list.loadData) {
                                                cxAjax({
                                                    url: getDataURLPrefix() + "/data/GlobalAction?ActionName=TaxiClient_GetMyAddresses",
                                                    onSuccess: function (result_) {
                                                        list.loadData(prepareData(result_));
                                                    }
                                                });
                                            }
                                            uiBack();
                                        }
                                    });
                                }
                            });
                        }
                        else {
                            showMyAddress(item_, function () {
                                if (list && list.loadData) {
                                    cxAjax({
                                        url: getDataURLPrefix() + "/data/GlobalAction?ActionName=TaxiClient_GetMyAddresses",
                                        onSuccess: function (result_) {
                                            list.loadData(prepareData(result_));
                                        }
                                    });
                                }
                                uiBack();
                            });
                        }
                    },
                    onHold: function (item_) {
                        showConfirmation({
                            caption: TaxiClientAppConsts.confirmationCaption,
                            message: TaxiClientAppConsts.deleteSelectedAddress + item_.Name + "?",
                            onButtonYesClick: function () {
                                cxAjax({
                                    url: getDataURLPrefix() + "/data/GlobalAction?ActionName=TaxiClient_DeleteMyAddress",
                                    params: item_,
                                    onComplete: function () {
                                        if (list && list.loadData) {
                                            cxAjax({
                                                url: getDataURLPrefix() + "/data/GlobalAction?ActionName=TaxiClient_GetMyAddresses",
                                                onSuccess: function (result_) {
                                                    list.loadData(prepareData(result_));
                                                }
                                            });
                                        }
                                        uiBack();
                                    }
                                });
                            },
                            onButtonNoClick: function () {
                                uiBack();
                            }
                        });
                    },
                    buttons: btBackOnly
                });
            },
            onError: tryLater
        });
    }

    function showMyAddress(address_, callback_) {

        address_.Kind = address_.RealKind;
        address_.rightButton = {
            Image: "info.png",
            onClick: function () {
                showAddressComments({
                    caption: TaxiClientAppConsts.notesCaption,
                    value: address_,
                    onSelect: function (newAddress_) {
                        address_ = newAddress_;

                        address_.Description = address_.Name;
                        address_.Description = getAddressDescription(address_);
                        //address_.Image = "my.png";
                        //address_.Kind = address_.RealKind;

                        if (apply)
                            apply(address_);
                        uiBack();
                    }
                });
            },
        };

        apply = showEditRemoveDialog({
            caption: TaxiClientAppConsts.myAddressCaption,
            placeholder: TaxiClientAppConsts.addressName,
            deleteConfirmationText: TaxiClientAppConsts.deleteSelectedAddress + address_.Name + "?",
            item: address_,
            onClick: function () {
                findAddress({
                    caption: address_.Name,
                    value: address_,
                    allowedModes: "Near|Address|History|Map",
                    defaultMode: "Near",
                    onSelect: function (newAddress_) {
                        var savedName = address_.Name;
                        var savedID = address_.ID;

                        address_ = newAddress_;

                        address_.Description = address_.Name;
                        address_.Description = getAddressDescription(address_);
                        //address_.Image = "my.png";
                        //address_.Kind = address_.RealKind;

                        address_.Name = savedName;
                        address_.ID = savedID;

                        if (apply)
                            apply(address_);

                        uiBack();
                    }
                });
            },
            onEdit: function (newItem_) {
                cxAjax({
                    url: getDataURLPrefix() + "/data/GlobalAction?ActionName=TaxiClient_UpdateMyAddress",
                    params: newItem_,
                    onComplete: callback_
                });
            },
            onDelete: function () {
                cxAjax({
                    url: getDataURLPrefix() + "/data/GlobalAction?ActionName=TaxiClient_DeleteMyAddress",
                    params: address_,
                    onComplete: callback_
                });
            },
            buttons: btBackOnly
        });
    }

    function addMyAddress(address_) {

        address_.Description = address_.Name;
        address_.Description = getAddressDescription(address_);
        //address_.Image = "my.png";
        //address_.Kind = address_.RealKind;

        address_.Clickable = false;

        apply = showEditRemoveDialog({
            caption: TaxiClientAppConsts.myAddressCaption,
            placeholder: TaxiClientAppConsts.addressName,
            item: address_,
            CanDelete: false,
            onEdit: function (newItem_) {
                cxAjax({
                    url: getDataURLPrefix() + "/data/GlobalAction?ActionName=TaxiClient_CreateMyAddress",
                    params: newItem_,
                    onComplete: function () {
                        uiBack();
                    }
                });
            },
            buttons: btBackOnly
        });
    }

    function showOrderCoords(order_) {
        var timer = null;
        var map = new showMap({
            onBackClick: function () {
                if (timer)
                    clearTimeout(timer);
                uiBack();
            },
            onCallClick: function () {
                phoneCall();
            },
            onShow: function () {
                updatePoints();
            }

        });

        function updatePoints() {
            cxAjax({
                url: getDataURLPrefix() + "/data/GlobalAction?ActionName=TaxiClient_GetCurrentOrderPoints",
                params: {
                    ID: order_.ID
                },
                onSuccess: function (result) {
                    var parsed = $.parseJSON(result);
                    console.log(parsed);
                    map.loadData(parsed);
                },
                background: true
            });
        }

        timer = setInterval(function () {
            updatePoints();
        }, 5000);
    }

    function showOrder(order_) {
        var order = order_.Data;
        //order.State.CanShowMap = true;
        function prepareData() {

            var rbCallDriver = null;
            var driverClickable = false;
            if (!IsNull(order.driverInfo) && (order.State.CanRequestDriverCallBack || order.State.CanCallDriver)) {
                /*rbCallDriver = {
                 Image: "makecall.png",
                 onClick: function () {
                 callDriver(order);
                 }
                 };*/
                driverClickable = true;
            }

            var rbShowMap = null;
            var carClickable = false;
            if (!IsNull(order.State.CanShowMap)) {
                /*rbShowMap = {
                 Image: "running.png",
                 onClick: function () {
                 showOrderCoords(order);
                 }
                 };*/
                carClickable = true;
            }

            var items = new Array();

            var buttons = new Array();

            if (order.State.CanShowMap) {
                buttons.push({
                    Kind: "ShowMap",
                    Image: "running.png",
                    Name: TaxiClientAppConsts.mapCaption
                });
            }

            if (driverClickable) {
                buttons.push({
                    Kind: "Driver",
                    Image: "makecall.png",
                    Name: TaxiClientAppConsts.driverCaption
                });
            }

            if (order.State.CanRate) {
                buttons.push({
                    Kind: "Rate",
                    Image: "star0.png",
                    Name: TaxiClientAppConsts.rateCaption
                });
            }
            if (order.State.CanRepeat) {
                buttons.push({
                    Kind: "Repeat",
                    Image: "repeat.png",
                    Name: TaxiClientAppConsts.repeatCaption,
                });
            }
            if (order.State.CanRepeatToggle) {
                buttons.push({
                    Kind: "RepeatToggle",
                    Image: "repeat_back.png",
                    Name: TaxiClientAppConsts.repeatToggle,
                });
            }
            if (order.State.CanCancel) {
                buttons.push({
                    Kind: "Cancel",
                    Image: "canceled.png",
                    Name: TaxiClientAppConsts.cancelCaption,
                });
            }
            if (buttons.length > 0) {
                var toolbar = {
                    Style: "Buttons",
                    Buttons: buttons
                }
                items.push(toolbar);
            }

            /*
             if (order.State.CanShowMap) {
             items.push({
             Kind: "ShowMap",
             Name: "Показать на карте",
             Style: "Button"
             });
             }
             if (order.State.CanRate) {
             items.push({
             Kind: "Rate",
             Name: "Оставить отзыв",
             Style: "Button"
             });
             }
             if (order.State.CanRepeat) {
             items.push({
             Kind: "Repeat",
             Name: "Повторить заказ",
             Style: "Button",
             Color: "Green"
             });
             }
             if (order.State.CanRepeatToggle) {
             items.push({
             Kind: "RepeatToggle",
             Name: "Поехали обратно",
             Style: "Button",
             Color: "Green"
             });
             }
             if (order.State.CanCancel) {
             items.push({
             Kind: "Cancel",
             Name: "Отменить заказ",
             Style: "Button",
             Color: "Red"
             });
             }
             */

            var stateName = order.State.Name;
            if (!IsNull(order.approxTime)) {
                stateName = TaxiClientAppConsts.comingPredictionDriver + order.approxTime.Name.toLowerCase();
            }


            items.push({
                Kind: "State",
                Name: translatParsedMessage(stateName),
                Image: "created.png",
                Clickable: false
            });

            if (!IsNull(order.carInfo)) {
                items.push({
                    Kind: "Car",
                    Name: translatParsedMessage(order.carInfo.Name),
                    Description: translatParsedMessage(order.carInfo.Description),
                    Image: "assigned.png",
                    Clickable: carClickable,
                    rightButton: rbShowMap
                });
            }
            if (!IsNull(order.driverInfo)) {
                items.push({
                    Kind: "Driver",
                    Name: translatParsedMessage(order.driverInfo.Name),
                    Description: translatParsedMessage(order.driverInfo.Description),
                    Image: "driver.png",
                    Clickable: driverClickable,
                    rightButton: rbCallDriver
                });
            }
            if (!IsNull(order.approxTime)) {
                items.push({
                    Kind: "ApproxTime",
                    Name: translatParsedMessage(order.approxTime.Name),
                    Description: translatParsedMessage(order.approxTime.Description),
                    Image: "assigned.png",
                    Clickable: false
                });
            }
            if (!IsNull(order.finalCost)) {
                items.push({
                    Kind: "Cost",
                    Name: translatParsedMessage(order.finalCost.Name),
                    Description: translatParsedMessage(order.finalCost.Description),
                    Image: "money.png",
                    Clickable: false
                });
            }
            else if (!IsNull(order.approxCost)) {
                items.push({
                    Kind: "Cost",
                    Name: translatParsedMessage(order.approxCost.Name),
                    Description: translatParsedMessage(order.approxCost.Description),
                    Image: "money.png",
                    Clickable: false
                });
            }

            var rbFrom = null;
            if (order.addressFrom.Kind != "Custom" && order.addressFrom.Kind != "My") {
                rbFrom = {
                    Image: "my.png",
                    onClick: function () {
                        var addr = $.extend({}, order.addressFrom);
                        addMyAddress(addr);
                    }
                }
            }

            items.push({
                Kind: "From",
                Name: translatParsedMessage(order.addressFrom.Name),
                Description: getAddressDescription(order.addressFrom),
                Image: "addr_from.png",
                Clickable: false,
                rightButton: rbFrom
            });

            var index = 0;
            $.each(order.addressTo, function () {
                var address = this;
                var rbTo = null;

                if (address.Kind == "Custom") {
                    address.Name = TaxiClientAppConsts.sayToDriver;
                    address.Description = null;
                }

                if (address.Kind != "Custom" && address.Kind != "My") {
                    rbTo = {
                        Image: "my.png",
                        onClick: function () {
                            var addr = $.extend({}, address);
                            addMyAddress(addr);
                        }
                    }
                }
                items.push({
                    Kind: "To",
                    Name: address.Name,
                    Description: getAddressDescription(address),
                    Image: (index == order.addressTo.length - 1) ? "addr_to.png" : "addr_inter.png",
                    Clickable: false,
                    rightButton: rbTo
                });

                index++;
            });

            items.push({
                Kind: "Time",
                Name: translatParsedMessage(order.time.Name),
                Description: translatParsedMessage(order.time.Description),
                Image: "time.png",
                Clickable: false
            });
            items.push({
                Kind: "Tariff",
                Name: translatParsedMessage(order.tariff.Name),
                Description: translatParsedMessage(order.tariff.Description),
                Image: "tariff.png",
                Clickable: false
            });

            var htmlOptions = "";
            if (order.options) {
                $.each(order.options, function () {
                    htmlOptions += this.Name + ", ";
                });
            }
            if (htmlOptions.length > 2)
                htmlOptions = htmlOptions.substr(0, htmlOptions.length - 2);

            if (htmlOptions != "") {
                items.push({
                    Kind: "Options",
                    Name: htmlOptions,
                    Image: "options.png",
                    Clickable: false
                });
            }
            items.push({
                Kind: "Payment",
                Name: translatParsedMessage(order.payment.Name),
                Description: translatParsedMessage(order.payment.Description),
                Image: "payment.png",
                Clickable: false
            });
            if (!IsNullOrEmpty(order.phone)) {
                items.push({
                    Kind: "Phone",
                    Name: translatParsedMessage(order.phone),
                    Image: "makecall.png",
                    Clickable: false
                });
            }
            if (!IsNullOrEmpty(order.notes)) {
                items.push({
                    Kind: "Notes",
                    Name: translatParsedMessage(order.notes),
                    Image: "notes.png",
                    Clickable: false
                });
            }
            return items;
        }

        var list = new showSelectableList({
            caption: TaxiClientAppConsts.orderCaption,
            items: prepareData(),
            onSelect: function (item_) {
                if (item_.Kind == "Repeat") {
                    var newOrder = $.extend(true, {}, order);
                    copyOrder(newOrder);
                }
                if (item_.Kind == "RepeatToggle") {
                    var newOrder = $.extend(true, {}, order);
                    copyOrder(newOrder, true);
                }
                else if (item_.Kind == "Cancel") {
                    showConfirmation({
                        caption: TaxiClientAppConsts.confirmationCaption,
                        message: TaxiClientAppConsts.cancelOrderCaption,
                        onButtonYesClick: function () {
                            cxAjax({
                                url: getDataURLPrefix() + "/data/GlobalAction?ActionName=TaxiClient_CancelOrder",
                                params: {ID: order.ID},
                                onSuccess: function (result) {
                                    updateActiveOrders(function () {
                                        uiBack(2);
                                    });
                                },
                                onError: function () {
                                    showMessage({
                                        message: TaxiClientAppConsts.cannotCancelOrderMessage,
                                        onButtonClick: function () {
                                            uiBack(2);
                                        }
                                    });
                                }
                            });
                        },
                        onButtonNoClick: function () {
                            uiBack();
                        }
                    });
                }
                else if (item_.Kind == "Rate") {
                    showRateForm({
                        caption: TaxiClientAppConsts.rateCaption,
                        buttons: btBackMenu,
                        onContinue: function (rating_, comments_) {
                            cxAjax({
                                url: getDataURLPrefix() + "/data/GlobalAction?ActionName=TaxiClient_RateOrder",
                                params: {ID: order.ID, Rating: rating_.toString(), Comments: comments_},
                                onSuccess: function (result) {
                                    var parsed = $.parseJSON(result);

                                    showMessage({
                                        message: translatParsedMessage(parsed.Message),
                                        onButtonClick: function () {
                                            uiBack(2);
                                        }
                                    });
                                },
                                onError: function () {
                                    uiBack();
                                }
                            });
                        }
                    });
                }
                else if (item_.Kind == "ShowMap" || item_.Kind == "Car") {
                    showOrderCoords(order);
                }
                else if (item_.Kind == "Driver") {
                    callDriver(order);
                }
            },
            buttons: btBackOnly
        });

        _activeOrdersCallBack2 = function (newOrders_) {
            var newOrders = $.grep(newOrders_, function (e) {
                return (!IsNull(e.Data) && e.Data.ID == order.ID);
            });

            if (newOrders && newOrders.length) {
                order = newOrders[0].Data;
                if (list && list.loadData) {
                    list.loadData(prepareData());
                }
            }
        }
    }

    function showDebug() {
        log("showDebug");

        var deviceInfo = "Информация об устройстве:<br />";

        var md = new MobileDetect(window.navigator.userAgent);
        if (md) {
            var res = window.devicePixelRatio || 1;

            deviceInfo += "FontSize: " + fontSize + "<br />";
            deviceInfo += "DocumentWidth: " + $(document).width() + "<br />";
            deviceInfo += "DocumentHeight: " + $(document).height() + "<br />";
            deviceInfo += "WindowWidth: " + $(window).width() + "<br />";
            deviceInfo += "WindowHeight: " + $(window).height() + "<br />";
            deviceInfo += "Resolution: " + res + "<br />";
            deviceInfo += "Mobile: " + md.mobile() + "<br />";
            deviceInfo += "Phone: " + md.phone() + "<br />";
            deviceInfo += "Tablet: " + md.tablet() + "<br />";
            deviceInfo += "UserAgent: " + md.userAgent() + "<br />";
            deviceInfo += "OS: " + md.os() + "<br />";
            deviceInfo += "WebkitVersion: " + md.version('Webkit') + "<br />";
            deviceInfo += "Build: " + md.versionStr('Build') + "<br />";
        }

        showSelectableList({
            caption: TaxiClientAppConsts.settingCaption,
            items: [
                {
                    ID: "Day",
                    Name: TaxiClientAppConsts.dayMode
                },
                {
                    ID: "Night",
                    Name: TaxiClientAppConsts.nightMode
                }/* ,
                 {
                 ID: "DeviceInfo",
                 Name: deviceInfo,
                 Clickable: false
                 }, */
            ],
            onSelect: function (item_) {
                if (item_.ID == "Day") {
                    styleSetDay();
                }
                else if (item_.ID == "Night") {
                    styleSetNight();
                }
            },
            buttons: {
                left: btBack
            }
        });

    }

    function stackPop(count) {
        for(var i =0; i< count; i++){
            uiStack.pop();
        }
    }

    function getCompanyPromo(callback_) {
        promoAjax({
            url: "/promo/promo/get/corporation",
            success: function (response) {
                console.log(response);
                callback_(response.content);
            },
            error: function () {
                callback_(null);
            }
        });
    }

    function promoAjax(data_){
        $.ajax({
            contentType: "application/json",
            processData: false,
            method: 'POST',
            url: getDataPromoPrefix() + data_.url,
            data: JSON.stringify(data_.data),
            dataType: "json",
            error: data_.error
        }).done(function (res) {
            switch (res.status) {
                case 200:
                    data_.success(res);
                    break;
                case 401:
                    if (!data_.skip401ErrorRetrial) {
                        console.log(">>>>>>> 401 : Updating push cookie");
                        cxAjax({
                            url: getDataURLPrefix() + "/data/GlobalAction?ActionName=TaxiClient_CheckUserAuthorized",
                            params: {
                                DevicePushCookie: pushCookie,
                                ApplicationName: applicationName,
                                ApplicationVersion: applicationVersion
                            },
                            onSuccess: function (result) {
                                data_.skip401ErrorRetrial = true;
                                promoAjax(data_);
                            },
                            onError: function () {
                                data_.skip401ErrorRetrial = true;
                                promoAjax(data_);
                            }
                        });
                    }else{
                        // skip401ErrorRetrial !
                        data_.error(res);
                    }
                    break;
                default:
                    if (!data_.onlyOneTimeRequest) {
                        data_.onlyOneTimeRequest = true;
                        promoAjax(data_);
                    } else {
                        data_.error(res);
                    }
                    break;
            }
        });
    }
}
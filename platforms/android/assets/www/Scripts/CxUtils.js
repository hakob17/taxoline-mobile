﻿///<reference path="jquery-1.9.1.js"/>
function IsNull(value_) {
    return value_ === null || value_ === undefined;
}

function IsNullOrEmpty(value_) {
    return IsNull(value_) || value_ === "";
}

function IsNullOrEmptyOrFalse(value_) {
    return IsNullOrEmpty(value_) || value_ === false ? true : false;
}

function CxDateTimeFormat(value_, format_) {
    if (IsNullOrEmpty(value_))
        return value_;
    else {
        if (moment(value_).isValid()) {
            return moment(value_).format(format_);
        }
        else {
            var DT = new CxDateTime();
            DT.applySpace(value_);
            value_ = DT.toStandardString();
            return moment(value_).format(format_);
        }
    }
}

function RefreshColorsModern(dv) {
    var i = 0;
    $(dv).children().each(function () {
        if ($(this).hasClass("div_gridrecord")) {
            if (i % 2 != 1) $(this).addClass("odd");
            else $(this).removeClass("odd");
            i++;
        }
    });
}
function isHidden(el) {
    return (el.offsetParent === null)
}
function RefreshColorsRecursive(dv) {
    var i = 0;
    $(dv).find(".tree-row").each(function () {
        if (!isHidden(this)) {
            if (i % 2 != 1) { $(this).removeClass("tree-even"); $(this).addClass("tree-odd"); }
            else { $(this).removeClass("tree-odd"); $(this).addClass("tree-even"); }
            i++;
        }
    });
}

function CxReplace(string_, parameters_) {
    if (string_ == null || string_ == undefined)
        return "";
    var i = string_.indexOf("[");
    while (i >= 0) {
        var j = string_.indexOf("]", i);
        if (j < 0)
            break;
        var paramName = string_.substring(i + 1, j);
        var paramValue = GetPropRecursive(parameters_, paramName);
        if (paramValue == null || paramValue == undefined)
            paramValue = "";
        string_ = string_.substring(0, i) + paramValue + string_.substring(j + 1);
        i = string_.indexOf("[");
    }
    return string_;
}
function GetPropRecursive(val_, name_) {
    if (name_ == null || name_ == undefined)
        return "";
    var props = name_.split(".");
    var val = val_;
    $.each(props, function () {
        val = $(val).prop(this);
    });
    return val;
}

function ProcessAction(action_, parameters_, row_, externalValidator_, control_, successCallBack_, skipCheckForSaveChanges_) {

    if ((SD_MODE != true) && (skipCheckForSaveChanges_ != true) && (action_.Name != "Cancel" && action_.Name != "Close" && action_.Name != "ApplyEdit" && action_.Name != "ApplyAdd")) {
        CxCheckForSaveChanges(function () {
            ProcessAction(action_, parameters_, row_, externalValidator_, control_, successCallBack_, true);
        });
        return;
    }

    var parametersAndRow = $.extend({}, row_, parameters_);
    //prepare parameters to send
    var parametersAction = $.extend({}, parameters_);
    var invalidElements = new Array();
    var valid = true;
    var oldControl = control_;
    var newControl = null;

    parametersAction.ActionName = action_.Name;
    if(IsNullOrEmpty(parametersAction.CxObjectsID) && !IsNull(control_))
        parametersAction.CxObjectsID = control_.id;

    $.each(action_.Parameters, function () {
        if (this.Kind == "Clear") {
            parametersAction = {};
        }
        else if (this.Kind == "Const") {
            $(parametersAction).prop(this.Name, this.Const);
        }
        else if (this.Kind == "Field") {

            var editor = null;
            if (control_.GetItemByIdentifier)
                editor = control_.GetItemByIdentifier(this.FieldName);

            if (IsNullOrEmpty(editor)) {
                $(parametersAction).prop(this.Name, GetPropRecursive(row_, this.FieldName));
            }
            else {

                if (editor.Visible) {
                    editor.ApplyUpdates();
                    editor.validate();
                    if (!IsNullOrEmpty(editor.ValidationText)) {
                        invalidElements.push(editor);
                        valid = false;
                    }
                }

                $(parametersAction).prop(this.Name, editor.Value);

            }
        }
        else if (this.Kind == "Parameter") {
            $(parametersAction).prop(this.Name, GetPropRecursive(parameters_, this.ParameterName));
        }
        else if (this.Kind == "JavaScriptCode") {
            var jsValue = new Function("parameters_, row_, control_", this.JavaScriptCode).call(null, parameters_, row_, oldControl);
            $(parametersAction).prop(this.Name, jsValue);
        }
        else if (this.Kind == "FormData") {
            $.each(control_.editorsCollection, function () {
                if (this.Visible) {
                    this.ApplyUpdates();

                    this.validate();
                    if (!IsNullOrEmpty(this.ValidationText)) {
                        invalidElements.push(this);
                        valid = false;
                    }
                }
            });

            if (control_.OnAdditionalValidate) {
                control_.OnAdditionalValidate();
            }
            if (!IsNull(externalValidator_)) {
                var validatorResult = null;
                try {
                    validatorResult = externalValidator_();
                }
                catch (e) {
                    PLATFORM_INTERNAL_ERROR("CxUtils externalValidator error: " + e.message);
                }
                if (validatorResult != null) {
                    valid = false;

                    $.each(validatorResult, function () {
                        var fieldName = this.Editor;
                        var message = this.Message;

                        var editor = GetEditorByFieldName(control_, fieldName);

                        if (IsNull(editor)) {
                            PLATFORM_INTERNAL_ERROR("Editor not found: " + fieldName);
                            return;
                        }

                        editor.ValidationText = message;

                        invalidElements.push(editor);
                    });
                }
            }
            
            $.each(control_.data, function () {
                for (var key in this) {

                    $(parametersAction).prop(key, this[key]);
                }
            });
        }
    });

    switch (action_.ActionKind) {
        case "Link":
            var local_link = CxReplace(action_.Link, parametersAndRow);
            if (local_link) {
                if (!SD_MODE) {
                    CxHistoryPush(local_link, null, null, true);
                }
            }
            else {
                PLATFORM_INTERNAL_ERROR("Invalid link: " + action_.Link);
            }
            break;
        case "UserControl":
            var local_link = CxReplace(action_.Link, parametersAndRow);

            if (SD_MODE) {
                action_.Position = "OverCurrent";
            }
            if (action_.Position == "OverCurrent") {
                oldControl.hide();
            }
            if (action_.Position == "Dialog") {
                SD_MODE = true;
            }

            if (!SD_MODE) {
                CxHistoryPush(local_link, null, null, false);
            }
            CxCreateControl(action_.Position, oldControl.element.parentNode, action_.UserControlName, JSON.stringify(parametersAction), function (a) {
                newControl = a;
                /* Илья - пока убрал. Пусть все редакторы отображаются

                for (var key in parameters_) {
                    if (a.editorsCollection) {
                        var editor = a.GetItemByIdentifier(key);
                        if (editor) {
                            if (editor.FieldName != "IDStructure")
                                editor.Visible = false;
                        }
                    }
                }*/
                if (!IsNull(action_.NextAction)) {
                    ProcessAction(action_.NextAction, parameters_, row_, externalValidator_, newControl, successCallBack_, true)
                }
                else {
                    if (successCallBack_)
                        successCallBack_();
                }
            }, parseInt(oldControl.zIndex) + 1);

            break;
        case "Perform":
            if (valid) {
                if (control_) {
                    if (control_.prevHistory)
                        parametersAction.CxObjectsID = control_.prevHistory.id;
                }

                $.ajax({
                    type: "POST",
                    url: action_.URL,
                    data: JSON.stringify(parametersAction),
                    contentType: "application/json",
                    success: function (result) {
                        if (!IsNull(action_.NextAction)) {

                            try {
                                var parsedResult = jQuery.parseJSON(result);
                                if (typeof parsedResult == 'object') {

                                    parameters_ = $.extend({}, parameters_);
                                    parameters_.ActionResult = parsedResult;
                                }
                            }
                            catch (e) {
                            }

                            ProcessAction(action_.NextAction, parameters_, row_, externalValidator_, oldControl, successCallBack_, true)
                        }
                        else {
                            if (successCallBack_)
                                successCallBack_();
                        }
                    },
                    error: ACTION_ERROR_HANDLER
                });
            }
            else {
                invalidElements[0].MakeVisible(control_);
                return;
            }
            break;
        case "JavaScript":
            var additionalParams = {
                delayNextAction: false, NextActionStub: function (extraParameters_) {
                    if (!IsNull(action_.NextAction)) {
                        var params = $.extend({}, parameters_, extraParameters_);
                        ProcessAction(action_.NextAction, params, row_, externalValidator_, oldControl, successCallBack_, true);
                    }
                    else {
                        if (successCallBack_)
                            successCallBack_();
                    }
                        
                }
            };

            new Function("parameters_, row_, control_, additionalParams_", action_.JavaScriptCode).call(null, parameters_, row_, oldControl, additionalParams);

            if (!IsNull(action_.NextAction) && !additionalParams.delayNextAction) {
                ProcessAction(action_.NextAction, parameters_, row_, externalValidator_, oldControl, successCallBack_, true);
            }
            else {
                if (successCallBack_)
                    successCallBack_();
            }

            break;
        case "Back":
            if (successCallBack_) {
                successCallBack_();
                return;
            }

            if (SD_MODE) {
                if ($(oldControl.element).prev().get(0)) {
                    $($(oldControl.element).prev().get(0)).show();
                    $(oldControl.element).detach();
                }
            }
            else {
                CxHistoryBack();
            }
            /*
            var backButtonClickEventHandlerFirstPart = function () {
                if (!SD_MODE) {
                    getMainWindow().myHistory.history.pop();
                    var nextLink = getMainWindow().myHistory.history[getMainWindow().myHistory.history.length - 1];
                    setMainDocumentURL(nextLink);
                }
                if ($(oldControl.element).prev().get(0)) {
                    $($(oldControl.element).prev().get(0)).show();
                    $(oldControl.element).detach();
                }
                else {
                    if (nextLink) {
                        setBodyFrameURL(nextLink.replace("/L/", "/UI/"));
                    }
                    else {
                        history.back();
                    }
                }
            }
            backButtonClickEventHandlerFirstPart();
            */
            break;
        default://Unknown. Do nothing
            break;
    }
}
function CxAction(actionsList_, id_, parameters_, row_, destination_, toolBarClass_, externalValidator_, control_, clearOldActions_, dynamicActionsAvailabilityURL_, origin_, isReadOnly_) {

    var parametersAndRow = $.extend({}, parameters_, row_);

    if (!IsNullOrEmpty(dynamicActionsAvailabilityURL_)) {

        var url = CxReplace(dynamicActionsAvailabilityURL_, parametersAndRow);

        $.ajax({
            type: "POST",
            url: url,
            data: null,
            contentType: "application/json",
            success: function (result) {
                var extendedRow = $.extend({}, row_, $.parseJSON(result));

                CxAction(actionsList_, id_, parameters_, extendedRow, destination_, toolBarClass_, externalValidator_, control_, clearOldActions_, null, origin_);
            },
            error: ACTION_ERROR_HANDLER
        });

        return;
    }

    var toolbar = findById("toolBar" + control_.id + id_);

    if (toolbar && clearOldActions_) {
        $(toolbar).empty();
    }

    var result = null;
    var realCounter = 0;
    $.each(actionsList_, function () {
        var al = this;

        if (isReadOnly_ && (true != al.AvailableInReadOnlyMode))
            return;

        if (IsNull(toolbar)) {
            toolbar = document.createElement("span");
            $(toolbar).addClass(toolBarClass_);
            toolbar.setAttribute("id", "toolBar" + control_.id + id_);
            destination_.appendChild(toolbar);
        }

        var styleClass;
        if (al.Style != "Unknown") styleClass = "button_" + al.Style.toLowerCase(); else styleClass = "button_gray";

        var canAction = $(row_).prop("Can" + al.Name);
        if (canAction == false)
            return;

        var actionCallBack = function (successCallback_) {

            if (IsNullOrEmpty(al.ConfirmationText)) {
                ProcessAction(al, parameters_, row_, externalValidator_, control_, successCallback_);
            }
            else {
                var CB = new CxConfirmationBox(CxReplace(al.ConfirmationText, parametersAndRow), function () {
                    ProcessAction(al, parameters_, row_, externalValidator_, control_, successCallback_);
                });
            }
        };

        var B = new CxButton(al.Caption, al.Image, "span_button " + styleClass, function (event) {
            actionCallBack();
            event.stopPropagation();
        }).element;
        if (origin_) {
            $(B).attr("origin", origin_);
        }
        toolbar.appendChild(B);
        realCounter++;

        if (actionsList_.length == 1)
            result = {
                button: B,
                callback: actionCallBack
            };

    });
    
    if (realCounter == 0) {
        $(toolbar).hide();
    }/*
    else {
        $(toolbar).show();
    }*/

    return result;
}

function GroupLoadContent(div_, advEditors_, callback_) {
    var div = $(div_).children("div");
    if ($(div_).hasClass("control-item-group-expanded")) {/*Card - group - expanded*/
        $(div).show();
        var idDiv = div[0].id;
        $.each(advEditors_, function () {
            var editor = this;
            if (idDiv == editor.ID && !editor.Processed) {

                if (editor.Content.Kind == "UserControl") {
                    CxCreateControl("OverCurrent", idDiv, editor.Content.UserControlName, JSON.stringify(editor.Parameters), callback_);
                }
                editor.Processed = true;
            }
        });
    }
    else {
        $(div).hide();
    }
}

function RefreshColors(T_) {
    var i = 0;
    $(T_).children().each(function () {
        if ($(this).hasClass("Heavy-grid-grid-row")) {
            if (i % 2 != 1) $(this).addClass("odd");
            else $(this).removeClass("odd");
            i++;
        }
    });
}

function createUUID() {
    var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 36; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
    s[8] = s[13] = s[18] = s[23] = "-";

    var uuid = s.join("");
    return uuid;
}

function CxHashTable() {
    var self = this;
    this.length = 0;
    this.items = new Object();
    /*
    for (var i = 0; i < arguments.length; i += 2) {
    if (typeof (arguments[i + 1]) != "undefined") {
    this.items[arguments[i]] = arguments[i + 1];
    this.length++;
    }
    }
    */


    this.removeItem = function (in_key) {
        var tmp_previous;
        if (typeof (self.items[in_key]) != "undefined") {
            self.length--;
            var tmp_previous = self.items[in_key];
            delete self.items[in_key];
        }

        return tmp_previous;
    }

    this.getItem = function (in_key) {
        return self.items[in_key];
    }

    this.setItem = function (in_key, in_value) {
        var tmp_previous;
        if (typeof (in_value) != "undefined") {
            if (typeof (self.items[in_key]) == "undefined") {
                self.length++;
            } else {
                tmp_previous = self.items[in_key];
            }

            self.items[in_key] = in_value;
        }

        return tmp_previous;
    }

    this.hasItem = function (in_key) {
        return typeof (self.items[in_key]) != "undefined";
    }

    this.clear = function () {
        for (var i in this.items) {
            delete self.items[i];
        }

        self.length = 0;
    }
}

jQuery.fn.scrollComplete = function (fn, ms) {
    var timer = null;
    this.scroll(function () {
        if (timer) {
            clearTimeout(timer);
        }
        timer = setTimeout(fn, ms);
    });
}

jQuery.fn.resizeComplete = function (fn, ms) {
    var timer = null;
    this.resize(function () {
        if (timer) {
            clearTimeout(timer);
        }
        timer = setTimeout(fn, ms);
    });
}



function setRegexParser(input, regex) {
    input.addEventListener("textInput", function (event) {

        var selStart = input.selectionStart;
        var selEnd = input.selectionEnd;

        var oldText = $(this).val();
        if (IsNull(oldText))
            oldText = "";

        var newText = oldText.substr(0, selStart) + event.data + oldText.substr(selEnd);

        if (newText.match(regex) == null) {
            event.preventDefault();
            event.returnValue = false;
            return false;
        }
    }, false);
}

function extend(Child, Parent) {
    var F = function () { }
    F.prototype = Parent.prototype
    Child.prototype = new F()
    Child.prototype.constructor = Child
    Child.superclass = Parent.prototype
}

function mix(source, target) {
    for (var key in source) {
        if (source.hasOwnProperty(key)) {
            target[key] = source[key];
        }
    }
}

function asyncSetDynamicData(value_, data_, divToInsert_) {
    var edName = data_["Value_EditorName"]; //HARDCODE
    if (!edName) {
        PLATFORM_INTERNAL_ERROR("Required field Value_EditorName for dynamic editor not found");
        return;
    }
    $.ajax({
        type: "GET",
        url: "/Data/",
        data: { ViewName: "Platform_Editors" }, //HARDCODE
        contentType: "application/json",
        success: function (result) {
            var editorsList = $.parseJSON(result);
            var result = $.grep(editorsList, function (e) { return e.Name == edName; });
            if (result.length == 0) {
                PLATFORM_INTERNAL_ERROR("EditorNotFound: " + edName);
            }
            else {

                var ops = $.parseJSON(result[0].MetaData);
                value_ = CxSimpleConvert(value_, ops);
                if (divToInsert_) {
                    $(divToInsert_).html(!IsNull(value_) ? value_ : "");
                }
                else {
                    PLATFORM_INTERNAL_ERROR("Error: container not created");
                }
            }
        },
        error: DATA_REQUEST_ERROR_HANDLER
    });
}
/*
function asyncCreateEditorAndAddToCollection(data_, editorParams_, collection_, fieldName_, callback_) {
    var edName = data_[fieldName_ + "_EditorName"];
    if (!edName) {
        PLATFORM_INTERNAL_ERROR("Required field Value_EditorName for dynamic editor not found");
    }
    $.ajax({
        type: "GET",
        url: "/Data/",
        data: { ViewName: "Platform_Editors" }, //HARDCODE
        contentType: "application/json",
        success: function (result) {
            var editorsList = $.parseJSON(result);
            var result = $.grep(editorsList, function (e) { return e.Name == edName; });
            if (result.length == 0) {
                PLATFORM_INTERNAL_ERROR("Editor not found: " + edName);
            }
            else {
                var ops = $.parseJSON(result[0].EditorOptions); //new options
                editorParams_.item.EditorOptions = ops;
                var C = CxCreateEditor(editorParams_);
                collection_.push(C);
                if (callback_)
                    callback_(C);
            }
        },
        error: DATA_REQUEST_ERROR_HANDLER
    });
}

var _cachedPlatformEditors = null;

function GetPlatformEditors() {

    if (IsNull(_cachedPlatformEditors)) {
        $.ajax({
            async: false,
            type: "POST",
            url: "/Data/",
            data: JSON.stringify({ "ViewName": "Platform_Editors" }),
            contentType: "application/json",
            error: DATA_REQUEST_ERROR_HANDLER
        }).done(function (result) {
            _cachedPlatformEditors = $.parseJSON(result);
        });
    }
    return _cachedPlatformEditors;
}
*/
/*
var PrepareDynamicData = function (metaData_) {

    var hasDynamicData = false;
    var ans = null;

    console.log("PrepareDynamicData");
    console.log(metaData_);

    if (metaData_.Body) {
        $.each(metaData_.Body.Rows, function () {
            $.each(this.Items, function () {
                if (this.EditorOptions && this.EditorOptions.EditorKind == "Dynamic") {
                    hasDynamicData = true;
                }
            });
        });
    }
    if (metaData_.FocusedBody) {
        $.each(metaData_.FocusedBody.Rows, function () {
            $.each(this.Items, function () {
                if (this.EditorOptions && this.EditorOptions.EditorKind == "Dynamic") {
                    hasDynamicData = true;
                }
            });
        });
    }
    if (hasDynamicData) {
        $.ajax({
            async: false,
            type: "POST",
            url: "/Data/",
            data: JSON.stringify({ "ViewName": "Platform_Editors" }),
            contentType: "application/json",
            error: DATA_REQUEST_ERROR_HANDLER
        }).done(function (result) {
            ans = $.parseJSON(result);
        });
    }

    console.log(ans);

    return ans;
}
*/
var renderItem = function (item_, data_) {
    var meat = $.extend({}, item_);

    if (IsNull(item_.EditorOptions))
        item_.EditorOptions = CxGetEditorByName(item_.EditorName);

    if (item_.EditorOptions.EditorKind == "Dynamic") {
        var edName = data_[item_.FieldName + item_.EditorOptions.FieldNameEditorNamePostfix];
        /*
        if (IsNull(edName)) {
            edName = dynamicEditorsHash.getItem(data_.ID);
        }
        else {
            dynamicEditorsHash.setItem(data_.ID, edName);
        }*/
        meat.EditorOptions = CxGetEditorByName(edName);
    }
    return meat;
}

var cumulativeOffset = function (element, global_) {

    var saved = element;

    var top = 0, left = 0;
    do {
        top += element.offsetTop + element.clientTop || 0;
        left += element.offsetLeft + element.clientLeft || 0;

        if (element.scrollTop)
            top -= element.scrollTop;

        if (element.scrollLeft)
            left -= element.scrollLeft;

        element = element.offsetParent;
    } while (element);


    if (global_) {
        var childWindow = saved.ownerDocument.defaultView;
        while (childWindow != getMainWindow()) {
            left = left + childWindow.frameElement.getBoundingClientRect().left;
            top = top + childWindow.frameElement.getBoundingClientRect().top;
            childWindow = childWindow.parent;
        }
    }


    return {
        top: top,
        left: left
    };
}

var ScrollTo = function (divToScroll, element) {
    
    if (!IsNull(element)) {
        var offset = cumulativeOffset(element);
        var delta = offset.top + $(element).height() - $(window).height();
        var p = parseInt(delta) + parseInt($(divToScroll).height() / 2) + parseInt($(element).height());
        if (delta > 0) {
            $(divToScroll).scrollTop(p);
        }
    }
}


var ScrollTo2 = function (divToScroll, element) {

    if (!IsNull(element)) {
        var delta = element.offsetTop + $(element).height() - $(divToScroll).height();
        var p = parseInt(delta) + parseInt($(divToScroll).height() / 2) + parseInt($(element).height());
        if (delta > 0) {
            $(divToScroll).scrollTop(p);
        }
    }
}



var rgbToHex = (function () {
    var rx = /^rgb\s*\(\s*(\d+)\s*,\s*(\d+)\s*,\s*(\d+)\s*\)$/i;

    function pad(num) {
        if (num.length === 1) {
            num = "0" + num;
        }

        return num;
    }

    return function (rgb, uppercase) {
        var rxArray = rgb.match(rx),
            hex;

        if (rxArray !== null) {
            hex = pad(parseInt(rxArray[1], 10).toString(16)) + pad(parseInt(rxArray[2], 10).toString(16)) + pad(parseInt(rxArray[3], 10).toString(16));

            if (uppercase === true) {
                hex = hex.toUpperCase();
            }

            return "#" + hex;
        }

        return;
    };
}());


var utilConvert = function (v, f) {
    if (IsNullOrEmpty(v)) return null;
    switch (f) {
        case "HH:mm:ss":
            var sec_num = parseInt(v, 10); // don't forget the second param
            var hours = Math.floor(sec_num / 3600);
            var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
            var seconds = sec_num - (hours * 3600) - (minutes * 60);

            if (hours < 10) { hours = "0" + hours; }
            if (minutes < 10) { minutes = "0" + minutes; }
            if (seconds < 10) { seconds = "0" + seconds; }
            var time = hours + ':' + minutes + ':' + seconds;
            return time;
        case "mm:ss":
            var sec_num = parseInt(v, 10); // don't forget the second param
            var hours = Math.floor(sec_num / 3600);
            var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
            var seconds = sec_num - (hours * 3600) - (minutes * 60);

            if (minutes < 10) { minutes = "0" + minutes; }
            if (seconds < 10) { seconds = "0" + seconds; }
            var time = minutes + ':' + seconds;

            if (hours > 0)
                time = hours + ":" + time;

            return time;
        default: return null;
    };
}


function dist(point1, point2) {
    return Math.sqrt(Math.pow(point2[0] - point1[0], 2) + Math.pow(point2[1] - point1[1], 2));
}
function copy(point1, point2) {
    point1[0] = point2[0];
    point1[1] = point2[1];
}

function IControlSizeHeight(elem, ics) {
    if (ics.Unit == "Auto") {
        var styleStr = "100%";
        $(elem).siblings().each(function () {
            styleStr += " - " + this.style.height;
        });
        styleStr = "calc(" + styleStr + ")";
        elem.style.height = styleStr;
    }
    else if (ics.Unit == "Percents") {
        elem.style.height = ics.Size + "%";
    }
    else if (ics.Unit == "Pixels") {
        elem.style.height = ics.Size + "px";
    }
}
function IControlSizeWidth(elem, ics) {
    if (ics.Unit == "Auto") {
        var styleStr = "100%";
        $(elem).siblings().each(function () {
            styleStr += " - " + this.style.width;
        });
        styleStr = "calc(" + styleStr + ")";
        elem.style.width = styleStr;
    }
    else if (ics.Unit == "Percents") {
        elem.style.width = ics.Size + "%";
    }
    else if (ics.Unit == "Pixels") {
        elem.style.width = ics.Size + "px";
    }
}

function CalculateSizeWidth(array) { 
    $.each(array, function () {
        var splitter = this.Splitter;
        var sizes = this.ChildrenSizes;
        var autoNumber = 0;
        $.each(sizes, function () {
            if (this.Unit == "Auto") {
                autoNumber++;
            }
        });
        for (var i = 0; i < sizes.length; i++) {
            var currentElement = $(splitter).children().get(i);
            var itsSize = sizes[i];
            switch (itsSize.Unit) {
                case "Auto":
                    var styleStr = "100%";
                    $.each(sizes, function () {
                        if(this.Unit == "Pixels"){
                            styleStr += " - " + this.Size + "px";
                            }
                        else if (this.Unit == "Percents") {
                            styleStr += " - " + this.Size + "%";
                        }
                    });
                    //autonumber cannot be = 0!
                    styleStr = "calc((" + styleStr + ") / " + autoNumber + ")";
                    currentElement.style.width = styleStr;
                    break;
                case "Pixels":
                    currentElement.style.width = itsSize.Size + "px";
                    break;
                case "Percents":
                    currentElement.style.width = itsSize.Size + "%";
                    break;
                default:
                    break;
            }
        }
    });
}

function CalculateSizeHeight(array) {
    $.each(array, function () {
        var splitter = this.Splitter;
        var sizes = this.ChildrenSizes;
        var autoNumber = 0;
        $.each(sizes, function () {
            if (this.Unit == "Auto") {
                autoNumber++;
            }
        });
        for (var i = 0; i < sizes.length; i++) {
            var currentElement = $(splitter).children().get(i);
            var itsSize = sizes[i];
            switch (itsSize.Unit) {
                case "Auto":
                    var styleStr = "100%";
                    $.each(sizes, function () {
                        if (this.Unit == "Pixels") {
                            styleStr += " - " + this.Size + "px";
                        }
                        else if (this.Unit == "Percents") {
                            styleStr += " - " + this.Size + "%";
                        }
                    });
                    //autonumber cannot be = 0!
                    styleStr = "calc((" + styleStr + ") / " + autoNumber + ")";
                    currentElement.style.height = styleStr;
                    break;
                case "Pixels":
                    currentElement.style.height = itsSize.Size + "px";
                    break;
                case "Percents":
                    currentElement.style.height = itsSize.Size + "%";
                    break;
                default:
                    break;
            }
        }
    });
}

Date.prototype.timeCut = function () {
    this.setHours(0, 0, 0, 0);
    return this;
}
Date.prototype.setDateExtended = function (d) {
    this.setDate(d);
    return this;
}
Date.prototype.addDateExtended = function (d) {
    this.setDate(this.getDate() + d);
    return this;
}
Date.prototype.addMonthExtended = function (m) {
    this.setMonth(this.getMonth() + m);
    return this;
}
Date.prototype.weekBegin = function () {
    var counter = 0;
    while (this.getDay() != 1 && counter++ < 7)
        this.setDate(this.getDate() - 1);
    return this;
}
Date.prototype.weekEnd = function () {
    var counter = 0;
    while (this.getDay() != 0 && counter++ < 7)
        this.setDate(this.getDate() +1);
    return this;
}
function today() {
    var now = new Date();
    return ((now.getDate() < 10) ? "0" : "") + now.getDate() + "." + (((now.getMonth() + 1) < 10) ? "0" : "") + (now.getMonth() + 1) + "." + now.getFullYear() + " " +
        ((now.getHours() < 10) ? "0" : "") + now.getHours() + ":" + ((now.getMinutes() < 10) ? "0" : "") + now.getMinutes() + ":" + ((now.getSeconds() < 10) ? "0" : "") + now.getSeconds();
};
function toSize(number) {
    var s = parseInt(number);
    var d = 0;
    var powers = 0;
    while (s) {
        d = s;
        s = parseInt(s / 1000);
        if (s) powers++;
    }
    switch (powers) {
        case 0: return d + " б";
        case 1: return d + " Кб";
        case 2: return d + " Мб";
        case 3: return d + " Гб";
        case 4: return d + " Тб";
        default: return ">999 Тб";
    }
}

function GetVoIPDisplayText(userName_, userNumber_) {
    if (IsNullOrEmpty(userName_) && IsNullOrEmpty(userNumber_))
        return "Номер скрыт";
    else if (!IsNullOrEmpty(userName_) && IsNullOrEmpty(userNumber_))
        return userName_;
    else if (IsNullOrEmpty(userName_) && !IsNullOrEmpty(userNumber_))
        return userNumber_;
    else if (userName_ == userNumber_)
        return userNumber_;
    else
        return userName_ + " (" + userNumber_ + ")";
}



function GetDataObjects(dataViewName_, parameters_) {
    var answer = [];
    $(parameters_).prop("ViewName", dataViewName_);
    cxGetAjax({
        async: false,
        type: "POST",
        url: "/Data/",
        data: /*JSON.stringify(*/parameters_/*)*/,
        contentType: "application/json",
        success: function (result) {
            answer = $.parseJSON(result);
        },
        error: DATA_REQUEST_ERROR_HANDLER
    });
    return answer;
}


(function ($) {
    $.each(['show', 'hide'], function (i, ev) {
        var el = $.fn[ev];
        $.fn[ev] = function () {
            this.trigger(ev);
            return el.apply(this, arguments);
        };
    });
})(jQuery);


function goToLinkOnClick(element_, link_) {
    if (link_.indexOf("/$prefix$/") >= 0) {
        $(element_).attr("onclick", "event.stopPropagation();\
        CxCheckForSaveChanges(function () { \
            CxHistoryPush('" + link_ + "', null, null, true); \
        });");
    }
    else {
        $(element_).attr("onclick", "event.stopPropagation();\
        CxCheckForSaveChanges(function () { \
            document.location.href = '" + link_ + "';\
        });");
    }
}

function showControlOnClick(element_, link_, userControlName_) {

    $(element_).click(function (event) {
        event.stopPropagation();
        CxCheckForSaveChanges(function () {
            CxHistoryPush(link_, null, null, true);
        });
    });

    /*
    $(element_).attr("onclick", "event.stopPropagation();\
        CxCheckForSaveChanges(function () { \
            CxHistoryPush('"+ link_ + "', null, null, true); \
            "+(IsNullOrEmpty(callbackCode_) ? "" : callbackCode_)+"\
        });");
    */
}

function addKeyCombinationHandler(editor_, keyCombinationObject_, handlerCode_) {
    var flag = false;
    var f = new Function(handlerCode_);
    $(document).keydown(function (event) {
        if (editor_.isFocused) {
            if (event.which == keyCombinationObject_.firstKey) {
                flag = true;
                if (!keyCombinationObject_.secondKey) {
                    f.call();
                    flag = false;
                }
            }
            else if (keyCombinationObject_.secondKey && event.which == keyCombinationObject_.secondKey && flag) {
                if (!keyCombinationObject_.thirdKey) {
                    f.call();
                    flag = false;
                }
            }
            else if (keyCombinationObject_.thirdKey && event.which == keyCombinationObject_.thirdKey && flag) {
                f.call();
                flag = false;
            }
            else {
                flag = false;
            }
        }
    });
    $(document).keyup(function () {
        flag = false;
    });
}


function CxServerLogWriteString(message_, level_) {

    if (IsNullOrEmpty(level_))
        level_ = 3;

    var parametersAction = {
        Message: message_,
        Level: level_
    };

    $.ajax({
        type: "POST",
        url: "/Utils/LogWriteString",
        data: JSON.stringify(parametersAction),
        contentType: "application/json",
        success: function (result) {
        },
        error: function (result) {
        }
    });
}

function CxProcessPOST(url_, parameters_, successCallBack_, errorCallBack_) {
    $.ajax({
        type: "POST",
        url: url_,
        data: JSON.stringify(parameters_),
        contentType: "application/json",
        success: function (result) {
            if (successCallBack_)
                successCallBack_(result);
        },
        error: function (result) {
            if (errorCallBack_)
                errorCallBack_(result);
        }
    });
}

function CxProcessSyncPOST(url_, parameters_, successCallBack_, errorCallBack_) {
    $.ajax({
        type: "POST",
        async: false,
        url: url_,
        data: JSON.stringify(parameters_),
        contentType: "application/json",
        success: function (result) {
            if (successCallBack_)
                successCallBack_(result);
        },
        error: function (result) {
            if (errorCallBack_)
                errorCallBack_(result);
        }
    });
}

function CxStatusSetOnline() {

    var parametersAction = {
        ActionName: "CallCenter_SetUserStatus",
        IDUserStatus: "online"
    };

    CxProcessPOST("/Data/GlobalAction", parametersAction);

    /*
    var parametersAction = {
        ActionName: "CallCenter_SetUserStatus",
        IDUserStatus: "online"
    };

    $.ajax({
        type: "POST",
        url: "/Data/GlobalAction",
        data: JSON.stringify(parametersAction),
        contentType: "application/json",
        success: function (result) {
        },
        error: function (result) {
        }
    });
    */
}


$.fn.setCursorPosition = function (position) {
    if (this.length == 0) return this;
    return $(this).setSelection(position, position);
}

$.fn.setSelection = function (selectionStart, selectionEnd) {
    if (this.length == 0) return this;
    input = this[0];

    if (input.createTextRange) {
        var range = input.createTextRange();
        range.collapse(true);
        range.moveEnd('character', selectionEnd);
        range.moveStart('character', selectionStart);
        range.select();
    } else if (input.setSelectionRange) {
        input.focus();
        input.setSelectionRange(selectionStart, selectionEnd);
    }

    return this;
}

$.fn.focusEnd = function () {
    this.setCursorPosition(this.val().length);
    return this;
}

function CxCreateAccessDeniedSplash(position_, idDiv_, callback_, zIndex_) {
    metadata = {
        Kind: "Custom",
        Padding: "Standard",
        Width: "Standard",
        HTMLCode: "<div style='text-align: center; width: 100%'>Недостаточно прав для просмотра</div>"
    };

    defineControl(position_, idDiv_, metadata, {}, callback_, zIndex_);

}

function GetFormData(control_) {
    var result = {};
    if (control_.editorsCollection.length) {
        $.each(control_.editorsCollection, function () {
            if (this.isEditor && this.FieldName) {
                result[this.FieldName] = this.Value;
            }
        });
        return result;
    }
}

jQuery.fn.extend({
    insertAtCaret: function (myValue) {
        return this.each(function (i) {
            if (document.selection) {
                //For browsers like Internet Explorer
                this.focus();
                var sel = document.selection.createRange();
                sel.text = myValue;
                this.focus();
            }
            else if (this.selectionStart || this.selectionStart == '0') {
                //For browsers like Firefox and Webkit based
                var startPos = this.selectionStart;
                var endPos = this.selectionEnd;
                var scrollTop = this.scrollTop;
                this.value = this.value.substring(0, startPos) + myValue + this.value.substring(endPos, this.value.length);
                this.focus();
                this.selectionStart = startPos + myValue.length;
                this.selectionEnd = startPos + myValue.length;
                this.scrollTop = scrollTop;
            } else {
                this.value += myValue;
                this.focus();
            }
        });
    }
});

function cxGetAjax(params_) {

    if (!IsNull(params_.data))
        params_.data = JSON.stringify(params_.data);
    return $.ajax(params_);
/*
    params_.type = "GET";
    params_.url += "?";

    console.log("CONVERT");
    console.log(params_.data);

    if (!IsNull(params_.data)) {
        $.each(params_.data, function (a, b) {
            if (!IsNull(b))
                params_.url += a + "=" + b + "&";
        });
    }
    
    params_.url += "rnd=" + Math.random();
    params_.data = null;

    console.log(params_.url);

    return $.ajax(params_);*/
}

